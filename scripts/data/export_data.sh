#!/bin/bash

set -e

APPLICATION=version
ORGANIZATION=gitlab
if [[ "$SINGLE_TABLE_TO_EXPORT" ]]; then
    declare -a TABLES_TO_EXPORT=("${SINGLE_TABLE_TO_EXPORT}")
else
    declare -a TABLES_TO_EXPORT=("conversational_development_indices" "fortune_companies" "hosts" "raw_usage_data" "usage_data" "usage_ping_errors" "usage_ping_metadata" "users" "versions")
fi
days_to_export=${DAYS_TO_EXPORT:-3}
reexport_existing_files=${REEXPORT_EXISTING_FILES:-0}
verbose=${VERBOSE:-0}

check_environment_variables() {
    if [[ $days_to_export -gt 7 ]]; then
          echo "Please set DAYS_TO_EXPORT environment variable to less or equal than 7"
    fi
    if [ -z "$GCLOUD_SERVICE_KEY" ]; then
        echo "Please set the GCLOUD_SERVICE_KEY environment variable to the Google auth JSON file name"
        exit 1
    else
      if [ -f "$GCLOUD_SERVICE_KEY" ]; then
          echo "GCLOUD_SERVICE_KEY is set"
      else
          echo "The GCLOUD_SERVICE_KEY environment variable must be set to variable type: file to prevent confidential data leaking into job logs"
          exit 1
      fi
    fi
    if [ -z "$GOOGLE_PROJECT_ID" ]; then
        echo "Please set the GOOGLE_PROJECT_ID environment variable to the id for the GCP project"
        exit 1
    else
        echo "GOOGLE_PROJECT_ID is $GOOGLE_PROJECT_ID"
    fi
    if [ -z "$GOOGLE_COMPUTE_ZONE" ]; then
        echo "Please set the GOOGLE_COMPUTE_ZONE environment variable for the region and zone"
        exit 1
    else
        echo "GOOGLE_COMPUTE_ZONE is $GOOGLE_COMPUTE_ZONE"
    fi

    if [ -z "$BUCKET" ]; then
        echo "Please set the BUCKET environment variable to the GCS bucket where we shall upload the SQL export"
        exit 1
    else
        echo "BUCKET is ${BUCKET}"
    fi

    if [ -z "$INSTANCE" ]; then
        echo "Please set the INSTANCE environment variable to the Cloud SQL instance"
        exit 1
    else
        echo "INSTANCE is $INSTANCE"
    fi

    if [ -z "$DATABASE" ]; then
        echo "Please set the DATABASE environment variable to the database name"
        exit 1
    else
        echo "DATABASE is $DATABASE"
    fi
}

authenticate_to_gcloud() {
    gcloud --quiet auth activate-service-account --key-file="${GCLOUD_SERVICE_KEY}"
    gcloud --quiet config set project "${GOOGLE_PROJECT_ID}"
    gcloud --quiet config set compute/zone "${GOOGLE_COMPUTE_ZONE}"
}


show_existing_files_on_gcloud() {
    existing_files=$(gsutil ls gs://"$BUCKET")
    if verbose_mode; then
        echo "These files already exist:"
        echo "$existing_files"
    fi
}

verbose_mode() {
 if [[ $verbose -gt 0 ]]; then
   return 0
 else
   return 1
 fi
}

# Builds the export query for the table by
# sourcing a file in the scripts/data/sql/ folder
#
# $1 date: date string  # e.g '2020-01-04'
# $2 table name: string # e.g usage_data
build_export_query() {
    date="$1"
    sql_file="scripts/data/sql/$2.sql.sh"

    # shellcheck source=/dev/null
    source "$sql_file"

    echo "$query"
}

wait_for_operations_to_finish() {
    running_operations=$(gcloud sql operations list --instance="$INSTANCE" --filter='NOT status:done' --format='value(name)')

    if  [ ! -z "$running_operations" ]; then
        echo "Waiting for operations: $running_operations"

        gcloud sql operations wait "$running_operations" --project="$GOOGLE_PROJECT_ID" --timeout=1800
    fi
}


# Exports data for a table
#
# $1 date: date string  # e.g '2020-01-04'
# $2 table name: string # e.g usage_data
# $3 day_number: string # e.g 0
export_day() {
    file=$ORGANIZATION-$APPLICATION-$2-$1.gz
    query="$(build_export_query "$1" "$2" "$3")"

    echo "Exporting $table for $date $day_number into $file"
    if [[ $reexport_existing_files == 0 && $4 -gt 0 && $existing_files == *$file* ]]; then
        echo "Already exported: $file"
    else
        if verbose_mode; then
            echo "$query"
        fi

        # make sure that there are no other operations that might block stating export process
        wait_for_operations_to_finish

        gcloud sql export csv "$INSTANCE" gs://"$BUCKET"/"$file" --database="$DATABASE" --query="$query" --async

        # wait for export process to finish
        wait_for_operations_to_finish
    fi
}

main() {
    check_environment_variables
    authenticate_to_gcloud
    show_existing_files_on_gcloud
    
    for table in "${TABLES_TO_EXPORT[@]}"
    do
        for day_number in $(seq "$days_to_export")
        do
            date=$(date -d "$day_number days ago" +%Y-%m-%d)
            export_day "$date" "$table" "$day_number"
        done
    done
}

main
