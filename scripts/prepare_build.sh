#!/bin/bash

if [ -f /.dockerenv ] || [ -f ./dockerinit ]; then
	mkdir -p vendor

    if [ ! -e vendor/phantomjs_1.9.8-0jessie_amd64.deb ]; then
    	wget -O vendor/phantomjs_1.9.8-0jessie_amd64.deb -q \
    		https://gitlab.com/axil/phantomjs-debian/raw/master/phantomjs_1.9.8-0jessie_amd64.deb
	fi
    dpkg -i vendor/phantomjs_1.9.8-0jessie_amd64.deb

    apt-get update -qq
    apt-get -o dir::cache::archives="vendor/apt" install -y -qq --force-yes nodejs

	cp config/database.yml.ci config/database.yml
fi