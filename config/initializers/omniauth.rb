# frozen_string_literal: true

# https://github.com/omniauth/omniauth/wiki/Resolving-CVE-2015-9284
# GET requests will result in 'Not found. Authentication passthru.'
OmniAuth.config.allowed_request_methods = [:post]
