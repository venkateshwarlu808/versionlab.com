# frozen_string_literal: true

# note while in google cloud, client commands are disabled by default
# set id to nil solves this as per
# https://github.com/mperham/sidekiq/issues/3518#issuecomment-390898217

Sidekiq.configure_client do |config|
  config.redis = {
    namespace: ENV['SIDEKIQ_NAMESPACE'].presence,
    id: nil
  }
end

Sidekiq.configure_server do |config|
  config.redis = {
    namespace: ENV['SIDEKIQ_NAMESPACE'].presence,
    id: nil
  }

  Sidekiq::ReliableFetch.setup_reliable_fetch!(config)
end
