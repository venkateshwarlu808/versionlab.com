# frozen_string_literal: true

require "rack/cors"

# Allow access to GitLab Version API from other domains
Rails.configuration.middleware.use Rack::Cors do
  allow do
    origins '*'
    resource '/api/*',
             headers: :any,
             methods: :any,
             expose: ['Link']
  end
end
