# frozen_string_literal: true
source 'https://rubygems.org'

env_ruby = ENV['RUBY_VERSION']

ruby env_ruby if env_ruby

gem 'rails', '~> 6.1.7.1'

gem 'rack', '~> 2.2.4'

# DBs
gem 'pg', '~> 1.1'
gem 'redis-rails', '~> 5.0.2'

# Application server
gem 'unicorn-rails'

# Authentication libraries
gem 'devise', '~> 4.7.1'
gem 'omniauth-gitlab', '~> 1.0.3'
gem 'omniauth-rails_csrf_protection', '~> 0.1'

# API
gem 'jbuilder', '~> 2.0'
gem 'grape', '~> 1.5.2'
gem 'grape-entity', '~> 0.10.0'
gem 'rack-cors', '~> 1.1.1', require: 'rack/cors'

# Pagination
gem 'kaminari', '~> 1.2.1'

# HAML
gem 'hamlit', '~> 2.15.0'

# Misc
gem 'addressable'
gem 'public_suffix'
gem 'version_sorter', '~> 2.2'
gem 'gon'
gem 'gitlab-sdk', '~> 0.2'

# Assets
gem 'sass-rails', '~> 5.0.7'
gem 'turbolinks', '~> 5.1.1'
gem 'jquery-rails', '~> 4.4.0'
gem 'jquery-ui-rails'
gem 'nprogress-rails'
gem 'terser', '~> 1.1.16'

gem 'httparty'
gem 'sidekiq', '~> 6.1'
gem 'sidekiq-failures'
gem 'sidekiq_alive'
gem 'gitlab-sidekiq-fetcher', '0.9.0', require: 'sidekiq-reliable-fetch'
gem 'redis-namespace'

# Charts
gem 'groupdate', '~> 4.0.1'
gem 'chartkick', '~> 3.4.0'

# Twitter bootstrap
gem 'bootstrap', '~> 4.3.1'

gem 'nokogiri', '~> 1.13.10'

# Salesforce integration
gem 'restforce', '~> 3.2.0'

group :development do
  # The version used here should match the SAST analyzer for consistency
  # https://gitlab.com/gitlab-org/security-products/analyzers/brakeman/-/blob/master/Dockerfile#L12
  gem 'brakeman', '4.3.1', require: false
  gem 'web-console', '~> 3.7'

  # Docs generator
  gem "sdoc", '~> 1.1.0'
end

# Sentry integration
gem 'sentry-ruby', '~>5.1.0'
gem 'sentry-rails', '~>5.1.0'

group :development, :test do
  gem 'dotenv-rails'
  gem 'bundler-audit', '~> 0.7.0.1', require: false
  gem 'gitlab-dangerfiles', '~> 2.6.1', require: false
  gem 'git', '~> 1.13.0' # make danger use a higher version of this gem
  gem 'gitlab-styles', '~> 7.1.0', require: false
  gem 'lefthook', '~> 1.1.1', require: false

  gem 'rubocop'

  # Tests
  gem 'rspec-rails', '~> 5.0.1'
  gem 'minitest'
  gem 'launchy'

  # Profiling
  gem 'rack-mini-profiler', require: false

  gem 'spring', '1.3.6'

  # Generate Fake data
  gem 'faker'

  gem 'simplecov', require: false
  gem 'factory_bot_rails', '~> 5.1.0'

  # JS Runtime
  gem 'execjs'

  # Debugging
  gem 'pry-rails'
  gem 'pry-rescue'
  gem 'pry-stack_explorer'
  gem 'pry-nav'

  gem 'timecop', '~> 0.8.0'
end

group :test do
  gem 'puma', '~> 5.6.4' # for browser testing locally
  gem 'webmock', '~> 3.9.1'
  gem 'shoulda-matchers', '~> 4.0.1', require: false
  gem 'rspec-parameterized', require: false
  gem 'selenium-webdriver', '~> 3.142'
  gem 'webdrivers'
  gem 'capybara', '~> 3.35.3'
  gem 'capybara-screenshot', '~> 1.0.22'
  gem 'test-prof', '~> 0.10.0'
end
