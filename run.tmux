#!/bin/bash

set -o allexport
docker-compose start

tmux new-session -d -s "version" -c "$PWD"
tmux split-pane -v -c "$PWD"

source .env
tmux send-keys -t 0.0 "rvm use 2.1 > /dev/null; nvm use default" C-m
tmux send-keys -t 0.0 "rails s" C-m

tmux send-keys -t 0.1 "rvm use 2.1 > /dev/null; nvm use default" C-m
tmux send-keys -t 0.1 "bundle exec sidekiq" C-m

source .env.test
tmux new-window -c "$PWD"
tmux send-keys -t 1.0 "rvm use 2.1; nvm use default" C-m

tmux new-window
tmux select-window -t 2.0
tmux split-pane -v 

tmux send-keys -t 2.0 "docker logs -f versiongitlabcom_redis_1" C-m
tmux send-keys -t 2.1 "docker logs -f versiongitlabcom_db_1" C-m
tmux select-window -t 0.0

tmux attach -t "version"
