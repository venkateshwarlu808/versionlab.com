# version.gitlab.com application

[[_TOC_]]

## Responsibilities

- Store available GitLab versions and information whether they are vulnerable
- Render version check badge
- Collect data sent during [version check](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#version-check)
  - Instance admins can turn off version check in the application settings
- Collect data sent with [service ping](#service-ping)

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).

## Installation

### With Docker desktop

A docker-compose setup is provided to simplify the installation of externals components (postgresql, redis).

1. Get source code

        git clone git@gitlab.com:gitlab-services/version-gitlab-com.git

1. Install gems

        bundle install

1. Docker based external services (an alternate way to utilize the database locally)

  Download and install [docker desktop](https://docs.docker.com/desktop/)

  Use `docker-compose up` to start a postgresql and redis instance.

  This docker stack exposes the following services:
    - PostgreSQL on port 9432
    - Redis on port 6379

1. Setup database (using PostgreSQL v11.x). You can skip the config copying step if you are using `docker-compose` for starting postgresql and redis.

        # For docker use
        cp config/database.yml.docker config/database.yml
        # For non-docker local postgres setup use
        cp config/database.yml.example config/database.yml
        bundle exec rake db:setup

1. Start application

        bundle exec rails s

1. Login
    - Username: **admin@example.com**
    - Password: **12345678**


### With Rancher Desktop

If one does not have ability to use docker-compose, there is an alternative set up that relay
only on docker commands and can be used with open source tooling like [Rancher Desktop](https://rancherdesktop.io/)



1. Get source code

        git clone git@gitlab.com:gitlab-services/version-gitlab-com.git

1. Install gems

        bundle install

1. Docker based external services (an alternate way to utilize the database locally)

Download and install [rancher desktop](https://rancherdesktop.io/)

Use local bash script `bin/dependencies up` to start or `bin/dependencies down` to stop postgresql and redis instances.

This docker stack exposes the following services:
- PostgreSQL on port 9432
- Redis on port 6379

1. Setup database (using PostgreSQL v11.x). You can skip the config copying step if you are using `docker-compose` for starting postgresql and redis.

        cp config/database.yml.docker config/database.yml
        bundle exec rake db:setup

1. Start application

        bundle exec rails s

1. Login
    - Username: **admin@example.com**
    - Password: **12345678**

### With GitPod

To use the remote development environment, [GitPod](https://gitpod.io), simply click on the **Code** dropdown then `Open in GitPod`. Sign in with GitLab to be able to push commits back to gitlab.com

The environment will setup Postgres and Redis and start the rails server.

All configuration is in `.gitpod.yml`

### Updating your installation with upstream changes

        git checkout master
        git fetch
        git rebase
        bundle
        bundle exec rails db:migrate && bundle exec rails db:migrate RAILS_ENV=test

### Lefthook

[Lefthook](https://github.com/Arkweid/lefthook) is a Git hooks manager that allows custom logic to be executed prior to Git committing or pushing. Version App comes with Lefthook configuration (`lefthook.yml`), but it must be installed. Its second installation step - `bundle exec lefthook install` - also needs to be re-run after each update to the `lefthook.yml` file.

We have a `lefthook.yml` checked in but it is ignored until Lefthook is installed.

#### Install Lefthook

1. Install the `lefthook` Ruby gem:

   ```shell
   bundle install
   ```

1. Install Lefthook managed Git hooks:

   ```shell
   bundle exec lefthook install
   ```

1. Test Lefthook is working by running the Lefthook `prepare-commit-msg` Git hook:

   ```shell
   bundle exec lefthook run prepare-commit-msg
   ```

This should return a fully qualified path command with no other output.

#### Run Lefthook hooks manually

To run the `pre-push` Git hook, run:

   ```shell
   bundle exec lefthook run pre-push
   ```

## Contributing

All commits to this project must be signed with GPG. Please [set up GPG](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#how-gitlab-handles-gpg)
if you have not done so already.

[Reviewer roulette](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette) is used to suggest a
reviewer and maintainer for merge requests opened in this project.

## Testing

### Locally

#### Entire suite

```bash
 bundle exec rspec
```

#### Tests that need Redis

1. Install docker and docker-compose locally using brew
1. Install [docker desktop](https://docs.docker.com/docker-for-mac/install/)
1. Bring up the redis instance locally: `docker-compose up`
1. Run the redis based tests: `bundle exec rspec --tag redis`
1. Shutdown containers started with `docker-compose up`: `ctrl + c`

### Auto DevOps

1. Set a `BUILDPACK_URL` on the project as per [docs](https://docs.gitlab.com/ee/topics/autodevops/#multiple-buildpacks) to point at a custom [buildpack](https://gitlab.com/gitlab-org/version-app-buildpack).
   * [More info](https://docs.gitlab.com/ee/topics/autodevops/#build-and-deployment) on setting variables in CI.
1. Auto DevOps will use the provided `BUILDPACK_URL` to build the images for `build` and `test`, installing `chrome` in the `test` image for use by `rspec`.
1. Auto DevOps will then correctly detect a Ruby project and run `bundle exec rspec`.

### Staging

Note: before merging a MR and triggering a deployment to the staging environment make sure that the latest
staging deployment was signed off in the #version-gitlab.com Slack channel. By convention, we use a green tick mark (✅) to indicate that
changes were tested on staging. If the latest staging deployments is not verified, its recommended to reach out to MR authors.
This practice prevents the accidental deploying of untested changes from previous MRs to production. For more information, see [this example](https://gitlab.com/gitlab-org/gitlab-services/version.gitlab.com/-/merge_requests/41).

The [staging environment](https://gitlab-org-gitlab-services-version-gitlab-com-staging.version-staging.gitlab.org/users/sign_in) is available for testing. After verifying the changes on staging, mark the deploying message
in #version-gitlab.com Slack message with a green sign (✅). 

If an MR deployed to staging turns out to be faulty, then create, review, and merge a revert MR. 
After that MR has been verified on staging, you can mark both the faulty and the revert deployment with a green sign (✅).

## Production

### Deployment

Important: before deploying to production make sure there are no [unverified deployments](#staging) to staging left.
Check the #version-gitlab-com Slack channel and verify that all staging deployment have green ticks.
This is critical for the application stability, as it helps us to avoid deploying untested changes to production.

We currently utilize Auto DevOps for managing deployments manually.

We do not deploy in production on Fridays, before long holidays or before Friends & Family days, due to reduced availability.

#### Process

1. A merge request is merged to `master`.
1. The Auto DevOps pipeline then runs against `master` branch, which includes `Staging` and `Production` stages.
1. The `staging` application should then be reviewed manually (url in the `staging` job output) by the MR author as needed.
1. The `maintainer` reviewing the MR should prepare the deploy to production. In the event that the `maintainer` does not have the
availability to deploy to production, they can reach out for help from other `maintainers`.
1. There is a level of coordination, with other `maintainers` that should be performed in order to ensure only one deployment is happening at a time. This could be as simple as sending a notification in the `#version-gitlab-com` slack channel.
1. The `maintainer` will then need to go to that pipeline and deploy to `production` by triggering the `Production` jobs.

#### Sidekiq Behavior

As a general statement, during deployment, we expect no Sidekiq jobs to be missed or dropped.  As with typical
best practices, all Sidekiq jobs should be [idempotent](https://en.wikipedia.org/wiki/Idempotence).

* Active Jobs
   * All active jobs operate in a [reliable queue pattern](https://redis.io/commands/rpoplpush#pattern-reliable-queue) so that if the container running Sidekiq is killed during deployment
     or otherwise, the job will be enqueued for rerunning when a worker becomes available.  We achieve this
     behavior by utilizing this [gem](https://gitlab.com/gitlab-org/sidekiq-reliable-fetch/tree/master) with
     a concept referred to as [Reliable Fetch](https://github.com/TEA-ebook/sidekiq-reliable-fetch).
* Queued Jobs
   * Typical queue management with Sidekiq handles this scenario, and the job will run once a worker becomes
     available.
* Jobs created during/by deployment (Background Migrations)
   * Typically, these jobs will be enqueued in Sidekiq as normal and may fall into the *Active Jobs* scenario
     depending on timing of Sidekiq worker availability.
* Cron jobs
   * Scheduled cron jobs
   * Use [config/schedule.yml](config/schedule.yml) to add scheduled jobs

### Database Migrations

As a general statement, we follow [this guide](https://docs.gitlab.com/ee/development/what_requires_downtime.html) where applicable.

* During deployment, the database migration process has a timeout of [29 minutes](https://gitlab.com/gitlab-services/version-gitlab-com/-/blob/master/.gitlab-ci.yml#L7).
Therefore, any migration that could exceed that timeout should use strategies such as [Background Migrations](https://docs.gitlab.com/ee/development/background_migrations.html)
and those mentioned [here](https://docs.gitlab.com/ee/development/what_requires_downtime.html#changing-the-schema-for-large-tables).
This process will help us avoid scenarios like [this one](https://gitlab.com/gitlab-services/version-gitlab-com/-/issues/287).
   * Note: We have a custom Rubocop Cop, `Migration/UpdateLargeTable`, that will alert when tables that are updated qualify as
applying to this condition.
  * Adding indexes to `usage_data` or `version_checks`(`add_concurrent_index`):
     * Until we can increase the timeout through
https://gitlab.com/gitlab-org/gitlab/-/issues/227028, we need to disable the Cop for adding that index and be aware that
the deployment will run the migration, but fail to deploy the code due to the timeout. In that scenario, the mitigation
strategy is to rerun the deployment or run a subsequent deployment after the index has finished being added. The merge
request author should explain why the Cop is being disabled and what to expect.

#### Schema Changes

Changes to the schema should be committed to `db/schema.rb`. This file
is automatically generated by Rails, so you normally should not edit
this file by hand. If your migration is adding a column to a table,
that column will be added at the bottom. Please do not reorder columns
manually for existing tables as this will create problems.

When your local database is diverging from the schema from `master` it
might be hard to cleanly commit the schema changes to Git. In that
case you can use the `scripts/regenerate-schema` script to regenerate
a clean `db/schema.rb` for the migrations you're adding. This script
will apply all migrations found in `db/migrate`, so if there are any
migrations you don't want to commit to the schema, rename or remove
them. If your branch is not targeting `master` you can set the
`TARGET` environment variable.

```shell
# Regenerate schema against `master`
scripts/regenerate-schema

# Regenerate schema against `12-9-stable-ee`
TARGET=12-9-stable-ee scripts/regenerate-schema

```

If you have new migrations which are lower than the current schema
version you must provide the versions as parameters to the
script. This will ensure older migrations are reflected.

```shell
# Regenerate schema against `master`
scripts/regenerate-schema 20200809095922 20200811095842
```

#### Dropping columns

Follow [this guide](https://docs.gitlab.com/ee/development/what_requires_downtime.html#dropping-columns), omitting adding the `remove_with` field.

For example, to remove the `updated_at` in the Host model you'd use the following:

```ruby
class Host < ApplicationRecord
  include IgnorableColumns
  ignore_column :updated_at, remove_after: '2019-12-22'
end
```

#### Background Migrations

When and how to use these can be found [here](https://docs.gitlab.com/ee/development/background_migrations.html).

* Note: When introducing supporting code outside of the typical migration helper/file(worker), we need to have that code deployed
before it is invoked by a migration. This is due to how the code is deployed. Database migrations are the first thing
that runs on the new Docker image.  Therefore if a Sidekiq job is enqueued by a migration, it will be first enqueued on the
previously deployed Docker image and will need the worker/supporting code for that worker to be present.

## Fortune Companies Rank

The Fortune 500, and 1000 is an annual list compiled and published by Fortune
magazine that ranks the largest U.S. corporations by total revenue for their
respective fiscal years.

In order to keep the percentage of these companies that are using GitLab
updated, we need to keep our database updated. You can do that by:

1. Update the CSV file:

        lib/seeds/fortune_companies.csv

1. Import the Fortune companies ranking from CSV:

        bundle exec rake fortune_companies:import

1. Updates all hosts with current Fortune company ranking:

        bundle exec rake fortune_companies:process


The rank for new hosts will be automatically checked, and the reports will be
updated on the fly.

The CSV file should have three columns (`rank;company;domain`), and should not contain headers. We should have cases where the company we're looking up is a subsidiary of a Fortune company that has a different domain name, or has many associated domains. To fix this we just need to identify these relationships, and duplicate the entries in the CSV file with the different domains, but keeping the same rank.

**Example**:

```
1,Wal-Mart Stores,walmart.com
2,Exxon Mobil,exxonmobil.com
3,Chevron,chevron.com
4,Berkshire Hathaway,berkshirehathaway.com
5,Apple,apple.com
5,Apple,appleinc.com
```

## Service Ping

Service Ping is a process in GitLab that collects and sends a weekly payload
to GitLab Inc. The payload provides important high-level data that helps
our product, support, and sales teams understand how GitLab is used.

For more information, see the [Service Ping guide](https://docs.gitlab.com/ee/development/service_ping/index.html).

### Removing usage data for a host

In the event of a usage data deletion request, you can use a background data migration that removes all usage data and related records for given hostnames.

Generate a database migration and add the following:

```ruby
# frozen_string_literal: true

class RemoveUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  MIGRATION = 'DeleteUsageData'
  HOSTNAMES = ['subdomain1.example.com', 'example2.com'].freeze

  def up
    BackgroundMigrationWorker.perform_async(MIGRATION, [HOSTNAMES])
  end

  def down
    # no-op
  end
end
```

## Data export using pipeline schedules

A scheduled pipeline job [exports](https://gitlab.com/gitlab-services/version-gitlab-com/-/blob/master/scripts/data/export_data.sh) chosen database tables to a GCS bucket every day
using `gcloud` command line tools. The data is later imported to Snowflake.

You may manage the following configuration options in [CI / CD Schedules](https://gitlab.com/gitlab-services/version-gitlab-com/-/pipeline_schedules)
menu.

| Variable | Value | Default |
| ------ | ------ | ------ |
| GCLOUD_SERVICE_KEY | GCP [service account](https://console.cloud.google.com/iam-admin/serviceaccounts) auth JSON file name |  |
| GOOGLE_PROJECT_ID |  GCP project id ( API [sqladmin.googleapis.com] not enabled on project) ) | |
| GOOGLE_COMPUTE_ZONE | The region and zone for the GCP compute | |
| BUCKET | The GCS bucket where we shall upload the SQL export | |
| INSTANCE | Cloud SQL instance name | |
| DATABASE | Cloud SQL database name | |
| MAX_PAST_WEEKS_TO_EXPORT | How many weeks backwards to export | 10 |
| REEXPORT_EXISTING_FILES | Re-export files already existing in GCS bucket overriding them | 0 |
| SINGLE_TABLE_TO_EXPORT | A single table to export |  |
| VERBOSE | Set to 1 to output SQL & bucket details | 0  |

## Connecting to the production Google Cloud database

To connect to the production Google Cloud database:
1. Create an access request, requesting the access to the database ([example](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/20836))
1. [Download and install the Cloud SQL Auth proxy](https://cloud.google.com/sql/docs/mysql/sql-proxy#install)
1. Get the connection name: go to [Google Cloud console](https://console.cloud.google.com) and from the navigation menu, choose `SQL` and click on the instance id of the available instance. In the resulting view, you should be able to see a value labeled as `Connection name`.
1. Run the SQL proxy: `./cloud-sql-proxy conection_name`, replacing `conection_name` with the `Connection name` value
1. Find the user and password needed for database access: go to [CI/CD settings](https://gitlab.com/gitlab-org/gitlab-services/version.gitlab.com/-/settings/ci_cd) -> `Variables` and check the `DATABASE_URL` value. When matching this url in a ruby console by using `/postgres\:\/\/(.*):(.*)@.*/.match(url)`, the two resulting matches should be the database username and password respectively.
1. In another terminal window, connect to the proxy by running `psql --host=127.0.0.1 --username=username` (replacing the second "username" with the username grabbed from the CI/CD variables). A prompt will ask for the password. After you pass it, you should be able to run SQL queries on the database.

## Testing Salesforce integrations

To be able to test the Salesforce integration, an account in Salesforce sandbox is required. An example access request for that kind of permissions can be found [here](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/20327).

After getting the permissions, it's necessary to create an application in the Salesforce sandbox and get its `client_id` and `client_secret` values. To do so:

1. In the top menu, click on `Setup` > `Setup` > `Platform Tools` > `Apps` > `App Manager` > `New Connected App`.
1. In the app creation form, make sure to enable the `Enable OAuth Settings` checkbox.
1. The `Callback URL` entry can be filled in with any URL.
1. After creating the app, click on `Manage Consumer Details` to see the `client_id` ("Consumer Key") and `client_secret` ("Consumer Secret").

To get a security token, it needs to be [reset](https://help.salesforce.com/s/articleView?id=sf.user_security_token.htm&type=5).

After getting all of that data, copy the `.env.example` file into a `.env` file and fill out all of the environment variables with values retrieved from Salesforce.

Now, after opening a new `rails console`, you should be able to connect to the sandbox Salesforce instance.
