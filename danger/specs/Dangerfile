# frozen_string_literal: true

NO_SPECS_LABELS = %w[
  type::tooling
  tooling::pipelines
  tooling::workflow
  documentation
  QA
].freeze

NO_NEW_SPEC_MESSAGE = <<~MSG
You've made some app changes, but didn't add any tests.
That's OK as long as you're refactoring existing code,
but please consider adding any of the %<labels>s labels.
MSG

CONTROLLER_SPEC_DEPRECATION_MESSAGE = <<~MSG
Do not add new controller specs. We are moving from controller specs to
request specs (and/or feature specs). Please add request specs under
`/spec/requests` instead.

See https://gitlab.com/groups/gitlab-org/-/epics/5076 for information.
MSG

all_changed_files = helper.all_changed_files
has_app_changes = all_changed_files.grep(%r{\A(app|lib|db/migrate)/}).any?
spec_changes = specs.changed_specs_files
has_spec_changes = spec_changes.any?
new_specs_needed = (gitlab.mr_labels & NO_SPECS_LABELS).empty?

if has_app_changes && !has_spec_changes && new_specs_needed
  warn format(NO_NEW_SPEC_MESSAGE, labels: helper.labels_list(NO_SPECS_LABELS)), sticky: false
end

# Forbidding a new file addition under `/spec/controllers`
if helper.changes.added.files.grep(%r{^spec/controllers/}).any?
  warn CONTROLLER_SPEC_DEPRECATION_MESSAGE
end

specs.changed_specs_files.each do |filename|
  specs.add_suggestions_for_match_with_array(filename)
end

warn("Before merging MR make sure there are no uncleared staging deployment messages in the #version-gitlab-com Slack channel.
 See https://gitlab.com/gitlab-org/gitlab-services/version.gitlab.com/-/blob/main/README.md?ref_type=heads#staging")
warn("After merging and verifying these changes on staging, apply a green tick mark in the #version-gitlab-com Slack channel to
unblock future MR from being merged.")
