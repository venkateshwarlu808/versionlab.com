# frozen_string_literal: true

class ConversationalDevelopmentIndex < ApplicationRecord
  belongs_to :usage_data

  before_save :calculate_percentages

  METRICS = %w[
    boards
    ci_pipelines
    deployments
    environments
    issues
    merge_requests
    milestones
    notes
    projects_prometheus_active
    service_desk_issues
  ].freeze

  METRICS_TITLES = {
    ci_pipelines: 'Pipelines',
    notes: 'Comments',
    projects_prometheus_active: 'Monitoring',
    service_desk_issues: 'Service Desk'
  }.freeze

  def calculate_percentages
    METRICS.each do |metric|
      next if leader_score(metric) == 0.0

      percentage = ((instance_score(metric) / leader_score(metric)) * 100)
      assign_attributes("percentage_#{metric}" => percentage)
    end
  end

  def to_json(*_args)
    result = {}
    METRICS.each do |metric|
      title = METRICS_TITLES[metric.to_sym] || metric.titleize

      result[title] = {
        'instance_score' => instance_score(metric),
        'leader_score' => leader_score(metric),
        'percentage_score' => percentage_score(metric)
      }
    end

    result.to_json
  end

  def instance_score(metric)
    self["instance_#{metric}"].to_f
  end

  def leader_score(metric)
    self["leader_#{metric}"].to_f
  end

  def percentage_score(metric)
    self["percentage_#{metric}"].to_f.round(1)
  end
end
