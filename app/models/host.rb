# frozen_string_literal: true

require 'csv'

class Host < ApplicationRecord
  validates :url, presence: true, uniqueness: true # rubocop:disable Rails/UniqueValidationWithoutIndex

  has_one :current_host_stat

  has_many :usage_datum, class_name: 'UsageData'
  has_many :usage_ping_errors

  scope :starred, -> { where(star: true) }
  scope :fortune_companies, -> { where.not(fortune_rank: nil).order(fortune_rank: :asc) }
  scope :fortune500, -> { fortune_companies.where('fortune_rank <= ?', 500) }
  scope :fortune1000, -> { fortune_companies.where('fortune_rank <= ?', 1000) }

  class << self
    def find_or_create_for(url)
      host = find_or_create_by(url: url)

      unless host.fortune_ranked?
        fortune = Gitlab::FortuneFinder.new(host.url) # rubocop:disable CodeReuse/Finder
        host.update_column(:fortune_rank, fortune.rank) if fortune.ranked? # rubocop:disable Rails/SkipsModelValidations
      end

      host
    end

    def fortune500_ranking
      fortune500.pluck(:fortune_rank).distinct
    end

    def fortune500_percentage
      (fortune500_ranking.count * 100.00) / 500
    end

    def fortune1000_ranking
      fortune1000.pluck(:fortune_rank).distinct
    end

    def fortune1000_percentage
      (fortune1000_ranking.count * 100.00) / 1000
    end

    def to_csv
      attributes = %w[id url created_at updated_at star fortune_rank]

      CSV.generate(headers: true) do |csv|
        csv << attributes

        all.find_each do |host|
          csv << attributes.map { |attr| host.public_send(attr) } # rubocop:disable GitlabSecurity/PublicSend
        end
      end
    end
  end

  def fortune_ranked?
    fortune_rank.present?
  end
end
