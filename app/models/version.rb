# frozen_string_literal: true
class Version < ApplicationRecord
  PERMITTED_VERSION = /\d+\.\d+\.\d+/.freeze

  has_many :features
  has_many :usage_ping_errors

  enum vulnerability_type: { not_vulnerable: 0, non_critical: 1, critical: 2 }

  validates :version,
            presence: true,
            uniqueness: true,
            format: { with: /\A#{PERMITTED_VERSION}\Z/, message: "should be X.X.X" }
  validates :details, length: { maximum: 2048 }

  scope :most_recent, -> { order(id: :desc) }
  scope :most_recent_by_version, -> { order(Arel.sql("string_to_array(version, '.')::integer[] DESC")) }
  scope :within, ->(version) { where("version LIKE ?", ParsedVersion.new(version).to_query) }

  # When :security_release_type is marked as :non_critical or :critical
  #   :mark_previous_versions_vulnerable will execute logic to mark previous
  #   versions with the correct vulnerability_type.
  attr_accessor :security_release_type

  before_save :mark_previous_versions_vulnerable

  def self.resolve!(version_or_id)
    version_or_id.to_s.start_with?('v') ? latest(version_or_id, raise_exception: true) : find(version_or_id)
  end

  def self.latest_minor_releases_after(count:, after_version:)
    latest_minor_releases(count).select do |version_object|
      VersionSorter.compare(version_object.version, after_version) == 1
    end
  end

  def self.latest_minor_releases(count)
    versions = Version.pluck(:version)

    # find the highest patch version for each minor version
    last_feature_versions = ::VersionSorter.rsort(versions).uniq do |version|
      version.split('.').take(2)
    end.take(count)

    VersionSorter.rsort(Version.where(version: last_feature_versions).to_a, &:version)
  end

  def self.latest(version = nil, raise_exception: false)
    scope = version ? Version.within(version) : Version
    last_version = VersionSorter.sort(scope.pluck(:version)).last # rubocop:disable Style/RedundantSort

    if raise_exception
      Version.find_by!(version: last_version)
    else
      Version.find_by(version: last_version)
    end
  end

  def self.latest?(version)
    latest_version = latest.version
    begin
      parsed = ParsedVersion.new(version)
      VersionSorter.compare(parsed.to_s, ParsedVersion.new(latest_version).to_s(parsed.precision)) >= 0
    rescue ParsedVersion::InvalidVersionError
      VersionSorter.compare(version, latest_version) >= 0
    end
  end

  def major
    parsed_version.major.to_i
  end

  def minor
    parsed_version.minor.to_i
  end

  private

  def parsed_version
    ParsedVersion.new(version)
  end

  def mark_previous_versions_vulnerable
    return unless security_patch?

    previous_versions = Version.within(parsed_version.to_s(:minor))

    case security_release_type
    when 'critical'
      previous_versions.update_all(vulnerability_type: security_release_type) # rubocop:disable Rails/SkipsModelValidations
    when 'non_critical'
      previous_versions.where.not(vulnerability_type: :critical).update_all(vulnerability_type: security_release_type) # rubocop:disable Rails/SkipsModelValidations
    end
  end

  def security_patch?
    return false if security_release_type.blank?

    security_release_type != 'not_vulnerable'
  end
end
