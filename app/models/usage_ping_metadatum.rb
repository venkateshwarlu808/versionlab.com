# frozen_string_literal: true

class UsagePingMetadatum < ApplicationRecord
  validates :uuid, length: { maximum: 255 }
  validates :metrics, presence: true
end
