# frozen_string_literal: true

class CurrentHostStat < ApplicationRecord
  include Sortable
  include UserCounters

  belongs_to :host

  scope :with_host, -> { includes(:host).references(:host) }
  scope :with_usage_datum, -> { where.not(usage_data_recorded_at: nil) }
  scope :starred, -> { with_host.merge(Host.starred) }
  scope :fortune_companies, -> { with_host.merge(Host.fortune_companies) }
  scope :by_license, ->(md5) { where(license_md5: md5) }
  scope :by_edition, ->(edition) { where(edition: edition) }
  scope :by_url, ->(url) { where(arel_table[:url].matches("%#{sanitize_sql_like(url)}%")) }

  delegate :fortune_rank, to: :host

  class << self
    def to_csv(view_context)
      csv_attributes = %w[
        host_id host_link active_users_count historical_max_users_count user_count version
        usage_data_recorded_at edition
      ]
      CSV.generate(headers: true) do |csv|
        csv << csv_attributes

        all.csv_limit.each do |current_host_stat| # Not using find_each to preserve applied limit and sorting
          csv << csv_attributes.map do |attr|
            if current_host_stat.attributes.include?(attr)
              current_host_stat.attributes[attr]
            elsif attr == 'host_link' && view_context.respond_to?(:host_url)
              view_context.host_url(current_host_stat.host)
            end
          end
        end
      end
    end

    def csv_limit
      limit(1000)
    end

    def find_or_create_for_usage_data!(host, usage_data)
      safe_find_or_create_by!(url: host.url) do |record|
        record.host_id = host.id
        record.host_created_at = host.created_at
        record.usage_data_recorded_at = usage_data.recorded_at
        record.active_users_count = usage_data.active_user_count
        record.historical_max_users_count = usage_data.historical_max_users
        record.user_count = usage_data.license_user_count
        record.license_md5 = usage_data.license_md5
        record.version = usage_data.version
        record.edition = usage_data.edition
      end
    end
  end

  def customers_account?
    zuora_account_id.present?
  end
end
