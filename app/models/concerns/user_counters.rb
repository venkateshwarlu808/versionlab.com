# frozen_string_literal: true

module UserCounters
  extend ActiveSupport::Concern

  def usage_users
    return unless has_user_count?

    historical_max_users_count - user_count
  end

  def has_user_count?
    user_count&.positive?
  end

  def over?
    return unless has_user_count?

    historical_max_users_count > user_count
  end
end
