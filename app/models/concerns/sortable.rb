# frozen_string_literal: true

module Sortable
  extend ActiveSupport::Concern

  InvalidSortDirection = Class.new(StandardError)
  InvalidSortColumn = Class.new(StandardError)

  class_methods do
    def order_by(sort_column, sort_direction)
      validate_sort_direction(sort_direction)

      if respond_to?("order_by_#{sort_column}")
        public_send("order_by_#{sort_column}", sort_direction) # rubocop:disable GitlabSecurity/PublicSend
      else
        validate_sort_column(sort_column)

        order("#{table_name}.#{sort_column} #{sort_direction} NULLS LAST")
      end
    end

    private

    def validate_sort_direction(sort_direction)
      return if %w[ASC DESC].include?(sort_direction.upcase)

      Rails.logger.warn("validate_sort_direction, invalid sort_direction: #{sort_direction}")
      raise InvalidSortDirection, "sort_direction not permitted. Valid values are 'asc' and 'desc'."
    end

    def validate_sort_column(sort_column)
      return if column_names.include?(sort_column.to_s)

      Rails.logger.warn("validate_sort_column, invalid sort_column: #{sort_column}")
      raise InvalidSortColumn, "sort_column not permitted."
    end
  end
end
