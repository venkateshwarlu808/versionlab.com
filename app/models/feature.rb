# frozen_string_literal: true

class Feature < ApplicationRecord
  belongs_to :version

  validates :title, :description, presence: true

  scope :for_version, ->(version) { where(version_id: Version.within(version).pluck(:id)) }
  scope :since, ->(time) { where(created_at: time..DateTime::Infinity.new) }
  scope :with_roles, ->(roles) { where('applicable_roles && ARRAY[?]::varchar[]', roles) }
  scope :with_version, -> { includes(:version) }
  scope :ordered, -> { order(created_at: :desc) }
end
