# frozen_string_literal: true

class VersionCheck
  include ActiveModel::Model
  include ActiveModel::Validations

  attr_accessor :gitlab_version, :request_data, :referer_url, :host

  validates :gitlab_version, :request_data, :referer_url, presence: true

  def up_to_date?
    Version.latest?(general_gitlab_version)
  end

  def update_required?
    Version.where(version: general_gitlab_version)
      .where.not(vulnerability_type: Version.vulnerability_types[:not_vulnerable]).any?
  end

  def critical_vulnerability?
    Version.where(version: general_gitlab_version)
      .where(vulnerability_type: Version.vulnerability_types[:critical]).any?
  end

  def general_gitlab_version
    gitlab_version.sub(/-ee\Z/, '')
  end
end
