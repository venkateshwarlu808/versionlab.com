# frozen_string_literal: true

class UsagePingError < ApplicationRecord
  validates :uuid, :message, presence: true

  belongs_to :host
  belongs_to :version
end
