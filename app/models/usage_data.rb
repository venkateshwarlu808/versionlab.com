# frozen_string_literal: true

class UsageData < ApplicationRecord
  serialize :licensee, JSON # rubocop:disable Cop/ActiveRecordSerialize
  serialize :license_add_ons, JSON # rubocop:disable Cop/ActiveRecordSerialize

  belongs_to :host, optional: true
  belongs_to :raw_usage_data, optional: true

  has_one :conversational_development_index
  has_one :avg_cycle_analytics, class_name: 'AvgCycleAnalytics', inverse_of: :usage_data

  scope :related_weekly_user_count_by_uuid, (lambda do |uuid|
    where(uuid: uuid).group_by_week(:created_at).maximum(:active_user_count)
  end)
  scope :related_stats_by_week, (lambda do |uuid, host_id, metric|
    where(uuid: uuid, host_id: host_id)
      .group_by_week(:recorded_at)
      .maximum(sanitize_sql_for_conditions(["(stats::json->>:metric)::int", metric: metric]))
  end)
  scope :recent_counts_by_month_edition, (lambda do
    select("date_trunc('month', recorded_at) as month, COALESCE(edition, 'EE') as computed_edition, COUNT(*) as count")
      .where(UsageData.arel_table[:recorded_at].gt(6.months.ago))
      .group(:month, :computed_edition)
      .order(month: :desc)
  end)
  scope(:ordered_and_paginated, ->(page, per) { order(id: :desc).page(page).per(per) })
  scope :ordered_by_id_with_limit, ->(rows) { order(id: :desc).limit(rows) }
  scope :by_uuid_with_recorded_limits, (lambda do |uuid, time_range|
    where(uuid: uuid, recorded_at: time_range).order(:recorded_at)
  end)
  scope :ids_by_edition_with_created_limits, (lambda do |edition, time_range|
    where(created_at: time_range, edition: edition).ids
  end)
  scope :by_uuid, ->(uuid) { where(uuid: uuid) }
  scope :uniq_gitlab_versions, -> { pluck(:version).uniq.reject(&:blank?) }

  CE_EDITION = 'CE'
  EE_EDITION = 'EE'
  EE_EDITIONS = ['EE Free', 'EES', 'EE', 'EEP', 'EEU'].freeze
  EDITIONS = ([CE_EDITION] + EE_EDITIONS).freeze

  def initialize(data = nil)
    data = clean_data(data || {})

    super(data)
  end

  def licensee_company
    licensee&.dig('Company')
  end

  def licensee_name
    licensee&.dig('Name')
  end

  def licensee_email
    licensee&.dig('Email')
  end

  def usage_users
    return unless has_user_count?

    historical_max_users - license_user_count
  end

  def over?
    has_user_count? && (historical_max_users > license_user_count)
  end

  def has_user_count?
    license_user_count.to_i.positive?
  end

  # Not every service ping has an edition, because this was only added when we
  # added CE service pings. So `nil` always means EE, and we don't know the exact
  # edition.
  def edition
    super || EE_EDITION
  end

  def ce?
    edition == CE_EDITION
  end

  def ee?
    !ce?
  end

  def related_weekly_user_count_by_uuid
    self.class.related_weekly_user_count_by_uuid(uuid)
  end

  private

  def clean_data(data)
    # Fix typo in 8.12.0
    if data.dig(:stats, :protected_branchess)
      data['stats']['protected_branches'] = data['stats'].delete('protected_branchess')
    end

    data
  end
end
