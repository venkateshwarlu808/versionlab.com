# frozen_string_literal: true

class FortuneCompany < ApplicationRecord
  validates :company, :domain, :rank, presence: true
  validates :domain, uniqueness: { case_sensitive: false }
end
