# frozen_string_literal: true

class AvgCycleAnalytics < ApplicationRecord
  belongs_to :usage_data, inverse_of: :avg_cycle_analytics
end
