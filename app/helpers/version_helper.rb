# frozen_string_literal: true

module VersionHelper
  def vulnerability_type_options_for_select
    Version.vulnerability_types.keys.map { |k| [vulnerability_type_ui_label(k), k] }
  end

  private

  def vulnerability_type_ui_label(key)
    if key == 'not_vulnerable'
      'No'
    else
      key.titleize
    end
  end
end
