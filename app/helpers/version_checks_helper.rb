# frozen_string_literal: true

module VersionChecksHelper
  def headers(request_data)
    JSON.parse(request_data)
  end

  # host_url is a route
  def host_url_for_display(host)
    host&.url || '-'
  end
end
