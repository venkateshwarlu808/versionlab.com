# frozen_string_literal: true

module CurrentHostStatsHelper
  def search_sortable_column(column:, title: nil, default_order: 'asc')
    title, column, direction, css_class = calculate_sorting(column, title, default_order)

    link_to title,
            {
              sort: column,
              direction: direction
            }.merge(search_params)
              .merge(applied_filters),
            {
              class: css_class
            }
  end

  def search_params
    {
      search: params[:search],
      search_type: params[:search_type]
    }.compact
  end

  def applied_filters
    {
      starred: params[:starred],
      fortune_rank: params[:fortune_rank],
      edition: params[:edition]
    }.compact
  end

  def filter_params
    {
      sort: sort_column,
      direction: sort_direction
    }.merge(search_params)
  end

  def dropdown_edition_option_class(edition)
    ['dropdown-item', (:active if params[:edition] == edition)]
  end

  def edition_filter_text
    params[:edition] || 'All'
  end

  def dropdown_button_class
    [:btn, 'dropdown-toggle', "btn-#{'outline-' if params[:starred] || params[:fortune_rank]}secondary"]
  end

  def dropdown_all_editions_option_class
    ['dropdown-item', (:active unless params[:edition])]
  end

  def starred_link_class
    [:btn, "btn-#{'outline-' unless params[:starred]}secondary"]
  end

  def fortune_rank_link_class
    [:btn, "btn-#{'outline-' unless params[:fortune_rank]}secondary"]
  end

  def licensed_user_count(record)
    count_text = record.user_count.presence || 'unknown'

    link_to_if(
      record.customers_account?,
      count_text,
      "#{Rails.configuration.application.customers_admin_url}/billing_account?query=#{record.zuora_account_id}",
      {
        data: { toggle: 'tooltip' },
        title: 'Link to admin this billing account on the Customers Application.'
      }
    )
  end
end
