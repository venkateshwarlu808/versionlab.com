# frozen_string_literal: true

class VersionsController < ApplicationController
  before_action :set_version, only: [:edit, :update, :destroy]

  def index
    @versions = Version.most_recent_by_version
    @versions = @versions.page(params[:page])
  end

  def new
    @version = Version.new
    @version.version = params[:version] if params[:version].present?
  end

  def edit; end

  def create
    @version = Version.new(version_params)

    respond_to do |format|
      if @version.save
        format.html { redirect_to versions_path, notice: 'Version was successfully created.' }
        format.json { render :show, status: :created, location: @version }
      else
        format.html { render :new }
        format.json { render json: @version.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @version.update(version_params)
        format.html { redirect_to versions_path, notice: 'Version was successfully updated.' }
        format.json { render :show, status: :ok, location: @version }
      else
        format.html { render :edit }
        format.json { render json: @version.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @version.destroy

    respond_to do |format|
      format.html { redirect_to versions_path, status: :found, notice: 'Version was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_version
    @version = Version.find(params[:id])
  end

  def version_params
    params.require(:version).permit(:version, :vulnerability_type, :security_release_type, :details)
  end
end
