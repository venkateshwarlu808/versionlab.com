# frozen_string_literal: true
class StatisticsController < ApplicationController
  def show
    @counts = {
      hosts: Host.count,
      current_host_stats: CurrentHostStat.count,
      current_host_stats_with_host: CurrentHostStat.with_host.count,
      current_host_stats_with_host_and_usage_datum: CurrentHostStat.with_host.with_usage_datum.count
    }
  end
end
