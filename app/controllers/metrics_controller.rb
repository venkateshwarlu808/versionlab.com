# frozen_string_literal: true

class MetricsController < ApplicationController
  def index
    @usage_statistics = CalculateUsageStatisticsService.new.execute
    @instance_counts = CalculateInstanceCountsService.new.execute
  end

  def show
    @metric_usage = UsageData.related_stats_by_week(params[:uuid], params[:host_id], params[:id])
    @metric = params[:id]
  end
end
