# frozen_string_literal: true

class UsageDataController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :create
  skip_before_action :authenticate_user!, only: [:create, :new]
  before_action :set_default_json_format, only: :create

  def new; end

  def show
    @usage_data =
      if /^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$/.match?(params[:id])
        UsageData.by_uuid(params[:id]).ordered_by_id_with_limit(1).last
      else
        UsageData.find(params[:id])
      end
  end

  def create
    if (uploaded_file = params[:service_usage_data_file])
      data = JSON.parse(uploaded_file.read)
      params[:usage_datum] = data
    end

    raw_usage_data = save_original_usage_ping

    response = UsagePing::ServiceHandler.new.execute(
      usage_data_params.to_h, request.remote_ip, raw_usage_data&.id
    )

    track_event(upload_successful: true)

    respond_to do |format|
      format.html do
        redirect_to new_usage_datum_path, notice: 'Service usage data was successfully uploaded.'
      end
      format.json { render json: response, status: :created }
    end

  rescue ActiveRecord::RecordInvalid, UsagePing::StatsParser::NonIntegerError
    track_event(upload_successful: false)

    respond_to do |format|
      format.json { render json: {}, status: :unprocessable_entity }
    end
  rescue StandardError
    track_event(upload_successful: false)

    flash[:upload_failed] = true
    respond_to do |format|
      format.html { redirect_to new_usage_datum_path, alert: 'There was an error processing uploaded file.' }
    end
  end

  def months
    @month_counts = UsageDataByMonthService.new.execute
  end

  def schedule_backfill
    count = Salesforce::BackfillService.new(params[:month_editions]).execute
    flash[:notice] = "Scheduled #{count} jobs"

    redirect_to months_usage_data_path
  end

  private

  # ServicePing gets submitted without `Accept: "application/json"` in the header.
  # To not break behaviour for GitLab instances that expect a JSON response, we default to JSON.
  def set_default_json_format
    request.format = :json unless request.media_type == Mime[:multipart_form]
  end

  def usage_data_params
    filtered_payload.permit(*usage_param_keys).tap do |whitelisted|
      # Whitelist entire hashes
      # https://github.com/rails/rails/blob/v4.2.7/guides/source/action_controller_overview.md#outside-the-scope-of-strong-parameters
      whitelisted_hash_keys.each do |key|
        whitelisted[key] = filtered_payload[key].permit! if filtered_payload[key].present?
      end

      # Even if we change the payload that GitLab EE is sending,
      # we need to handle both and will have to do so forever to
      # support old GitLab installations.
      whitelisted[:stats] = filtered_payload[:counts].permit! if filtered_payload[:counts].present?
    end
  end

  def usage_param_keys
    UsagePing::USAGE_DATA_PARAMS +
      UsagePing::FEATURES_PARAMS
  end

  def whitelisted_hash_keys
    UsagePing::COMPONENTS_PARAMS +
      UsagePing::CYCLE_ANALYTICS_PARAMS +
      UsagePing::LICENSE_PARAMS +
      UsagePing::SYSTEM_USAGE_PARAMS +
      UsagePing::SMAU_PARAMS +
      UsagePing::OBJECT_STORE_PARAMS +
      UsagePing::TOPOLOGY_PARAMS +
      UsagePing::SETTINGS_PARAMS +
      UsagePing::UNIQUE_VISITS_PARAMS
  end

  def save_original_usage_ping
    RawUsageData.create(
      uuid: filtered_payload[:uuid],
      recorded_at: filtered_payload[:recorded_at],
      payload: filtered_payload,
      raw_usage_data_payload: params[:usage_datum]
    )
  end

  def filtered_payload
    return @filtered_payload if defined?(@filtered_payload)

    usage_params = UsagePing::FixParams.new.execute(params[:usage_datum])
    @filtered_payload = UsagePing::FilterUsageDataService.new(usage_params).execute
  end

  def track_event(event_data)
    return unless params[:service_usage_data_file] # we only track manual uploads

    EventTracker.new.track("service_ping_manual_upload", event_data)
  end
end
