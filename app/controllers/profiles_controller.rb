# frozen_string_literal: true

class ProfilesController < ApplicationController
  before_action :set_user

  def show
    @user = current_user
  end

  def reset_private_token
    flash[:notice] = "Token was successfully updated." if current_user.reset_private_token!

    redirect_to profile_path
  end

  private

  def set_user
    @user = current_user
  end

  def user_params
    params.require(:user).permit(:current_password, :password, :password_confirmation)
  end
end
