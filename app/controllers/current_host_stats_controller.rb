# frozen_string_literal: true

class CurrentHostStatsController < ApplicationController
  helper_method :sort_column,
                :sort_direction

  def index
    @current_host_stats = CurrentHostStat.with_host.with_usage_datum
    @current_host_stats = @current_host_stats.starred if params[:starred].present?
    @current_host_stats = @current_host_stats.fortune_companies if params[:fortune_rank].present?
    @current_host_stats = @current_host_stats.by_edition(params[:edition]) if params[:edition].present?
    @current_host_stats = by_search_type if search_applied?
    @current_host_stats = @current_host_stats.order_by(sort_column, sort_direction)

    respond_to do |format|
      format.html do
        @current_host_stats = @current_host_stats.page(params[:page]).per(params[:per])
      end
      format.csv do
        send_data @current_host_stats.to_csv(view_context), filename: "version-gitlab-hosts-#{Date.today}.csv"
      end
    end
  end

  private

  def search_applied?
    params[:search].present?
  end

  def by_search_type
    if license_search?
      @current_host_stats.by_license(params[:search])
    else
      @current_host_stats.by_url(params[:search])
    end
  end

  def license_search?
    params[:search_type] == 'License MD5'
  end

  def sort_column
    params[:sort] || 'url'
  end

  def sort_direction
    params[:direction] || 'desc'
  end
end
