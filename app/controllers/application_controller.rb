# frozen_string_literal: true

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!
  before_action :set_sentry_context
  before_action :set_gon_variables

  rescue_from Sortable::InvalidSortColumn, with: :render_422_with_exception
  rescue_from Sortable::InvalidSortDirection, with: :render_422_with_exception

  private

  def set_sentry_context
    return unless Rails.env.production?

    Sentry.set_user(user_id: current_user.try(:id), user_email: current_user.try(:email))
    Sentry.set_extras(params: params.to_unsafe_h, url: request.url)
  end

  def render_422_with_exception(exception)
    @message = exception.message || 'Unable to display this page'
    render 'shared/error_message.html.haml',
           layout: 'application',
           status: :unprocessable_entity,
           content_type: "text/html"
  end

  def set_gon_variables
    gon.browser_sdk_app_id = ENV['BROWSER_SDK_APP_ID']
    gon.browser_sdk_host = ENV['BROWSER_SDK_HOST']
    gon.browser_sdk_enabled = ENV['BROWSER_SDK_ENABLED']
  end
end
