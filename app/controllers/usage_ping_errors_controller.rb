# frozen_string_literal: true

class UsagePingErrorsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :create
  skip_before_action :authenticate_user!, only: :create

  def create
    UsagePing::ErrorHandler.new.execute!(error_params)

    head :created
  end

  private

  def error_params
    params.require(:error).permit(:uuid, :hostname, :version, :message, :elapsed, :time)
  end
end
