# frozen_string_literal: true

class HostsController < ApplicationController
  before_action :set_host

  def show
    @versions = VersionSorter.rsort(@host.usage_datum.uniq_gitlab_versions)
    @usage_ping_data = @host.usage_datum.ordered_and_paginated(params[:page], params[:per])
  end

  def update
    @host.update(host_params)

    head :ok
  end

  private

  def set_host
    @host = Host.find(params[:id])
  end

  def host_params
    @host_params ||= params[:host].permit(:star)
  end
end
