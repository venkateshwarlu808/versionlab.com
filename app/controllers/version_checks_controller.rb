# frozen_string_literal: true

class VersionChecksController < ApplicationController
  LAST_VERSIONS_SHOWN_COUNT = 3

  skip_before_action :authenticate_user!, only: :check

  def check
    return head(404) unless request.env['HTTP_REFERER'].present?

    begin
      service = VersionCheckCreatorService.new(params[:gitlab_info], request)
      version_check_result = service.execute
    rescue ArgumentError
      return head(404)
    end

    respond_to do |format|
      format.png do
        send_file File.realpath("public/update_#{version_check_result[:status]}.png"),
                  type: 'image/png', disposition: 'inline'
      end

      format.svg do
        send_file File.realpath("public/update_#{version_check_result[:status]}.svg"),
                  type: 'image/svg+xml', disposition: 'inline'
      end

      format.json do
        if version_check_result[:status] == 'failed'
          render json: {}, status: :bad_request
        else
          latest_version = Version.latest
          render json: {
            latest_stable_versions: latest_stable_versions,
            latest_version: latest_version.version,
            severity: version_check_result[:status],
            critical_vulnerability: version_check_result[:critical_vulnerability],
            details: latest_version.details
          }
        end
      end
    end
  end

  private

  def latest_stable_versions
    Version.latest_minor_releases_after(
      count: LAST_VERSIONS_SHOWN_COUNT,
      after_version: GitlabInfoDecoder.decode(params[:gitlab_info])['version']
    ).map(&:version)
  end
end
