# frozen_string_literal: true

module Salesforce
  class BackfillService < BaseService
    attr_reader :month_editions

    def initialize(month_editions)
      @month_editions = month_editions
    end

    def execute
      return 0 if month_editions.blank?

      scheduled = 0

      month_editions.each do |month_edition|
        next unless month_edition.match?(/^\d{4}-\d{2}-[A-Z]+$/)

        year, month, edition = month_edition.split('-')

        # Not every service ping has an edition, because this was only added when
        # we added CE service pings. So `nil` always means EE, and we don't know
        # the exact edition.
        edition = [nil, 'EE'] if edition == 'EE'
        start_time = Time.utc(year, month)
        end_time = start_time + 1.month

        ids = UsageData.ids_by_edition_with_created_limits(edition, start_time...end_time)

        scheduled += UsageDataWorker.bulk_perform_async(ids.map { |id| [id] }).count
      end

      scheduled
    end
  end
end
