# frozen_string_literal: true

module Salesforce
  class PushUsageDataService < BaseService
    attr_reader :data, :params

    def initialize(usage_data, params)
      @data = usage_data
      @params = params
    end

    def execute
      return unless params[:account_id].present?

      create_usage_data_for_account!
    end

    class << self
      def param_name(type, fieldname)
        fieldname = Salesforce::METRICS_MAPPINGS[fieldname.to_sym] || fieldname

        "#{type}_#{fieldname}__c".to_sym
      end

      # Map the input count field to SalesForce naming scheme
      #  data          - A source hash
      #  type          - Type of ping (:count or :stats)
      #
      def map_fields(data, type)
        Hash[data.map { |field, value| [param_name(type, field), value || 0] }]
      end

      def whitelist_fields(data)
        extra_fields = data.except(*Salesforce::USAGE_PING_WHITELIST)
        Rails.logger.warn("Trying to push extraneous fields: #{extra_fields}") if extra_fields.any?

        Hash[data.except(*extra_fields.keys)]
      end
    end

    private

    def create_usage_data_for_account!
      create_params = usage_data_params.merge(
        zuora_subscription_id__c: params[:zuora_subscription_id],
        Account__c: params[:account_id]
      )

      create_usage_data!(create_params)
    end

    def create_usage_data!(create_params)
      create!('Usage_Ping_Data__c', create_params)
    rescue Faraday::ClientError => e
      if e.message.to_s.starts_with?('DUPLICATE_VALUE')
        Rails.logger.info("Service ping #{data.id} already exists in Salesforce")
        return true
      end

      if /\AINVALID_FIELD: No such column '(?<missing_column>\w+)'/ =~ e.message
        Rails.logger.info("Service ping #{data.id} no such column '#{missing_column}'")
      end

      raise e
    end

    # rubocop:disable Metrics/AbcSize
    def usage_data_params
      @usage_data_params ||= {
        active_user_count__c: data.active_user_count,
        conv_dev_index__c: json_field(data.conversational_development_index),
        counts__c: json_field(data.stats),
        created_at__c: date_field(data.created_at),
        edition__c: data.edition,
        historical_max_users__c: data.historical_max_users,
        hostname__c: data.hostname,
        installation_type__c: data.installation_type || "",
        license_add_ons__c: data.license_add_ons ? data.license_add_ons.to_json : '',
        license_expires_at__c: date_field(data.license_expires_at),
        license_md5__c: data.license_md5,
        license_sha256__c: data.license_sha256,
        license_restricted_count__c: data.license_restricted_user_count,
        license_starts_at__c: date_field(data.license_starts_at),
        license_user_count__c: data.license_user_count,
        license_trial__c: data.license_trial?,
        licensee__c: data.licensee ? data.licensee.to_json : '',
        mattermost_enabled__c: !!data.mattermost_enabled,
        recorded_at__c: date_field(data.recorded_at),
        source_ip__c: data.source_ip,
        updated_at__c: date_field(data.updated_at),
        usage_data_id__c: data.id,
        uuid__c: data.uuid,
        version__c: data.version
      }

      @usage_data_params.merge!(self.class.map_fields(data.stats, :count)) if data.stats.present?

      self.class.whitelist_fields(@usage_data_params)
    end
    # rubocop:enable Metrics/AbcSize
  end
end
