# frozen_string_literal: true

class EventTracker
  def initialize
    return if tracking_disabled

    @client = GitlabSDK::Client.new(
      # We're using the same setup as we do for frontend events
      app_id: ENV['BROWSER_SDK_APP_ID'],
      host: ENV['BROWSER_SDK_HOST']
    )
  end

  def track(*args)
    return if tracking_disabled

    client.track(*args)
  end

  def identify(*args)
    return if tracking_disabled

    client.identify(*args)
  end

  private

  attr_reader :client

  def tracking_disabled
    !ActiveModel::Type::Boolean.new.cast(ENV['BACKEND_SDK_ENABLED'])
  end
end
