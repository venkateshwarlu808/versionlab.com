# frozen_string_literal: true

class VersionCheckCreatorService
  attr_reader :request, :gitlab_info

  def initialize(gitlab_info, request)
    @request = request
    @gitlab_info = GitlabInfoDecoder.decode(gitlab_info, request_data)
  end

  def execute
    create_version_check

    { status: status, critical_vulnerability: critical_vulnerability? }
  end

  private

  attr_reader :version_check,
              :host

  def create_version_check
    return unless gitlab_info

    gitlab_url = URI.parse(request.env['HTTP_REFERER']).host
    return unless gitlab_url

    # We do not want to persist version check information any longer
    @version_check = VersionCheck.new(
      request_data: request_data.to_json,
      gitlab_version: gitlab_info['version'],
      referer_url: request.env['HTTP_REFERER']
    )
  rescue URI::InvalidURIError
    nil
  end

  def request_data
    return @request_data if @request_data

    @request_data = request.env.to_hash.select do |key, value|
      %w[HTTP_USER_AGENT HTTP_REFERER HTTP_ACCEPT_LANGUAGE].include?(key)
    end
    @request_data.each { |k, v| request_data[k] = v.dup&.force_encoding('ISO-8859-1') }
  end

  def status
    if version_check.update_required?
      'danger'
    elsif version_check.up_to_date?
      'success'
    else
      'warning'
    end
  rescue NoMethodError
    'failed'
  end

  def critical_vulnerability?
    return false unless version_check.present?

    version_check.critical_vulnerability?
  end
end
