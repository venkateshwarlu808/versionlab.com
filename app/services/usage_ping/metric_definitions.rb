# frozen_string_literal: true

module UsagePing
  class MetricDefinitions
    GITLAB_BASE_URL = 'https://gitlab.com'

    def execute
      Rails.cache.fetch(cache_key, expires_in: 1.day) do
        metric_definitions
      end
    end

    private

    def cache_key
      "#{self.class.name}:metric_definitions"
    end

    def metric_definitions
      @metric_definitions ||= YAML.safe_load(make_request)
    end

    def url
      "#{GITLAB_BASE_URL}/api/v4/usage_data/metric_definitions"
    end

    def make_request
      headers = {
        'Content-Type' => 'application/json',
        'Accept' => 'application/yaml'
      }

      HTTParty.get(url, headers: headers).parsed_response # rubocop:disable Gitlab/HTTParty
    end
  end
end
