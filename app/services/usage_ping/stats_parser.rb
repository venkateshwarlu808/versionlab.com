# frozen_string_literal: true

module UsagePing
  # Even if we change the payload that GitLab EE is sending,
  # we need to handle both incorrect and correctly formatted stats forever to
  # support old GitLab installations.

  class StatsParser
    NonIntegerError = Class.new(StandardError)

    def initialize(stats)
      @stats = stats
    end

    def execute
      parse!
      validate!

      {
        stats: stats
      }
    end

    private

    attr_reader :stats

    def parse!
      parse_invalid_stats if illegal_stats_format?
      parse_invalid_nil_values
    end

    def validate!
      invalid_stats_values = stats.select { |key, _value| !stats[key].is_a?(Integer) }
      return if invalid_stats_values.empty?

      raise NonIntegerError, "Non integer stats exist: #{invalid_stats_values}"
    end

    def illegal_stats_format?
      stats.key?(:user_preferences) || stats.key?(:operations_dashboard)
    end

    def parse_invalid_stats
      illegal_stats = @stats.extract!(:user_preferences, :operations_dashboard)
      @stats = flatten_invalid_stats(illegal_stats).merge(stats)
    end

    def flatten_invalid_stats(params)
      {
        user_preferences_group_overview_details:
          params.dig(:user_preferences, :group_overview_details).to_i,
        user_preferences_group_overview_security_dashboard:
          params.dig(:user_preferences, :group_overview_security_dashboard).to_i,
        operations_dashboard_default_dashboard:
          params.dig(:operations_dashboard, :default_dashboard).to_i,
        operations_dashboard_users_with_projects_added:
          params.dig(:operations_dashboard, :users_with_projects_added).to_i
      }
    end

    def parse_invalid_nil_values
      @stats[:epics_deepest_relationship_level] ||= 0
    end
  end
end
