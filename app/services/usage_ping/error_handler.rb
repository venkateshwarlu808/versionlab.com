# frozen_string_literal: true

module UsagePing
  class ErrorHandler
    def execute!(params)
      host = Host.find_or_create_for(params[:hostname])
      version = parse_version!(params[:version])

      UsagePingError.create!(
        uuid: params[:uuid],
        host: host,
        version: version,
        elapsed: params[:elapsed],
        message: params[:message]
      )
    end

    private

    def parse_version!(version)
      matching_version = version.match(Version::PERMITTED_VERSION)

      unless matching_version
        raise ArgumentError, "Version format mismatch found for UsagePing error association with version #{version}"
      end

      Version.find_by_version!(matching_version[0])
    end
  end
end
