# frozen_string_literal: true

module UsagePing
  class UsageDataCreator
    def initialize(attributes)
      @attributes = attributes
    end

    def execute!
      UsageData.create!(attributes)
    end

    private

    attr_reader :attributes
  end
end
