# frozen_string_literal: true

module UsagePing
  class ServiceHandler
    def execute(params, source_ip, raw_usage_data_id)
      parse_cycle_analytics_params(params)
      parse_usage_data_params(params, source_ip)
      cleanup_usage_data
      create_usage_data(raw_usage_data_id)
      create_current_host_stat

      create_cycle_analytics
      push_usage_data_to_salesforce

      { conv_index: calculate_conv_index }
    end

    private

    attr_reader :host,
                :usage_data,
                :usage_data_params,
                :cycle_analytics_params

    def cleanup_usage_data
      # cleans surely illegit license info
      return unless usage_data_params[:license_user_count].present? && usage_data_params[:license_user_count] > 500_000

      usage_data_params[:license_user_count] = 0
    end

    def create_usage_data(raw_usage_data_id)
      @usage_data = UsageDataCreator.new(usage_data_params.merge({ raw_usage_data_id: raw_usage_data_id })).execute!
    end

    def create_current_host_stat
      return unless host

      CurrentHostStat.find_or_create_for_usage_data!(host, usage_data)
    end

    def create_cycle_analytics
      return if cycle_analytics_params.empty?

      usage_data.create_avg_cycle_analytics(cycle_analytics_params)
    end

    def parse_cycle_analytics_params(params)
      params = params.delete(:avg_cycle_analytics) || {}

      @cycle_analytics_params = create_cycle_analytics_params(params)
    end

    def create_cycle_analytics_params(params)
      params.each_with_object({}) do |(key, value), hash|
        if value.is_a?(Hash)
          value.each do |calc_key, calc_value|
            hash[:"#{key}_#{calc_key}"] = calc_value
          end
        else
          hash[key.to_sym] = value
        end
      end
    end

    def parse_usage_data_params(params, source_ip)
      source_params = parse_source_params(params, source_ip)
      components_params = parse_components_params(params)
      features_params = parse_features_params(params)
      stats_params = parse_stats_params(params)

      @usage_data_params = params
                             .merge(source_params)
                             .merge(features_params)
                             .merge(components_params)
                             .merge(stats_params)
    end

    def parse_components_params(params)
      params = params.extract!(*COMPONENTS_PARAMS)

      {
        container_registry_vendor: params.dig(:container_registry_server, :vendor),
        container_registry_version: params.dig(:container_registry_server, :version),
        database_adapter: params.dig(:database, :adapter),
        database_version: params.dig(:database, :version),
        database_pg_system_id: params.dig(:database, :pg_system_id),
        database_flavor: params.dig(:database, :flavor),
        mail_smtp_server: params.dig(:mail, :smtp_server),
        git_version: (params.dig(:git, :version) || {}).values.join('.'),
        gitaly_version: params.dig(:gitaly, :version),
        gitaly_servers: params.dig(:gitaly, :servers),
        gitaly_filesystems: params.dig(:gitaly, :filesystems),
        gitaly_clusters: params.dig(:gitaly, :clusters),
        gitlab_pages_enabled: params.dig(:gitlab_pages, :enabled),
        gitlab_pages_version: params.dig(:gitlab_pages, :version)
      }
    end

    def parse_stats_params(params)
      return {} unless params.key?(:stats)

      StatsParser.new(params.delete(:stats)).execute
    end

    def parse_features_params(params)
      params = params.extract!(*FEATURES_PARAMS)

      params.each_with_object({}) do |(key, value), hash|
        key =
          if key.to_s.ends_with?("enabled")
            key
          else
            "#{key}_enabled"
          end

        hash[key.to_sym] = value
      end
    end

    def parse_source_params(params, source_ip)
      @host = Host.find_or_create_for(params[:hostname]) if params[:hostname]
      license_id = params.delete(:license_id)

      {
        host: host,
        source_ip: source_ip,
        source_license_id: license_id
      }
    end

    def push_usage_data_to_salesforce
      UsageDataWorker.perform_async(usage_data.id)
    end

    def calculate_conv_index
      BuildConvIndexResponseService.new.execute(usage_data)
    end
  end
end
