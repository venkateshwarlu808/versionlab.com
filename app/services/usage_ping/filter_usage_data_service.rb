# frozen_string_literal: true

module UsagePing
  class FilterUsageDataService
    # The following metric root keys are a subset of `UsagePing::ALL_KEYS` that predate metric
    # definitions and don't have corresponding entries in gitlab-com's metric definitions.
    #
    # They should be preserved, since older (version < 11.0.0) GitLab instances might still
    # report them, but they should be eventually removed if they are not loaded into Snowflake.
    ALLOWED_LEGACY_ROOT_KEYS = %w[
      analytics_unique_visits
      app_server
      auto_devops_enabled
      avg_cycle_analytics
      container_registry
      elasticsearch
      geo
      gitlab_shared_runners
      gravatar
      ldap
      license
      license_add_ons
      license_restricted_user_count
      omniauth
      reply_by_email
      signup
      stats
    ].freeze

    # Similar to the previous constant - specific legacy key paths that lack metric definitions,
    # but should be preserved.
    ALLOWED_LEGACY_KEY_PATHS = %w[
      git.version.major
      git.version.minor
      git.version.patch
      redis_hll_counters.compliance.g_compliance_dashboard
      redis_hll_counters.compliance.g_compliance_audit_events
      redis_hll_counters.compliance.i_compliance_audit_events
      redis_hll_counters.compliance.i_compliance_credential_inventory
      redis_hll_counters.analytics.g_analytics_contribution
      redis_hll_counters.analytics.g_analytics_insights
      redis_hll_counters.analytics.g_analytics_issues
      redis_hll_counters.analytics.g_analytics_productivity
      redis_hll_counters.analytics.g_analytics_valuestream
      redis_hll_counters.analytics.p_analytics_pipelines
      redis_hll_counters.analytics.p_analytics_code_reviews
      redis_hll_counters.analytics.p_analytics_valuestream
      redis_hll_counters.analytics.p_analytics_insights
      redis_hll_counters.analytics.p_analytics_issues
      redis_hll_counters.analytics.p_analytics_repo
      redis_hll_counters.analytics.i_analytics_cohorts
      redis_hll_counters.analytics.i_analytics_dev_ops_score
      redis_hll_counters.analytics.g_analytics_merge_request
      redis_hll_counters.analytics.p_analytics_merge_request
      redis_hll_counters.ide_edit.g_edit_by_web_ide
      redis_hll_counters.ide_edit.g_edit_by_sfe
      redis_hll_counters.ide_edit.g_edit_by_snippet_ide
      redis_hll_counters.search.i_search_total
      redis_hll_counters.search.i_search_advanced
      redis_hll_counters.search.i_search_paid
    ].freeze

    def initialize(payload)
      @payload = payload
    end

    def execute
      filtered_payload
    end

    attr_reader :payload

    private

    def filtered_payload
      params = ActionController::Parameters.new

      allow_by_legacy_root_keys!(params)
      allow_by_key_paths!(params)
      allow_object_metrics!(params)

      params
    end

    def allow_by_legacy_root_keys!(params)
      ALLOWED_LEGACY_ROOT_KEYS.each do |root_key|
        params[root_key] = payload.fetch(root_key)
      rescue ActionController::ParameterMissing
        next
      end
    end

    def allow_by_key_paths!(params)
      allowed_key_paths.each do |key_path|
        split_key_path = key_path.split('.')
        value = deep_fetch(payload, split_key_path)

        next if value.is_a?(ActionController::Parameters) && !value.empty?

        deep_set(params, split_key_path, value)
      rescue ActionController::ParameterMissing
        next
      end
    end

    def allow_object_metrics!(params)
      object_metrics_key_paths.each do |key_path|
        split_key_path = key_path.split('.')
        value = deep_fetch(payload, split_key_path)

        deep_set(params, split_key_path, value)
      rescue ActionController::ParameterMissing
        next
      end
    end

    def deep_fetch(params, keys)
      keys.reduce(payload) { |payload, key| payload.fetch(key) }
    end

    def deep_set(params, keys, value)
      key = keys.first

      if keys.length == 1
        params[key] = value
      else
        params[key] ||= {}
        deep_set(params[key], keys.slice(1..-1), value)
      end
    end

    def allowed_key_paths
      metric_definitions_key_paths | ALLOWED_LEGACY_KEY_PATHS
    end

    def metric_definitions_key_paths
      metric_definitions.map { |metric| metric['key_path'] }
    end

    def object_metrics_key_paths
      metric_definitions
        .select { |metric| metric['value_type'] == 'object' && metric['value_json_schema'].present? }
        .map { |metric| metric['key_path'] }
    end

    def metric_definitions
      @metric_definitions ||= UsagePing::MetricDefinitions.new.execute
    end
  end
end
