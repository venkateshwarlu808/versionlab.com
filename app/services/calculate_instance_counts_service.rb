# frozen_string_literal: true

class CalculateInstanceCountsService
  class ResultRow
    attr_reader :instances, :licenses

    def initialize(values)
      @instances = values.fetch('instances', 0).to_i
      @licenses = values.fetch('licenses', 0).to_i
    end

    def ratio
      licenses.zero? ? 0 : instances / licenses.to_f
    end
  end

  def execute
    results = ActiveRecord::Base.connection.exec_query(query)
    format_results(results)
  end

  private

  def query
    <<-SQL.strip_heredoc
      SELECT
        COUNT (DISTINCT uuid) as instances,
        COUNT (DISTINCT license_md5) as licenses,
        edition
      FROM usage_data
      WHERE recorded_at > '#{30.days.ago}'
      AND uuid IS NOT NULL
      GROUP BY edition;
    SQL
  end

  def format_results(results)
    by_edition = results.index_by { |row| row['edition'] }

    UsageData::EDITIONS.each_with_object({}) do |edition, results_map|
      edition_data = by_edition.fetch(edition, {})
      results_map[edition] = ResultRow.new(edition_data)
    end
  end
end
