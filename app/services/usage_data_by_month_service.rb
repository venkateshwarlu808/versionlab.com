# frozen_string_literal: true

class UsageDataByMonthService
  def execute
    @grouped_usage_data = UsageData.recent_counts_by_month_edition.group_by(&:month)

    format
  end

  private

  attr_reader :grouped_usage_data

  def format
    grouped_usage_data.each_with_object({}) do |(mth, values), collection|
      collection[mth.strftime('%Y-%m')] = transform_values(values)
    end
  end

  def transform_values(values)
    indexed_values = values.index_by(&:computed_edition)

    indexed_values.each_with_object({}) do |(ed, ud_collection), collection| # rubocop:disable Style/HashTransformValues
      collection[ed] = ud_collection.count
    end
  end
end
