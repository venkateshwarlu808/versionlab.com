# frozen_string_literal: true

module UsagePing
  COMPONENTS_PARAMS = [
    :app_server,
    :container_registry_server,
    :database,
    :mail,
    :git,
    :gitaly,
    :gitlab_pages
  ].freeze

  CYCLE_ANALYTICS_PARAMS = [
    :avg_cycle_analytics
  ].freeze

  OBJECT_STORE_PARAMS = [
    :object_store
  ].freeze

  TOPOLOGY_PARAMS = [
    :topology
  ].freeze

  SETTINGS_PARAMS = [
    :settings
  ].freeze

  UNIQUE_VISITS_PARAMS = [
    :analytics_unique_visits,
    :compliance_unique_visits,
    :search_unique_visits,
    :redis_hll_counters
  ].freeze

  FEATURES_PARAMS = [
    :auto_devops_enabled,
    :container_registry,
    :container_registry_enabled,
    :grafana_link_enabled,
    :dependency_proxy_enabled,
    :ingress_modsecurity_enabled,
    :elasticsearch,
    :elasticsearch_enabled,
    :geo,
    :geo_enabled,
    :gitlab_shared_runners,
    :gitlab_shared_runners_enabled,
    :gravatar,
    :gravatar_enabled,
    :ldap,
    :ldap_enabled,
    :mattermost_enabled,
    :omniauth,
    :omniauth_enabled,
    :prometheus_enabled,
    :prometheus_metrics_enabled,
    :reply_by_email,
    :reply_by_email_enabled,
    :signup,
    :signup_enabled,
    :web_ide_clientside_preview_enabled,
    :gitpod_enabled
  ].freeze

  LICENSE_PARAMS = [
    :license_add_ons,
    :licensee,
    :license
  ].freeze

  USAGE_DATA_PARAMS = [
    :active_user_count,
    :edition,
    :historical_max_users,
    :hostname,
    :installation_type,
    :license_expires_at,
    :license_id,
    :license_md5,
    :license_sha256,
    :license_plan,
    :license_restricted_user_count,
    :license_starts_at,
    :license_trial,
    :license_trial_ends_on,
    :license_user_count,
    :recorded_at,
    :recording_ce_finished_at,
    :recording_ee_finished_at,
    :uuid,
    :version
  ].freeze

  SYSTEM_USAGE_PARAMS = [
    :counts_monthly,
    :counts_weekly,
    :stats
  ].freeze

  SMAU_PARAMS = [
    :usage_activity_by_stage,
    :usage_activity_by_stage_monthly
  ].freeze

  ALL_KEYS = (
    COMPONENTS_PARAMS +
    CYCLE_ANALYTICS_PARAMS +
    OBJECT_STORE_PARAMS +
    TOPOLOGY_PARAMS +
    SETTINGS_PARAMS +
    UNIQUE_VISITS_PARAMS +
    FEATURES_PARAMS +
    LICENSE_PARAMS +
    USAGE_DATA_PARAMS +
    SYSTEM_USAGE_PARAMS +
    SMAU_PARAMS
  ).freeze

  FILTERED_KEYS = UsagePing::ALL_KEYS + [:counts, :usage_datum] - [:uuid, :hostname, :version, :edition]
end
