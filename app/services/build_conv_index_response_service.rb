# frozen_string_literal: true

class BuildConvIndexResponseService
  # defined as 80th percentile of the values returned by CalculateConvIndexService
  # after rejecting instances that do not use a given feature at all
  # calculated for 11 May 2017 00:00:00 - 15 June 2017 00:00:00
  LEADER_SCORES = {
    'boards' => 0.33,
    'ci_pipelines' => 17.84,
    'deployments' => 10.24,
    'environments' => 0.83,
    'issues' => 3.31,
    'merge_requests' => 3.77,
    'milestones' => 0.50,
    'notes' => 19.03,
    'projects_prometheus_active' => 0.12,
    'service_desk_issues' => 3.30
  }.freeze

  def execute(latest_ping)
    return {} unless latest_ping.uuid

    success, instance_scores = CalculateConvIndexService.new.execute(
      latest_ping.uuid,
      latest_ping.recorded_at
    )

    return { usage_data_id: latest_ping.id } unless success

    result = build_response(instance_scores)
    conv_dev_index = ConversationalDevelopmentIndex.create!(result.merge(usage_data: latest_ping))

    conv_dev_index.as_json
  end

  private

  def build_response(instance_scores)
    LEADER_SCORES.each_with_object({}) do |(name, leader_value), accumulator|
      instance_value = instance_scores.fetch(name)

      accumulator["leader_#{name}"] = leader_value
      accumulator["instance_#{name}"] = [instance_value, leader_value].min
    end
  end
end
