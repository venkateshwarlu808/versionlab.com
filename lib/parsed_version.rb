# frozen_string_literal: true

class ParsedVersion
  REGEX = /\Av?(?<major>\d+)\.?(?<minor>\d+)?\.?(?<patch>\d+)?\.?(?<label>[a-zA-Z0-9-_]+)?\Z/.freeze
  PARTS = [:major, :minor, :patch, :label].freeze

  InvalidVersionError = Class.new(StandardError)

  attr_reader(*PARTS)

  def initialize(version)
    @match = version.to_s.match(REGEX)
    raise InvalidVersionError, %(version "#{version}" is invalid for parsing.) unless @match

    PARTS.each { |part| instance_variable_set(:"@#{part}", @match[part]) }
  end

  def precision
    return :major if minor.nil?
    return :minor if patch.nil?

    label.nil? ? :patch : :label
  end

  def to_a(precision = nil)
    return [major&.to_i, minor&.to_i, patch&.to_i, label].compact unless precision

    parts = PARTS[0..[PARTS.index(precision), 2].min].map { |part| @match[part].to_i }
    precision == :label ? parts + [label] : parts
  end

  def to_query(precision = nil)
    effective_precision = precision || self.precision
    to_s(precision) + ([:patch, :label].include?(effective_precision) ? '%' : '.%')
  end

  def to_s(precision = nil)
    to_a(precision).join('.')
  end
end
