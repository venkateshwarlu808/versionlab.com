# frozen_string_literal: true

desc "Generate public/update_*.svg images"
task generate_svg: [:environment] do
  Image = Struct.new(:color, :text)

  def svg_path(name)
    Rails.root.join("public/update_#{name}.svg")
  end

  images = {
    danger: Image.new('#D0021B', 'update asap'),
    failed: Image.new('#4A4A4A', 'unable to connect'),
    success: Image.new('#00B43F', 'up-to-date'),
    warning: Image.new('#E39E00', 'update available')
  }

  images.each do |name, image|
    path = svg_path(name)
    puts "--> Writing #{path}"

    File.open(path, 'w') do |f|
      f.puts <<-SVG.strip_heredoc
        <!-- Generated automatically via `rake generate_svg` -->
        <svg xmlns="http://www.w3.org/2000/svg" width="134" height="25">
          <g shape-rendering="crispEdges">
            <path fill="#{image.color}" d="M0 0h134v25H0z"/>
          </g>
          <text x="67" y="16" fill="#fff" text-anchor="middle" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="13">#{image.text}</text>
        </svg>
      SVG
    end
  end
end
