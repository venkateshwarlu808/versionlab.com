# frozen_string_literal: true

require 'csv'

namespace :fortune_companies do
  desc 'Import Fortune companies ranking from CSV'
  task import: :environment do
    filename = Rails.root.join('lib/seeds/fortune_companies.csv')
    columns = %w[rank company domain]
    counter = 0

    FortuneCompany.delete_all

    CSV.foreach(filename) do |row|
      fortune = FortuneCompany.new(Hash[*columns.zip(row).flatten])

      if fortune.save
        counter += 1
      else
        puts "#{fortune.domain} - #{fortune.errors.full_messages.join(',')}"
      end
    end

    puts "Imported #{counter} domains"
  end

  desc 'Updates all hosts with current Fortune company ranking'
  task process: :environment do
    Host.where.not(fortune_rank: nil).update_all(fortune_rank: nil) # rubocop:disable Rails/SkipsModelValidations

    Host.all.each do |host|
      record = Gitlab::FortuneFinder.new(host.url)
      host.update_attribute(:fortune_rank, record.rank) if record.ranked? # rubocop:disable Rails/SkipsModelValidations
    end

    puts "Processed #{Host.count} hosts"

    ranking = Host.fortune500_ranking
    percentage = Host.fortune500_percentage

    puts "Over #{percentage}% of the Fortune 500 are using our product"
    puts "We have users in #{ranking.count} of the Fortune 500 companies"

    ranking = Host.fortune1000_ranking
    percentage = Host.fortune1000_percentage

    puts "Over #{percentage}% of the Fortune 1000 are using our product"
    puts "We have users in #{ranking.count} of the Fortune 1000 companies"
  end
end
