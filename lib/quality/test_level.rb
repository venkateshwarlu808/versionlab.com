# frozen_string_literal: true

module Quality
  class TestLevel
    UnknownTestLevelError = Class.new(StandardError)

    TEST_LEVEL_FOLDERS = {
      migration: %w[
        migrations
      ],
      background_migration: %w[
        lib/gitlab/background_migration
      ],
      unit: %w[
        bin
        config
        db
        helpers
        initializers
        lib
        models
        routing
        rubocop
        services
        tasks
        tooling
        views
        workers
      ],
      integration: %w[
        controllers
        mailers
        requests
      ],
      system: ['features']
    }.freeze

    def initialize
      @patterns = {}
      @regexps = {}
    end

    def pattern(level)
      @patterns[level] ||= "spec/#{folders_pattern(level)}{,/**/}*_spec.rb".freeze # rubocop:disable Style/RedundantFreeze
    end

    def regexp(level)
      @regexps[level] ||= Regexp.new("spec/#{folders_regex(level)}").freeze
    end

    def level_for(file_path)
      case file_path
        # Detect migration first since some background migration tests are under
        # spec/lib/gitlab/background_migration and tests under spec/lib are unit by default
      when regexp(:migration), regexp(:background_migration)
        :migration
      when regexp(:unit)
        :unit
      when regexp(:integration)
        :integration
      when regexp(:system)
        :system
      else
        raise UnknownTestLevelError,
              "Test level for #{file_path} couldn't be set. Please rename the file properly or change the test level "\
              "detection regexes in #{__FILE__}."
      end
    end

    def background_migration?(file_path)
      !!(file_path =~ regexp(:background_migration))
    end

    private

    def migration_and_background_migration_folders
      TEST_LEVEL_FOLDERS.fetch(:migration) + TEST_LEVEL_FOLDERS.fetch(:background_migration)
    end

    def folders_pattern(level)
      case level
      when :migration
        "{#{migration_and_background_migration_folders.join(',')}}"
      else
        "{#{TEST_LEVEL_FOLDERS.fetch(level).join(',')}}"
      end
    end

    def folders_regex(level)
      case level
      when :migration
        "(#{migration_and_background_migration_folders.join('|')})"
      else
        "(#{TEST_LEVEL_FOLDERS.fetch(level).join('|')})"
      end
    end
  end
end
