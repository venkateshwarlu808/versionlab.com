# frozen_string_literal: true

module API
  # Features API
  class Features < Grape::API
    resource :features do
      # Get a features list
      #
      # Parameters:
      #   version id or version (required) - The ID or version number (X.X.X)
      #   since - An ISO 8601 compatible datetime
      #   roles - An array of roles
      # Example Request:
      #   GET /features?version=666
      #   GET /features?version=v12.9.2
      #   GET /features?version=v12.9
      #   GET /features?version=v12&roles[]=admin
      get do
        scope = Version.latest.features
        if params[:version] || params[:since]
          scope = Feature.all

          scope = scope.for_version(params[:version]) if params[:version]

          if params[:since]
            since = begin
                      DateTime.parse(params[:since])
                    rescue ArgumentError
                      error!('since is an invalid date', 422)
                    end
            scope = scope.since(since)
          end
        end

        scope = scope.with_roles(params[:roles]) if params[:roles]

        present paginate(scope.with_version.ordered), with: Entities::Feature
      end
    end
  end
end
