require 'addressable/uri'

module Gitlab
  class FortuneFinder
    attr_reader :url

    def initialize(url)
      @url = url.to_s.downcase.strip
    end

    def rank
      return @rank if defined?(@rank)

      @rank = find_company(domain).try(:rank) if domain.present?
    end

    def ranked?
      rank.present?
    end

    private

    def find_company(domain)
      FortuneCompany.find_by(domain: domain)
    end

    def domain
      return @domain if defined?(@domain)

      @domain = begin
        PublicSuffix.parse(host).domain
      rescue PublicSuffix::DomainInvalid, PublicSuffix::DomainNotAllowed
        nil
      end
    end

    def host
      return nil if url.empty?

      uri = ::Addressable::URI.parse(url)

      if uri.host # valid https?://* URI
        uri.host
      else # url sans http://
        uri = ::Addressable::URI.parse("http://#{url}")
        # properly parse http://foo edge cases
        # see https://github.com/sporkmonger/addressable/issues/145
        uri.host if uri.host =~ /\./
      end
    rescue ::Addressable::URI::InvalidURIError
      nil
    end
  end
end
