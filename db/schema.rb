# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_11_16_111941) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "avg_cycle_analytics", id: :serial, force: :cascade do |t|
    t.integer "usage_data_id", null: false
    t.integer "total"
    t.integer "issue_average"
    t.integer "issue_sd"
    t.integer "issue_missing"
    t.integer "plan_average"
    t.integer "plan_sd"
    t.integer "plan_missing"
    t.integer "code_average"
    t.integer "code_sd"
    t.integer "code_missing"
    t.integer "test_average"
    t.integer "test_sd"
    t.integer "test_missing"
    t.integer "review_average"
    t.integer "review_sd"
    t.integer "review_missing"
    t.integer "staging_average"
    t.integer "staging_sd"
    t.integer "staging_missing"
    t.integer "production_average"
    t.integer "production_sd"
    t.integer "production_missing"
    t.index ["usage_data_id"], name: "index_avg_cycle_analytics_on_usage_data_id"
  end

  create_table "conversational_development_indices", id: :serial, force: :cascade do |t|
    t.integer "usage_data_id", null: false
    t.float "leader_boards", null: false
    t.float "instance_boards", null: false
    t.float "leader_ci_pipelines", null: false
    t.float "instance_ci_pipelines", null: false
    t.float "leader_deployments", null: false
    t.float "instance_deployments", null: false
    t.float "leader_environments", null: false
    t.float "instance_environments", null: false
    t.float "leader_issues", null: false
    t.float "instance_issues", null: false
    t.float "leader_merge_requests", null: false
    t.float "instance_merge_requests", null: false
    t.float "leader_milestones", null: false
    t.float "instance_milestones", null: false
    t.float "leader_notes", null: false
    t.float "instance_notes", null: false
    t.float "leader_projects_prometheus_active", null: false
    t.float "instance_projects_prometheus_active", null: false
    t.float "leader_service_desk_issues", null: false
    t.float "instance_service_desk_issues", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float "percentage_boards"
    t.float "percentage_ci_pipelines"
    t.float "percentage_deployments"
    t.float "percentage_environments"
    t.float "percentage_issues"
    t.float "percentage_merge_requests"
    t.float "percentage_milestones"
    t.float "percentage_notes"
    t.float "percentage_projects_prometheus_active"
    t.float "percentage_service_desk_issues"
    t.index ["updated_at"], name: "index_conversational_development_indices_on_updated_at"
    t.index ["usage_data_id"], name: "index_conversational_development_indices_on_usage_data_id"
  end

  create_table "current_host_stats", id: :serial, force: :cascade do |t|
    t.integer "host_id", null: false
    t.string "url", limit: 255, null: false
    t.string "version", limit: 255
    t.datetime "host_created_at", null: false
    t.datetime "usage_data_recorded_at"
    t.integer "active_users_count"
    t.integer "historical_max_users_count"
    t.integer "user_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "license_md5"
    t.string "edition", limit: 255
    t.string "zuora_account_id", limit: 255
    t.index ["active_users_count"], name: "index_current_host_stats_on_active_users_count"
    t.index ["edition"], name: "index_current_host_stats_on_edition"
    t.index ["historical_max_users_count"], name: "index_current_host_stats_on_historical_max_users_count"
    t.index ["host_created_at"], name: "index_current_host_stats_on_host_created_at"
    t.index ["host_id"], name: "index_current_host_stats_on_host_id"
    t.index ["url"], name: "index_current_host_stats_on_url"
    t.index ["usage_data_recorded_at"], name: "index_current_host_stats_on_usage_data_recorded_at"
    t.index ["user_count"], name: "index_current_host_stats_on_user_count"
    t.index ["version"], name: "index_current_host_stats_on_version"
  end

  create_table "features", force: :cascade do |t|
    t.bigint "version_id", null: false
    t.string "title", null: false
    t.text "description", null: false
    t.string "applicable_roles", default: [], null: false, array: true
    t.string "read_more_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["version_id"], name: "index_features_on_version_id"
  end

  create_table "fortune_companies", id: :serial, force: :cascade do |t|
    t.integer "rank", null: false
    t.string "company", null: false
    t.string "domain", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["domain"], name: "index_fortune_companies_on_domain", unique: true
    t.index ["updated_at"], name: "index_fortune_companies_on_updated_at"
  end

  create_table "hosts", id: :serial, force: :cascade do |t|
    t.string "url", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "star", default: false, null: false
    t.integer "fortune_rank"
    t.index ["created_at"], name: "index_hosts_on_created_at"
    t.index ["updated_at"], name: "index_hosts_on_updated_at"
    t.index ["url"], name: "index_hosts_on_url"
  end

  create_table "raw_usage_data", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "recorded_at", null: false
    t.string "uuid", limit: 255, null: false
    t.jsonb "payload", null: false
    t.jsonb "raw_usage_data_payload", default: {}, null: false
    t.index ["created_at"], name: "index_raw_usage_data_on_created_at"
    t.index ["uuid", "recorded_at"], name: "index_raw_usage_data_on_uuid_and_recorded_at"
  end

  create_table "usage_data", id: :serial, force: :cascade do |t|
    t.string "source_ip"
    t.string "version"
    t.integer "active_user_count"
    t.string "license_md5"
    t.integer "historical_max_users"
    t.string "licensee"
    t.integer "license_user_count"
    t.datetime "license_starts_at"
    t.datetime "license_expires_at"
    t.string "license_add_ons"
    t.integer "license_restricted_user_count"
    t.datetime "recorded_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "stats", default: {}
    t.boolean "mattermost_enabled"
    t.string "uuid"
    t.string "edition"
    t.string "hostname"
    t.integer "host_id"
    t.boolean "license_trial"
    t.integer "source_license_id"
    t.string "installation_type"
    t.string "license_plan"
    t.string "database_adapter"
    t.string "database_version"
    t.string "git_version"
    t.boolean "gitlab_pages_enabled"
    t.string "gitlab_pages_version"
    t.boolean "container_registry_enabled"
    t.boolean "elasticsearch_enabled"
    t.boolean "geo_enabled"
    t.boolean "gitlab_shared_runners_enabled"
    t.boolean "gravatar_enabled"
    t.boolean "ldap_enabled"
    t.boolean "omniauth_enabled"
    t.boolean "reply_by_email_enabled"
    t.boolean "signup_enabled"
    t.boolean "prometheus_metrics_enabled"
    t.json "usage_activity_by_stage", default: {}
    t.string "gitaly_version"
    t.integer "gitaly_servers"
    t.text "gitaly_filesystems", default: [], array: true
    t.boolean "web_ide_clientside_preview_enabled"
    t.date "license_trial_ends_on"
    t.json "usage_activity_by_stage_monthly", default: {}
    t.json "object_store", default: {}
    t.datetime "recording_ce_finished_at"
    t.datetime "recording_ee_finished_at"
    t.boolean "dependency_proxy_enabled"
    t.integer "gitaly_clusters"
    t.bigint "raw_usage_data_id"
    t.boolean "grafana_link_enabled"
    t.boolean "ingress_modsecurity_enabled"
    t.json "topology", default: {}
    t.jsonb "counts_monthly", default: {}
    t.jsonb "analytics_unique_visits", default: {}
    t.boolean "prometheus_enabled"
    t.string "container_registry_vendor", limit: 255
    t.string "container_registry_version", limit: 255
    t.jsonb "redis_hll_counters", default: {}
    t.boolean "auto_devops_enabled"
    t.string "mail_smtp_server", limit: 255
    t.jsonb "search_unique_visits", default: {}
    t.jsonb "compliance_unique_visits", default: {}
    t.boolean "gitpod_enabled"
    t.bigint "database_pg_system_id"
    t.jsonb "counts_weekly", default: {}
    t.jsonb "settings", default: {}
    t.jsonb "license", default: {}
    t.string "database_flavor", limit: 255
    t.string "license_sha256", limit: 64
    t.index "date_trunc('month'::text, recorded_at), COALESCE(edition, 'EE'::character varying)", name: "recorded_at_and_edit_by_function"
    t.index ["created_at"], name: "index_usage_data_on_created_at"
    t.index ["host_id"], name: "index_usage_data_on_host_id"
    t.index ["raw_usage_data_id"], name: "index_usage_data_on_raw_usage_data_id"
    t.index ["recorded_at"], name: "index_usage_data_on_recorded_at"
    t.index ["updated_at"], name: "index_usage_data_on_updated_at"
    t.index ["uuid"], name: "index_usage_data_on_uuid"
  end

  create_table "usage_ping_errors", force: :cascade do |t|
    t.bigint "version_id", null: false
    t.bigint "host_id", null: false
    t.string "uuid", limit: 255, null: false
    t.float "elapsed", null: false
    t.string "message", limit: 2056, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["host_id"], name: "index_usage_ping_errors_on_host_id"
    t.index ["version_id"], name: "index_usage_ping_errors_on_version_id"
  end

  create_table "usage_ping_metadata", force: :cascade do |t|
    t.string "uuid", limit: 255
    t.jsonb "metrics", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "provider"
    t.string "uid"
    t.string "private_token"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
    t.index ["updated_at"], name: "index_users_on_updated_at"
  end

  create_table "versions", id: :serial, force: :cascade do |t|
    t.string "version", null: false
    t.boolean "vulnerable", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "details"
    t.integer "vulnerability_type", limit: 2, default: 0
    t.index ["updated_at"], name: "index_versions_on_updated_at"
    t.index ["version"], name: "index_versions_on_version", unique: true
    t.check_constraint "char_length(details) < 2049", name: "versions_details_length_check"
  end

  add_foreign_key "current_host_stats", "hosts", on_delete: :cascade
  add_foreign_key "features", "versions", on_delete: :cascade
  add_foreign_key "usage_data", "raw_usage_data", column: "raw_usage_data_id", name: "fk_d7015bb583", on_delete: :nullify
  add_foreign_key "usage_ping_errors", "hosts", on_delete: :cascade
  add_foreign_key "usage_ping_errors", "versions", on_delete: :cascade
end
