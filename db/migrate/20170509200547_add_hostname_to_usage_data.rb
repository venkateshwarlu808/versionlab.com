# frozen_string_literal: true

class AddHostnameToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_column :usage_data, :hostname, :string # rubocop:disable Migration/AddLimitToStringColumns
  end
end
