# frozen_string_literal: true

class CreateVersions < ActiveRecord::Migration[4.2]
  def change
    create_table :versions do |t|
      t.string :version # rubocop:disable Migration/AddLimitToStringColumns
      t.boolean :vulnerable

      t.timestamps null: false # rubocop:disable Migration/Timestamps
    end
  end
end
