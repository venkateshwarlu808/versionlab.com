# frozen_string_literal: true

class AddEditionToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_column :usage_data, :edition, :string # rubocop:disable Migration/AddLimitToStringColumns
  end
end
