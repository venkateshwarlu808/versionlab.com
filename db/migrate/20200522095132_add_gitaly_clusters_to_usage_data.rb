# frozen_string_literal: true

class AddGitalyClustersToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :gitaly_clusters, :integer
  end
end
