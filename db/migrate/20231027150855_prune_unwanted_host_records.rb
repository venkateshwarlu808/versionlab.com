# frozen_string_literal: true

class PruneUnwantedHostRecords < ActiveRecord::Migration[6.1]
  # This migration included a reference to a wrong table.
  # See 20231103084654_prune_unwanted_host_records_v2.rb
  # for the correct one
  def up
    # no-op
  end

  def down
    # no-op
  end
end
