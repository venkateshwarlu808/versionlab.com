# frozen_string_literal: true

class AddIndexOnUuidToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_index :usage_data, :uuid # rubocop:disable Migration/AddIndex
  end
end
