# frozen_string_literal: true

class RenameCountsToOldCountsOnUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    rename_column_using_background_migration(:usage_data, :counts, :old_counts, batch_size: 5_000, interval: 2.minutes)
  end

  def down
    rename_column_using_background_migration(:usage_data, :old_counts, :counts, batch_size: 5_000, interval: 2.minutes)
  end
end
