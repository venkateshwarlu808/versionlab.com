# frozen_string_literal: true

class AddWebIdeClientSidePreviewEnabledToUsageData < ActiveRecord::Migration[5.0]
  def change
    add_column :usage_data, :web_ide_clientside_preview_enabled, :boolean
  end
end
