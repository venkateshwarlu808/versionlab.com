# frozen_string_literal: true

class AddSmauToUsageData < ActiveRecord::Migration[5.0]
  def change
    add_column :usage_data, :smau, :json, default: {}
  end
end
