# frozen_string_literal: true

class AddPrometheusMetricsEnabledToUsageData < ActiveRecord::Migration[5.0]
  def change
    add_column :usage_data, :prometheus_metrics_enabled, :boolean
  end
end
