# frozen_string_literal: true

class AddLicenseMd5ToCurrentHostStats < ActiveRecord::Migration[5.0]
  def change
    add_column :current_host_stats, :license_md5, :string # rubocop:disable Migration/AddLimitToStringColumns
  end
end
