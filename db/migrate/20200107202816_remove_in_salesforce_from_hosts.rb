# frozen_string_literal: true

class RemoveInSalesforceFromHosts < ActiveRecord::Migration[5.1]
  def change
    remove_column :hosts, :in_salesforce, :boolean
  end
end
