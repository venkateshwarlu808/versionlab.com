# frozen_string_literal: true

class CreateUsagePingErrors < ActiveRecord::Migration[6.1]
  include Gitlab::Database::MigrationHelpers
  def change
    create_table :usage_ping_errors do |t|
      t.references :version, foreign_key: { on_delete: :cascade }, null: false
      t.references :host, foreign_key: { on_delete: :cascade }, null: false

      t.string :uuid, null: false, limit: 255
      t.float :elapsed, null: false
      t.string :message, null: false, limit: 2056

      t.timestamps null: false # rubocop:disable Migration/Timestamps
    end
  end
end
