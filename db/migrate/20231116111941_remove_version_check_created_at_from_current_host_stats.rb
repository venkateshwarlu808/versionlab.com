# frozen_string_literal: true

class RemoveVersionCheckCreatedAtFromCurrentHostStats < ActiveRecord::Migration[6.1]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    if index_exists?(:current_host_stats, :version_check_created_at)
      remove_concurrent_index :current_host_stats, column: :version_check_created_at
    end

    remove_column :current_host_stats, :version_check_created_at, :datetime # rubocop:disable Migration/Datetime
  end

  def down
    add_column :current_host_stats, :version_check_created_at, :datetime # rubocop:disable Migration/Datetime
    add_concurrent_index :current_host_stats, :version_check_created_at
  end
end
