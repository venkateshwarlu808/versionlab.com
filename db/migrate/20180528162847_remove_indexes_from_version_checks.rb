# frozen_string_literal: true

class RemoveIndexesFromVersionChecks < ActiveRecord::Migration[4.2]
  INDEXES = [:created_at, :gitlab_version].freeze

  disable_ddl_transaction!

  def up
    INDEXES.each do |column|
      if index_exists?(:version_checks, column)
        remove_index :version_checks, column # rubocop:disable Migration/RemoveIndex
      end
    end
  end

  def down
    INDEXES.each do |column|
      unless index_exists?(:version_checks, column)
        add_index :version_checks, column, algorithm: :concurrently # rubocop:disable Migration/AddIndex
      end
    end
  end
end
