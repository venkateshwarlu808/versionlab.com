# frozen_string_literal: true

class AddInstallationTypeToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_column :usage_data, :installation_type, :string # rubocop:disable Migration/AddLimitToStringColumns
  end
end
