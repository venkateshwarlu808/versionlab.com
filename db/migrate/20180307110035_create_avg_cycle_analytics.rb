# frozen_string_literal: true

class CreateAvgCycleAnalytics < ActiveRecord::Migration[4.2]
  STAGES = [:issue, :plan, :code, :test, :review, :staging, :production]
  CALCULATIONS = [:average, :sd, :missing]

  def change
    create_table :avg_cycle_analytics do |t| # rubocop:disable Rails/CreateTableWithTimestamps
      t.integer :usage_data_id, null: false
      t.integer :total

      STAGES.each do |stage|
        CALCULATIONS.each do |calc|
          t.integer :"#{stage}_#{calc}"
        end
      end
    end
  end
end
