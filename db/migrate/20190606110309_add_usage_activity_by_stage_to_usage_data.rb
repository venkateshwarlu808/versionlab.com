# frozen_string_literal: true

class AddUsageActivityByStageToUsageData < ActiveRecord::Migration[5.0]
  def change
    add_column :usage_data, :usage_activity_by_stage, :json, default: {}
  end
end
