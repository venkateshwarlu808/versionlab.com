# frozen_string_literal: true

class DropLicensesTable < ActiveRecord::Migration[5.2]
  def up
    drop_table :licenses
  end

  def down
    # rubocop:disable Migration/AddLimitToStringColumns
    create_table :licenses, force: :cascade do |t|
      t.string "name"
      t.string "email"
      t.string "company"
      t.integer "user_count"
      t.string "add_ons"
      t.string "md5"
      t.date "starts_on"
      t.date "expires_on"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer "active_users_count"
      t.integer "historical_max_users_count"
      t.datetime "last_ping_received_at"
      t.string "version"
      t.index ["md5"], name: "index_licenses_on_md5", using: :btree
    end
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
