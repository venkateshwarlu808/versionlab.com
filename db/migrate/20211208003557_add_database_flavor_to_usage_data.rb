# frozen_string_literal: true

class AddDatabaseFlavorToUsageData < ActiveRecord::Migration[6.0]
  def change
    add_column :usage_data, :database_flavor, :string, limit: 255
  end
end
