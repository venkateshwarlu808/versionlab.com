# frozen_string_literal: true

class ResolveNilCountersOnUsageData < ActiveRecord::Migration[5.2]
  def up
    # no-op this was for a one time data integrity issue in production for counters with nil values
  end

  def down
    # no-op
  end
end
