# frozen_string_literal: true
class AddRecordingTimesToUsageData < ActiveRecord::Migration[5.2]
  def change
    change_table(:usage_data, bulk: true) do |t|
      t.datetime :recording_ce_finished_at, :recording_ee_finished_at
    end
  end
end
