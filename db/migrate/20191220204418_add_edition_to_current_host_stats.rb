# frozen_string_literal: true

class AddEditionToCurrentHostStats < ActiveRecord::Migration[5.1]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    add_column :current_host_stats, :edition, :string, limit: 255
    add_concurrent_index :current_host_stats, :edition
  end

  def down
    remove_concurrent_index :current_host_stats, column: :edition if index_exists?(:current_host_stats, :edition)
    remove_column :current_host_stats, :edition, :string
  end
end
