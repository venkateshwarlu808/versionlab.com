# frozen_string_literal: true

class AddMoreFieldsToVersionCheck < ActiveRecord::Migration[4.2]
  def change
    # rubocop:disable Migration/AddLimitToStringColumns
    add_column :version_checks, :request_ip, :string # rubocop:disable Rails/BulkChangeTable
    add_column :version_checks, :gitlab_version, :string
    add_column :version_checks, :gitlab_url, :string
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
