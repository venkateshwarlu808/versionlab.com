# frozen_string_literal: true

class AddHostIdToVersionCheck < ActiveRecord::Migration[4.2]
  def change
    add_column :version_checks, :host_id, :integer
  end
end
