# frozen_string_literal: true

class DropWebHooksTable < ActiveRecord::Migration[5.1]
  def up
    drop_table :web_hooks
  end

  def down
    # rubocop:disable Migration/AddLimitToStringColumns
    create_table :web_hooks, force: :cascade do |t|
      t.string "url", null: false
      t.boolean "active", default: false, null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
