# frozen_string_literal: true

class CreateVersionChecks < ActiveRecord::Migration[4.2]
  def change
    create_table :version_checks do |t|
      t.text :request_data

      t.timestamps null: false # rubocop:disable Migration/Timestamps
    end
  end
end
