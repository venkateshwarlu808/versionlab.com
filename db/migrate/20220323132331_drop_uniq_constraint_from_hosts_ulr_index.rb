# frozen_string_literal: true

class DropUniqConstraintFromHostsUlrIndex < ActiveRecord::Migration[6.1]
  include Gitlab::Database::MigrationHelpers

  TABLE_NAME = 'hosts'
  INDEX_NAME_ON_PRODUCTION = 'index_hosts_on_url'
  INDEX_NAME = 'index_hosts_on_url_unique'
  COLUMN = 'url'
  # When using the methods "add_concurrent_index" or "remove_concurrent_index"

  disable_ddl_transaction!

  def up
    # no-op broken index was manually fix by SRE https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6660#note_882008224
    return if index_name_exists?(TABLE_NAME, INDEX_NAME_ON_PRODUCTION)

    add_concurrent_index TABLE_NAME, COLUMN, name: INDEX_NAME_ON_PRODUCTION
    remove_concurrent_index_by_name TABLE_NAME, INDEX_NAME
  end

  def down
    # no-op broken index was manually fix by SRE https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6660#note_882008224
    # but duplicated records which caused invalid index in the begging are still present, so any atempt to
    # recreate unique index are sure to fail.
  end
end
