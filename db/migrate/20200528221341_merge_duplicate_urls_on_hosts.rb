# frozen_string_literal: true

class MergeDuplicateUrlsOnHosts < ActiveRecord::Migration[5.2]
  def up
    # no-op this was for a one time data migration in production to merge duplicated hosts
  end

  def down
    # no-op
  end
end
