# frozen_string_literal: true

class RemoveCurrentFieldsFromHosts < ActiveRecord::Migration[5.0]
  def change
    remove_column :hosts, :current_usage_data_id, :integer # rubocop:disable Rails/BulkChangeTable
    remove_column :hosts, :current_version_check_id, :integer
  end
end
