# frozen_string_literal: true

class AddMissingIndex < ActiveRecord::Migration[4.2]
  def change
    add_index :version_checks, :host_id # rubocop:disable Migration/AddIndex
  end
end
