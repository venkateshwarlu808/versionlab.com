# frozen_string_literal: true

class AddDependencyProxyEnabledToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :dependency_proxy_enabled, :boolean
  end
end
