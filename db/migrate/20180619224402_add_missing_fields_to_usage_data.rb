# frozen_string_literal: true

class AddMissingFieldsToUsageData < ActiveRecord::Migration[4.2]
  def change
    # rubocop:disable Migration/AddLimitToStringColumns
    add_column :usage_data, :license_plan, :string # rubocop:disable Rails/BulkChangeTable
    add_column :usage_data, :database_adapter, :string
    add_column :usage_data, :database_version, :string
    add_column :usage_data, :git_version, :string
    add_column :usage_data, :gitlab_pages_enabled, :boolean
    add_column :usage_data, :gitlab_pages_version, :string
    add_column :usage_data, :container_registry_enabled, :boolean
    add_column :usage_data, :elasticsearch_enabled, :boolean
    add_column :usage_data, :geo_enabled, :boolean
    add_column :usage_data, :gitlab_shared_runners_enabled, :boolean
    add_column :usage_data, :gravatar_enabled, :boolean
    add_column :usage_data, :ldap_enabled, :boolean
    add_column :usage_data, :omniauth_enabled, :boolean
    add_column :usage_data, :reply_by_email_enabled, :boolean
    add_column :usage_data, :signup_enabled, :boolean
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
