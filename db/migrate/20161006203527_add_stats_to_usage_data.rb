# frozen_string_literal: true

class AddStatsToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_column :usage_data, :stats, :json, default: {}
  end
end
