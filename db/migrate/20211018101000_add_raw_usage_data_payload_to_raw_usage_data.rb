# frozen_string_literal: true

class AddRawUsageDataPayloadToRawUsageData < ActiveRecord::Migration[6.0]
  include Gitlab::Database::MigrationHelpers

  def change
    add_column :raw_usage_data, :raw_usage_data_payload, :jsonb, default: {}, null: false
  end
end
