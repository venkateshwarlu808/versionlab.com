# frozen_string_literal: true

class AddIndexOnUsageDataIdToAvgCycleAnalytics < ActiveRecord::Migration[5.0]
  def change
    add_index :avg_cycle_analytics, :usage_data_id # rubocop:disable Migration/AddIndex
  end
end
