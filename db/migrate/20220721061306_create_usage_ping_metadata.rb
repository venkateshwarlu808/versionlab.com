# frozen_string_literal: true
#
class CreateUsagePingMetadata < ActiveRecord::Migration[6.1]
  def change
    create_table :usage_ping_metadata do |t|
      t.string :uuid, limit: 255 # nullable for backwards compatibility
      t.jsonb :metrics, null: false

      t.timestamps # rubocop:disable Migration/Timestamps
    end
  end
end
