# frozen_string_literal: true

class AddGitalyToUsageData < ActiveRecord::Migration[5.0]
  def change
    # rubocop:disable Migration/AddLimitToStringColumns
    add_column :usage_data, :gitaly_version, :string # rubocop:disable Rails/BulkChangeTable
    add_column :usage_data, :gitaly_servers, :integer
    add_column :usage_data, :gitaly_filesystems, :text, array: true, default: []
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
