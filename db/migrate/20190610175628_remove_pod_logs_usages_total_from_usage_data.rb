# frozen_string_literal: true

class RemovePodLogsUsagesTotalFromUsageData < ActiveRecord::Migration[5.0]
  def change
    remove_column :usage_data, :pod_logs_usages_total, :integer
  end
end
