# frozen_string_literal: true

class AddCountsToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_column :usage_data, :counts, :text
  end
end
