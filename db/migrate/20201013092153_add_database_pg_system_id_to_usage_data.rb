# frozen_string_literal: true

class AddDatabasePgSystemIdToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :database_pg_system_id, :bigint
  end
end
