# frozen_string_literal: true

class PruneUnwantedHostRecordsV2 < ActiveRecord::Migration[6.1]
  # This migration will be obsolete, as we are going to remove
  # _all_ version check data.
  def up
    # no-op
  end

  def down
    # no-op
  end
end
