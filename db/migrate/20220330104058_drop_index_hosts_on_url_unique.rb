# frozen_string_literal: true

class DropIndexHostsOnUrlUnique < ActiveRecord::Migration[6.1]
  include Gitlab::Database::MigrationHelpers

  TABLE_NAME = 'hosts'
  INDEX_NAME_ON_PRODUCTION = 'index_hosts_on_url_unique'
  COLUMN = 'url'

  disable_ddl_transaction!

  def up
    # no-op broken index was removed from staging and dev environment after cleanup
    # from fix by SRE https://gitlab.com/gitlab-services/version-gitlab-com/-/merge_requests/672#note_893279998
    return unless index_name_exists?(TABLE_NAME, INDEX_NAME_ON_PRODUCTION)

    remove_concurrent_index_by_name TABLE_NAME, INDEX_NAME_ON_PRODUCTION
  end

  def down
    # no-op index is broken there are duplicated records on production environment
  end
end
