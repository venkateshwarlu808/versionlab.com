# frozen_string_literal: true

class AddIndexesToVersionChecks < ActiveRecord::Migration[4.2]
  disable_ddl_transaction!

  def change
    [:created_at, :gitlab_version].each do |column|
      unless index_exists?(:version_checks, column)
        add_index :version_checks, column, algorithm: :concurrently # rubocop:disable Migration/AddIndex
      end
    end
  end
end
