# frozen_string_literal: true

class AddMattermostEnabledToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_column :usage_data, :mattermost_enabled, :boolean
  end
end
