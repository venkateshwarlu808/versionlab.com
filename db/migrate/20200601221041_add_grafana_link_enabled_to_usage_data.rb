# frozen_string_literal: true

class AddGrafanaLinkEnabledToUsageData < ActiveRecord::Migration[5.2]
  def change
    add_column :usage_data, :grafana_link_enabled, :boolean
  end
end
