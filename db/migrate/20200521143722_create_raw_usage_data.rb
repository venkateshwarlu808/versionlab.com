# frozen_string_literal: true

class CreateRawUsageData < ActiveRecord::Migration[5.2]
  def change
    create_table :raw_usage_data do |t|
      t.datetime :created_at, null: false
      t.datetime :recorded_at, null: false
      t.string :uuid, null: false, limit: 255
      t.jsonb :payload, null: false
    end
  end
end
