# frozen_string_literal: true

class TestHelmTimeout < ActiveRecord::Migration[5.2]
  disable_ddl_transaction!

  def up
    # no-op test was successful https://gitlab.com/gitlab-services/version-gitlab-com/-/issues/372#note_3919785960
  end

  def down
    # no-op
  end
end
