# frozen_string_literal: true

class AddSourceLicenseIdToUsageData < ActiveRecord::Migration[4.2]
  def change
    add_column :usage_data, :source_license_id, :integer
  end
end
