# frozen_string_literal: true

class AddCountsWeeklyToUsageData < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  def change
    add_column :usage_data, :counts_weekly, :jsonb, default: {}
  end
end
