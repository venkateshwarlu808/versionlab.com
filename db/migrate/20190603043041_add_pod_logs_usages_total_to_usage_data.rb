# frozen_string_literal: true

class AddPodLogsUsagesTotalToUsageData < ActiveRecord::Migration[5.0]
  def change
    add_column :usage_data, :pod_logs_usages_total, :integer
  end
end
