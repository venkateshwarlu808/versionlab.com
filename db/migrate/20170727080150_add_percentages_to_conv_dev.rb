# frozen_string_literal: true

class AddPercentagesToConvDev < ActiveRecord::Migration[4.2]
  def change
    add_column :conversational_development_indices, :percentage_boards, :float # rubocop:disable Rails/BulkChangeTable
    add_column :conversational_development_indices, :percentage_ci_pipelines, :float
    add_column :conversational_development_indices, :percentage_deployments, :float
    add_column :conversational_development_indices, :percentage_environments, :float
    add_column :conversational_development_indices, :percentage_issues, :float
    add_column :conversational_development_indices, :percentage_merge_requests, :float
    add_column :conversational_development_indices, :percentage_milestones, :float
    add_column :conversational_development_indices, :percentage_notes, :float
    add_column :conversational_development_indices, :percentage_projects_prometheus_active, :float
    add_column :conversational_development_indices, :percentage_service_desk_issues, :float
  end
end
