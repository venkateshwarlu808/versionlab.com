# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
if Rails.env.development?
  class Seeder
    STATS = %w[
      boards ci_builds ci_pipelines ci_runners ci_triggers
      deploy_keys deployments environments geo_nodes groups
      issues keys labels ldap_group_links ldap_keys ldap_users
      lfs_objects merge_requests milestones notes pages_domains
      projects projects_prometheus_active protected_branches releases
      remote_mirrors services snippets todos uploads web_hooks
    ].freeze

    def seed
      ActiveRecord::Base.transaction do
        create_user
        create_versions
        create_usage_ping_data
      end
    end

    private

    def create_user
      User.create!(
        email: 'admin@example.com',
        password: '12345678'
      )
    end

    def create_versions
      0.upto(13) do |minor|
        0.upto(rand(8)) do |patch|
          Version.create!(version: "7.#{minor}.#{patch}")
        end
      end

      @versions = Version.all
    end

    def create_usage_ping_data
      create_ee_usage_ping_data
      create_ce_usage_ping_data
    end

    def create_ee_usage_ping_data
      Host.order(:id).limit(120).each do |host|
        # this loop simulates from 1 to 4 hosts using the same license
        rand(1..4).times do
          uuid = SecureRandom.uuid

          rand(20..40).times do
            stats = generate_usage_stats

            usage_data = UsageData.create!(
              hostname: host.url,
              uuid: uuid,
              edition: 'EE',
              version: @versions.sample.version,
              active_user_count: rand(10..10_000),
              historical_max_users: rand(10..10_000),
              license_user_count: rand(10..10_000),
              recorded_at: Faker::Date.between(from: 6.months.ago, to: Time.current),
              created_at: Faker::Date.between(from: 6.months.ago, to: Time.current),
              stats: stats,
              host: host
            )

            ConversationalDevelopmentIndex.create!(
              leader_boards: 0.33,
              instance_boards: rand(0.0..0.33).round(2),
              leader_ci_pipelines: 17.84,
              instance_ci_pipelines: rand(0.0..17.84).round(2),
              leader_deployments: 10.24,
              instance_deployments: rand(0.0..10.24).round(2),
              leader_environments: 0.83,
              instance_environments: rand(0.0..0.83).round(2),
              leader_issues: 3.31,
              instance_issues: rand(0.0..3.31).round(2),
              leader_merge_requests: 3.77,
              instance_merge_requests: rand(0.0..3.77).round(2),
              leader_milestones: 0.5,
              instance_milestones: rand(0.0..0.5).round(2),
              leader_notes: 19.03,
              instance_notes: rand(0.0..19.03).round(2),
              leader_projects_prometheus_active: 0.12,
              instance_projects_prometheus_active: rand(0.0..0.12).round(2),
              leader_service_desk_issues: 3.3,
              instance_service_desk_issues: rand(0.0..3.3).round(2),
              usage_data: usage_data
            )
          end
        end

        CurrentHostStat.find_or_create_for_usage_data!(host, host.usage_datum.last)
      end
    end

    def create_ce_usage_ping_data
      # this is the shape of data that we have now - 1 usage data record per
      # 1 "fake" CE license record
      # see https://dev.gitlab.org/gitlab/version-gitlab-com/issues/60

      Host.order(:id).offset(120).limit(50).each do |host|
        rand(1..4).times do
          uuid = SecureRandom.uuid

          rand(20..40).times do
            stats = generate_usage_stats

            usage_data = UsageData.create!(
              hostname: host.url,
              uuid: uuid,
              edition: UsageData::CE_EDITION,
              version: @versions.sample.version,
              active_user_count: rand(10..10_000),
              historical_max_users: rand(10..10_000),
              recorded_at: Faker::Date.between(from: 6.months.ago, to: Time.current),
              created_at: Faker::Date.between(from: 6.months.ago, to: Time.current),
              stats: stats,
              host: host
            )

            ConversationalDevelopmentIndex.create!(
              leader_boards: 0.33,
              instance_boards: rand(0.0..0.33).round(2),
              leader_ci_pipelines: 17.84,
              instance_ci_pipelines: rand(0.0..17.84).round(2),
              leader_deployments: 10.24,
              instance_deployments: rand(0.0..10.24).round(2),
              leader_environments: 0.83,
              instance_environments: rand(0.0..0.83).round(2),
              leader_issues: 3.31,
              instance_issues: rand(0.0..3.31).round(2),
              leader_merge_requests: 3.77,
              instance_merge_requests: rand(0.0..3.77).round(2),
              leader_milestones: 0.5,
              instance_milestones: rand(0.0..0.5).round(2),
              leader_notes: 19.03,
              instance_notes: rand(0.0..19.03).round(2),
              leader_projects_prometheus_active: 0.12,
              instance_projects_prometheus_active: rand(0.0..0.12).round(2),
              leader_service_desk_issues: 3.3,
              instance_service_desk_issues: rand(0.0..3.3).round(2),
              usage_data: usage_data
            )
          end
        end

        CurrentHostStat.find_or_create_for_usage_data!(host, host.usage_datum.last)
      end
    end

    def generate_usage_stats
      STATS.index_with do |name|
        rand(0..1_000_000)
      end
    end
  end

  Seeder.new.seed
end
