# frozen_string_literal: true

module Tooling
  module Danger
    module ProjectHelper
      LOCAL_RULES ||= %w[
        gemfile
        database
      ].freeze

      CI_ONLY_RULES ||= %w[
        specs
        roulette
        z_metadata
      ].freeze

      MESSAGE_PREFIX = '==>'

      CATEGORIES = {
        %r{\Adoc/} => :none, # To reinstate roulette for documentation, set to `:docs`.
        %r{\A(CONTRIBUTING|LICENSE|MAINTENANCE|PHILOSOPHY|PROCESS|README)(\.md)?\z} => :backend,
        %r{\Aapp/(assets|views)/} => :frontend,
        %r{\Apublic/} => :frontend,
        %r{\Aspec/(javascripts|frontend)/} => :frontend,
        %r{\Avendor/assets/} => :frontend,
        %r{\Ascripts/frontend/} => :frontend,
        %r{(\A|/)(
          \.babelrc |
          \.eslintignore |
          \.eslintrc(\.yml)? |
          \.nvmrc |
          \.prettierignore |
          \.prettierrc |
          \.scss-lint.yml |
          \.stylelintrc |
          \.haml-lint.yml |
          \.haml-lint_todo.yml |
          babel\.config\.js |
          jest\.config\.js |
          karma\.config\.js |
          webpack\.config\.js |
          package\.json |
          yarn\.lock |
          \.gitlab/ci/frontend\.gitlab-ci\.yml
        )\z}x => :frontend,

        %r{\Adb/(?!fixtures)[^/]+} => :database,
        %r{\Alib/gitlab/(database|background_migration|sql)(/|\.rb)} => :database,
        %r{\Arubocop/cop/migration(/|\.rb)} => :database,

        %r{\A(\.gitlab-ci\.yml\z|\.gitlab\/ci)} => :backend,
        %r{\A\.gitlab/dashboards/} => :backend,
        %r{Dangerfile\z} => :backend,
        %r{\A(danger/|lib/gitlab/danger/)} => :backend,
        %r{\Ascripts/} => :backend,
        %r{\A\.overcommit\.yml\.example\z} => :backend,
        %r{\A\.codeclimate\.yml\z} => :backend,

        %r{\Aapp/(?!assets|views)[^/]+} => :backend,
        %r{\A(bin|config|generator_templates|lib|rubocop)/} => :backend,
        %r{\Aspec/features/} => :test,
        %r{\Aspec/(?!javascripts|frontend)[^/]+} => :backend,
        %r{\Avendor/(?!assets)[^/]+} => :backend,
        %r{\Avendor/(languages\.yml|licenses\.csv)\z} => :backend,
        %r{\A(Gemfile|Gemfile.lock|Procfile|Rakefile)\z} => :backend,
        %r{\A[A-Z_]+_VERSION\z} => :backend,
        %r{\A\.rubocop(_todo)?\.yml\z} => :backend,

        # Fallbacks in case the above patterns miss anything
        %r{\.(rb|ru)\z} => :backend,
        %r{(
          \.(md|txt)\z |
          \.markdownlint\.json
        )}x => :backend,
        %r{\.js\z} => :frontend
      }.freeze

      def local_warning_message
        "#{MESSAGE_PREFIX} Only the following Danger rules can be run locally: #{LOCAL_RULES.join(', ')}"
      end
      module_function :local_warning_message # rubocop:disable Style/AccessModifierDeclarations

      def success_message
        "#{MESSAGE_PREFIX} No Danger rule violations!"
      end
      module_function :success_message # rubocop:disable Style/AccessModifierDeclarations

      def rule_names
        helper.ci? ? LOCAL_RULES | CI_ONLY_RULES : LOCAL_RULES
      end

      def file_lines(filename)
        read_file(filename).lines(chomp: true)
      end

      def labels_to_add
        @labels_to_add ||= []
      end

      private

      def read_file(filename)
        File.read(filename)
      end
    end
  end
end
