# frozen_string_literal: true

require 'spec_helper'

describe BackgroundMigrationWorker, :sidekiq do
  let(:worker) { described_class.new }

  describe '.perform' do
    it 'performs a background migration' do
      expect(Gitlab::BackgroundMigration)
        .to receive(:perform)
        .with('Foo', [10, 20])

      worker.perform('Foo', [10, 20])
    end
  end
end
