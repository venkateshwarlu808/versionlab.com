# frozen_string_literal: true

require 'spec_helper'

describe UsageDataWorker do
  describe '#perform' do
    let!(:current_host_stat) { create(:current_host_stat, host: usage_data.host) }

    context 'with a CE service ping' do
      let(:usage_data) { create(:usage_data, :ce) }

      it 'does not try to push usage data' do
        expect(Salesforce::PushUsageDataService).not_to receive(:new)

        subject.perform(usage_data.id)
      end

      it 'does not update current host stat' do
        expect { subject.perform(usage_data.id) }.not_to change { current_host_stat.reset.zuora_account_id }
      end
    end

    context 'with an EE service ping' do
      let(:usage_data) { create(:usage_data, :ees, historical_max_users: 10) }
      let(:account_matcher_service) { double(:account_matcher_service) }

      before do
        allow(Salesforce::AccountMatcherService).to receive(:new).and_return(account_matcher_service)
      end

      context 'when no account is found' do
        before do
          allow(account_matcher_service).to receive(:execute).and_return(nil)
          allow(account_matcher_service).to receive(:account_id).and_return(nil)
        end

        it 'does not try to push usage data' do
          expect(Salesforce::PushUsageDataService).not_to receive(:new)

          subject.perform(usage_data.id)
        end

        it 'does not update current host stat' do
          expect { subject.perform(usage_data.id) }.not_to change { current_host_stat.reset.zuora_account_id }
        end
      end

      context 'when a license is found' do
        let(:create_params) { { account_id: 'ac12', zuora_subscription_id: 'zuora123' } }
        let(:push_usage_data_service) { double(:push_usage_data_service) }

        before do
          allow(account_matcher_service).to receive(:execute).and_return(create_params)
          allow(account_matcher_service).to receive(:account_id).and_return(create_params[:account_id])
          allow(push_usage_data_service).to receive(:execute)
        end

        context 'with unreliable associated records' do
          before do
            allow(Salesforce::PushUsageDataService).to receive(:new).with(bad_usage_data, create_params)
                                                         .and_return(push_usage_data_service)
          end

          context 'with no host record' do
            let(:bad_usage_data) { create(:usage_data, :ees, historical_max_users: 10, host: nil) }

            it 'does not update current host stat' do
              expect { subject.perform(bad_usage_data.id) }.not_to change { current_host_stat.reset.zuora_account_id }
            end
          end

          context 'with no current host stat record' do
            let(:bad_usage_data) { create(:usage_data, :ees, historical_max_users: 10) }

            it 'does not update current host stat' do
              expect { subject.perform(bad_usage_data.id) }.not_to change { current_host_stat.reset.zuora_account_id }
            end
          end
        end

        context 'with reliable associated records' do
          before do
            allow(Salesforce::PushUsageDataService).to receive(:new).with(usage_data, create_params)
                                                         .and_return(push_usage_data_service)
          end

          it 'pushes usage data to Salesforce' do
            subject.perform(usage_data.id)

            expect(push_usage_data_service).to have_received(:execute)
          end

          it 'updates current host stat' do
            expect { subject.perform(usage_data.id) }.to change { current_host_stat.reset.zuora_account_id }
                                                           .from(nil)
                                                           .to(create_params[:account_id])
          end
        end
      end
    end
  end
end
