# frozen_string_literal: true

require "spec_helper"

describe "VersionCheckController" do
  describe "routing" do
    describe "#check" do
      it "with json format" do
        expect(get("/check.json")).to route_to("version_checks#check", format: 'json')
      end
    end
  end
end
