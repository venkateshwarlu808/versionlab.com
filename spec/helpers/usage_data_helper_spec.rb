# frozen_string_literal: true

require 'spec_helper'

describe UsageDataHelper do
  describe '.license_usage_class' do
    it 'is over the license limit' do
      license_item = double(over?: true)

      expect(helper.license_usage_class(license_item)).to eq 'license-over'
    end

    it 'is under the license limit' do
      license_item = double(over?: false)

      expect(helper.license_usage_class(license_item)).to eq 'license-under'
    end
  end

  describe '.license_usage_users' do
    let(:usage_users) { 5 }

    it 'is over the license limit' do
      license_item = double(over?: true, usage_users: usage_users)

      expect(helper.license_usage_users(license_item)).to eq "+#{usage_users}"
    end

    it 'is under the license limit' do
      license_item = double(over?: false, usage_users: usage_users)

      expect(helper.license_usage_users(license_item)).to eq usage_users
    end
  end
end
