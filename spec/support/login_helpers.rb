# frozen_string_literal: true

module LoginHelpers
  # Internal: Login as the specified user
  #
  # user - User instance to login with
  def login_with(user)
    visit new_user_session_path
    fill_in :user_email, with: user.email
    fill_in :user_password, with: '12345678'
    click_button 'Sign in'
  end
end
