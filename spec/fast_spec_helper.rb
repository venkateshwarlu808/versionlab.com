# frozen_string_literal: true

require 'bundler/setup'

require 'active_support/dependencies'
require 'active_support/all'

ActiveSupport::XmlMini.backend = 'Nokogiri'
