# frozen_string_literal: true

require 'spec_helper'

describe Version do
  describe 'associations' do
    it { is_expected.to have_many(:features) }
  end

  describe 'callbacks' do
    describe '#mark_previous_versions_vulnerable' do
      shared_examples 'when security_release_type is a non-security release' do |value|
        it "does nothing to previous versions" do
          previous_vulnerable = create(:version, version: '10.8.0',
                                                 vulnerability_type: :non_critical)
          previous_non_vulnerable = create(:version, version: '10.8.1')
          previous_critical = create(:version, version: '10.8.2',
                                               vulnerability_type: :critical)

          described_class.create!(version: '10.8.3', security_release_type: value)

          expect(previous_vulnerable.reset.non_critical?).to be(true)
          expect(previous_non_vulnerable.reset.not_vulnerable?).to be(true)
          expect(previous_critical.reset.critical?).to be(true)
        end
      end

      context 'when security_release_type is :non_critical' do
        it "marks all previous :non_vulnerable versions as vulnerability_type = :non_critical" do
          to_mark = [
            create(:version, version: '10.8.1'),
            create(:version, version: '10.8.0')
          ]
          ignored = create(:version, version: '10.7.0')

          release = described_class.create!(
            version: '10.8.2',
            security_release_type: 'non_critical'
          )
          to_mark.map(&:reset)
          ignored.reset

          expect(to_mark).to be_all(&:non_critical?)
          expect(ignored.not_vulnerable?).to be(true)
          expect(release.not_vulnerable?).to be(true)
        end

        it "does not mark previous :critical versions as vulnerability_type = :non_critical" do
          to_mark = [
            create(:version, version: '10.8.2'),
            create(:version, version: '10.8.1')
          ]
          ignored = create(:version, version: '10.8.0',
                                     vulnerability_type: :critical)

          release = described_class.create!(
            version: '10.8.3',
            security_release_type: 'non_critical'
          )
          to_mark.map(&:reset)
          ignored.reset

          expect(to_mark).to be_all(&:non_critical?)
          expect(ignored.critical?).to be(true)
          expect(release.not_vulnerable?).to be(true)
        end
      end

      context 'when security_release_type is :critical' do
        it "marks all previous versions as vulnerability_type = :critical" do
          to_mark = [
            create(:version, version: '10.8.1', vulnerability_type: :non_critical),
            create(:version, version: '10.8.0')
          ]
          ignored = create(:version, version: '10.7.0')

          release = described_class.create!(
            version: '10.8.2',
            security_release_type: 'critical'
          )
          to_mark.map(&:reset)
          ignored.reset

          expect(to_mark).to be_all(&:critical?)
          expect(ignored.not_vulnerable?).to be(true)
          expect(release.not_vulnerable?).to be(true)
        end
      end

      include_examples 'when security_release_type is a non-security release', nil
      include_examples 'when security_release_type is a non-security release', ''
      include_examples 'when security_release_type is a non-security release', 'not_vulnerable'
    end
  end

  describe 'scopes' do
    describe '.within' do
      it 'finds the expected matching records' do
        create(:version, version: '10.8.2')
        create(:version, version: '10.8.1')
        create(:version, version: '10.80.6')
        create(:version, version: '10.7.12')
        create(:version, version: '9.8.0')
        create(:version, version: '9.7.6')

        expect(described_class.within(10).count).to eq 4
        expect(described_class.within(9).count).to eq 2
        expect(described_class.within('9.7').count).to eq 1
        expect(described_class.within('10.8').count).to eq 2
        expect(described_class.within('10.8.2').count).to eq 1
        expect(described_class.within('10.80').count).to eq 1
      end
    end
  end

  describe '.resolve!' do
    let!(:version) { create(:version, version: '10.8.2') }

    it 'resolves a version by id' do
      expect(described_class.resolve!(version.id)).to eq version
      expect { described_class.resolve!(-1) }.to raise_error ActiveRecord::RecordNotFound
    end

    it 'resolves to the latest version by version when it starts with "v"' do
      create(:version, version: '10.8.1')

      expect(described_class.resolve!('v10.8')).to eq version
      expect { described_class.resolve!('v11') }.to raise_error ActiveRecord::RecordNotFound
    end
  end

  describe '.most_recent_by_version' do
    it 'orders versions by string with 8.10.0 more recent than 8.2.0' do
      v8_10_0 = create(:version, version: '8.10.0')
      v8_2_0 = create(:version, version: '8.2.0')

      expect(described_class.most_recent_by_version).to eq([v8_10_0, v8_2_0])
    end
  end

  describe '.latest' do
    it 'returns the highest version' do
      create(:version, version: '9.8.0')
      create(:version, version: '10.7.6')
      create(:version, version: '9.8.2')
      create(:version, version: '9.9.1')
      create(:version, version: '10.8.2')

      expect(described_class.latest.version).to eq '10.8.2'
      expect(described_class.latest('v10.7').version).to eq '10.7.6'
      expect(described_class.latest(9).version).to eq '9.9.1'
      expect(described_class.latest('9.8').version).to eq '9.8.2'
    end

    it 'allows raising an exception on failure' do
      expect { described_class.latest('11') }.not_to raise_error
      expect { described_class.latest('11', raise_exception: true) }.to raise_error ActiveRecord::RecordNotFound
    end
  end

  describe '.latest_minor_releases' do
    it 'returns the highest feature versions' do
      create(:version, version: '10.6.9')
      create(:version, version: '10.7.5')
      create(:version, version: '10.7.6')
      create(:version, version: '10.8.0')
      create(:version, version: '10.8.1')
      create(:version, version: '10.8.2')

      last_versions = described_class.latest_minor_releases(2).map(&:version)

      expect(last_versions).to eq ['10.8.2', '10.7.6']
    end
  end

  describe '.latest_minor_releases_after' do
    subject { described_class.latest_minor_releases_after(count: 2, after_version: after_version).map(&:version) }

    before do
      create(:version, version: '10.5.9')
      create(:version, version: '10.6.9')
      create(:version, version: '10.7.5')
      create(:version, version: '10.7.6')
      create(:version, version: '10.8.0')
      create(:version, version: '10.8.1')
      create(:version, version: '10.8.2')
    end

    context "when after a version without highest patch version" do
      let(:after_version) { '10.7.5' }

      it 'returns the highest feature versions' do
        expect(subject).to eq ['10.8.2', '10.7.6']
      end
    end

    context "when there's only one newer minor version" do
      let(:after_version) { '10.7.6' }

      it 'returns the highest feature versions' do
        expect(subject).to eq ['10.8.2']
      end
    end

    context "when there's no newer version" do
      let(:after_version) { '10.8.2' }

      it 'returns the highest feature versions' do
        expect(subject).to eq []
      end
    end
  end

  describe '.latest?' do
    it 'checks if the provided version is the latest one' do
      create(:version, version: '10.8.1')
      create(:version, version: '9.7.6')
      create(:version, version: '10.8.2')

      expect(described_class.latest?('10.8.1')).to be false
      expect(described_class.latest?('10.8.2')).to be true
      expect(described_class.latest?('10.8.3')).to be true
      expect(described_class.latest?('10.8')).to be true
      expect(described_class.latest?('10')).to be true
      expect(described_class.latest?('10.8.2.200')).to be true
    end

    it 'handles versions that are not standard / understood versions' do
      create(:version, version: '10.8.2')

      expect(described_class.latest?('v10.8.1.v20190904.2')).to be false
      expect(described_class.latest?('10.8.X')).to be false
      expect(described_class.latest?('10.8.2.v200')).to be false
    end
  end

  describe 'validations' do
    it 'doesnt save invalid version' do
      expect { described_class.create!(version: '15.0.1-pre') }.to raise_error(ActiveRecord::RecordInvalid)
    end

    it 'doesnt save version with very long details field' do
      expect { described_class.create!(version: '15.0.1', details: 'a' * 2049) }
        .to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
