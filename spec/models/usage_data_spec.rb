# frozen_string_literal: true

require 'spec_helper'

describe UsageData do
  it { expect(described_class::EDITIONS).to be_kind_of(Array) }

  describe '#edition' do
    context 'when the edition is set in the DB' do
      subject { build(:usage_data, edition: 'EEP') }

      it 'uses the value from the DB' do
        expect(subject.edition).to eq('EEP')
      end
    end

    context 'when the edition is not set in the DB' do
      subject { build(:usage_data, edition: nil) }

      it 'returns EE' do
        expect(subject.edition).to eq('EE')
      end
    end
  end

  describe '#clean_data' do
    it 'does not crash when data is nil' do
      expect { described_class.create!(nil) }.not_to raise_error
    end

    context 'with protected_branchess typo' do
      let(:payload) { { stats: { protected_branchess: 3 } }.with_indifferent_access }

      it 'uses the correct protected_branches' do
        usage_data = described_class.create!(payload)

        expect(usage_data.stats['protected_branches']).to eq(3)
      end
    end
  end

  describe '.recent_counts_by_month_edition' do
    it 'returns a collection' do
      travel_to(Time.utc(2017, 3, 1, 12, 0, 0)) do
        create(:usage_data, edition: 'EE', recorded_at: Time.utc(2017, 2, 28))
        create(:usage_data, edition: nil, recorded_at: Time.utc(2017, 2, 14))
        create(:usage_data, :eep, recorded_at: Time.utc(2017, 2, 1))
        create(:usage_data, :eep, recorded_at: Time.utc(2017, 1, 31))

        expect(described_class.recent_counts_by_month_edition.length).to eq(3)
      end
    end
  end

  describe '.ordered_and_paginated' do
    it 'returns correct page and size' do
      create_list(:usage_data, 2)
      page_3 = create_list(:usage_data, 2).reverse
      create_list(:usage_data, 4)

      expect(described_class.ordered_and_paginated(3, 2)).to match(page_3)
    end
  end

  describe '.ordered_by_id_with_limit' do
    it 'returns correct size and order' do
      create(:usage_data)
      ud_2 = create(:usage_data)
      ud_3 = create(:usage_data)

      expect(described_class.ordered_by_id_with_limit(2)).to match([ud_3, ud_2])
    end
  end

  describe '.related_stats_by_week' do
    it 'returns correct counts grouped by week' do
      uuid = SecureRandom.hex(8)
      ud_1, ud_2 = nil
      travel_to(Time.utc(2019, 3, 1, 12, 0, 0)) do
        ud_1 = create(:usage_data, :with_stats, uuid: uuid, recorded_at: Time.now)
        create(:usage_data, :with_stats, uuid: uuid, host_id: ud_1.host_id, recorded_at: Time.now)
        ud_2 = create(:usage_data, :with_stats, uuid: uuid, recorded_at: Time.now)
      end
      travel_to(Time.utc(2019, 3, 14, 12, 0, 0)) do
        create(:usage_data, :with_stats, uuid: uuid, host_id: ud_1.host_id, recorded_at: Time.now)
      end

      expect(described_class.related_stats_by_week(uuid, ud_1.host_id, 'ci_builds').values).to match([83, nil, 83])
      expect(described_class.related_stats_by_week(uuid, ud_2.host_id, 'ci_builds').values).to match([83])
    end
  end

  describe '.by_uuid_with_recorded_limits' do
    it 'returns correct records by range and recorded at order' do
      uuid = SecureRandom.hex(8)
      end_date = Time.utc(2019, 3, 14, 12, 0, 0)
      start_date = end_date - 30.days

      create(:usage_data, uuid: uuid, recorded_at: Time.utc(2018, 3, 14, 12, 0, 0))
      ud_3 = create(:usage_data, uuid: uuid, recorded_at: end_date)
      ud_1 = create(:usage_data, uuid: uuid, recorded_at: Time.utc(2019, 3, 1, 12, 0, 0))
      ud_2 = create(:usage_data, uuid: uuid, recorded_at: Time.utc(2019, 3, 1, 13, 0, 0))

      expect(described_class.by_uuid_with_recorded_limits(uuid, start_date..end_date)).to match([ud_1, ud_2, ud_3])
    end
  end

  describe '.by_uuid' do
    it 'returns correct records' do
      uuid = SecureRandom.hex(8)
      uuid_another = SecureRandom.hex(8)

      ud_another = create(:usage_data, uuid: uuid_another)
      ud_1 = create(:usage_data, uuid: uuid)
      ud_2 = create(:usage_data, uuid: uuid)

      expect(described_class.by_uuid(uuid)).to match_array([ud_2, ud_1])
      expect(described_class.by_uuid(uuid)).not_to include([ud_another])
    end
  end

  describe '.ids_by_edition_with_created_limits' do
    it 'returns correct ids by edition for a time range' do
      end_date = Time.utc(2019, 3, 14, 12, 0, 0)
      start_date = end_date - 30.days

      ud_1 = create(:usage_data, :ee)
      travel_to(Time.utc(2019, 3, 1, 12, 0, 0)) do
        create(:usage_data, :ee)
      end
      travel_to(end_date) do
        create(:usage_data, :ee)
      end

      ids = described_class.ids_by_edition_with_created_limits('EE', start_date..end_date)
      expect(ids.size).to eq 2
      expect(ids).not_to include(ud_1.id)
    end

    it 'filters by edition correctly' do
      time = Time.now
      ud_1 = create(:usage_data, :ee)
      create(:usage_data, :eep)

      expect(described_class.ids_by_edition_with_created_limits('EE', time..DateTime::Infinity.new)).to match([ud_1.id])
    end
  end

  describe '.related_weekly_user_count_by_uuid' do
    it 'returns a hash of counts per week' do
      uuid = SecureRandom.hex(8)
      another_uuid = SecureRandom.hex(8)

      ud_1 = travel_to(Time.utc(2020, 10, 5, 12, 0, 0)) do
        create(:usage_data, uuid: uuid, active_user_count: 3)
      end

      travel_to(Time.utc(2020, 10, 14, 12, 0, 0)) do
        create(:usage_data, uuid: uuid, active_user_count: 5)
      end

      another_ud = travel_to(Time.utc(2020, 10, 14, 12, 0, 0)) do
        create(:usage_data, uuid: another_uuid, active_user_count: 11)
      end

      expect(ud_1.related_weekly_user_count_by_uuid).to eq({ Date.new(2020, 10, 4) => 3, Date.new(2020, 10, 11) => 5 })
      expect(another_ud.related_weekly_user_count_by_uuid).to eq({ Date.new(2020, 10, 11) => 11 })
    end

    it 'counts each uuid once per week' do
      uuid = SecureRandom.hex(8)

      ud_1 = travel_to(Time.utc(2020, 10, 14, 12, 0, 0)) do
        create(:usage_data, uuid: uuid, active_user_count: 3)
      end

      travel_to(Time.utc(2020, 10, 15, 12, 0, 0)) do
        create(:usage_data, uuid: uuid, active_user_count: 5)
      end

      expect(ud_1.related_weekly_user_count_by_uuid).to eq({ Date.new(2020, 10, 11) => 5 })
    end
  end
end
