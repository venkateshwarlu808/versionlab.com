# frozen_string_literal: true

require 'spec_helper'

describe RawUsageData, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:payload) }
    it { is_expected.to validate_presence_of(:uuid) }
    it { is_expected.to validate_presence_of(:recorded_at) }
  end
end
