# frozen_string_literal: true

require 'spec_helper'

describe Feature, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:version) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:description) }
  end

  describe 'scopes' do
    describe 'for_version' do
      it 'looks up all features for various versions' do
        create(:version, :with_features, feature_count: 2, version: '10.1.2')
        create(:version, :with_features, feature_count: 3, version: '10.2.2')
        create(:version, :with_features, feature_count: 1, version: '9.1.0')

        expect(described_class.for_version(10).count).to eq 5
        expect(described_class.for_version('10.2').count).to eq 3
        expect(described_class.for_version('9').count).to eq 1
      end
    end

    describe 'since' do
      it 'finds all records after a certain time' do
        create(:feature, created_at: Time.current - 1.week)
        yesterday = create(:feature, created_at: Time.current - 1.day)
        today = create(:feature, created_at: Time.current)

        expect(described_class.since(Time.current - 2.days)).to eq [yesterday, today]
        expect(described_class.since(Time.current - 1.day).count).to eq 1
        expect(described_class.since(Time.current - 2.weeks).count).to eq 3
      end
    end

    describe 'with_roles' do
      it 'finds records matching given role(s)' do
        user = create(:feature, applicable_roles: %w[user])
        user_and_admin = create(:feature, applicable_roles: %w[user admin])
        admin = create(:feature, applicable_roles: %w[admin])

        expect(described_class.with_roles(['user'])).to eq [user, user_and_admin]
        expect(described_class.with_roles(['admin'])).to eq [user_and_admin, admin]
        expect(described_class.with_roles(%w[admin user])).to eq [user, user_and_admin, admin]
      end
    end
  end
end
