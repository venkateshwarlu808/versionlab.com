# frozen_string_literal: true

require 'spec_helper'

describe VersionCheck do
  let(:valid_attributes) do
    {
      request_data: '{"HTTP_REFERER": "https://gitlab.com/admin"}',
      referer_url: 'https://gitlab.com/help',
      gitlab_version: '7.7.1'
    }
  end

  def create_version_check(version:, type:)
    build(:version_check, gitlab_version: version).tap do |record|
      create(:version, version: record.gitlab_version, vulnerability_type: type)
    end
  end

  subject { build(:version_check, valid_attributes) }

  it { is_expected.to be_valid }

  describe '.up_to_date?' do
    it 'returns true if version is most recent' do
      create(:version, version: '2.2.1')
      create(:version, version: '2.2.2')

      not_most_recent_version = build(:version_check, gitlab_version: '2.2.1')
      most_recent_version = build(:version_check, gitlab_version: '2.2.2')

      expect(not_most_recent_version.up_to_date?).to be(false)
      expect(most_recent_version.up_to_date?).to be(true)
    end
  end

  describe '#update_required?' do
    it 'returns true if version is marked with a vulnerable vulnerability_type' do
      non_vulnerable_version = create_version_check(version: '2.2.1', type: :not_vulnerable)
      non_critical_version = create_version_check(version: '2.2.2', type: :non_critical)
      critical_version = create_version_check(version: '2.2.3', type: :critical)

      expect(non_vulnerable_version).not_to be_update_required
      expect(non_critical_version).to be_update_required
      expect(critical_version).to be_update_required
    end
  end

  describe '#critical_vulnerability?' do
    it 'returns true if version is marked with a critical vulnerability' do
      non_vulnerable_version = create_version_check(version: '2.2.1', type: :not_vulnerable)
      non_critical_version = create_version_check(version: '2.2.2', type: :non_critical)
      critical_version = create_version_check(version: '2.2.3', type: :critical)

      expect(non_vulnerable_version).not_to be_critical_vulnerability
      expect(non_critical_version).not_to be_critical_vulnerability
      expect(critical_version).to be_critical_vulnerability
    end
  end
end
