# frozen_string_literal: true

require 'spec_helper'

describe ApplicationRecord do
  describe '.safe_ensure_unique' do
    let(:model) { build(:current_host_stat) }
    let(:klass) { model.class }

    before do
      allow(model).to receive(:save).and_raise(ActiveRecord::RecordNotUnique)
    end

    it 'returns false when ActiveRecord::RecordNotUnique is raised' do
      expect(model).to receive(:save).once
      expect(klass.safe_ensure_unique { model.save }).to be_falsey
    end

    it 'retries based on retry count specified' do
      expect(model).to receive(:save).exactly(3).times
      expect(klass.safe_ensure_unique(retries: 2) { model.save }).to be_falsey
    end
  end

  describe '.safe_find_or_create_by!' do
    let(:host) { create(:host) }

    it 'creates the user avoiding race conditions' do
      expect(CurrentHostStat).to receive(:find_or_initialize_by).and_raise(ActiveRecord::RecordNotUnique)
      allow(CurrentHostStat).to receive(:find_or_initialize_by).and_call_original

      expect do
        CurrentHostStat
          .safe_find_or_create_by!(build(:current_host_stat).attributes) { |record| record.host_id = host.id }
      end.to change(CurrentHostStat, :count).by(1)
    end

    it 'creates a record using safe_find_or_create_by!' do
      attributes = build(:current_host_stat).attributes

      expect(CurrentHostStat).to receive(:find_or_initialize_by).and_call_original
      expect(CurrentHostStat.safe_find_or_create_by!(attributes) { |record| record.host_id = host.id })
        .to be_a(CurrentHostStat)
    end

    it 'raises a validation error if the record was not persisted' do
      expect do
        CurrentHostStat.safe_find_or_create_by!(url: 'test_url') { |record| record.host_id = nil }
      end.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  describe '.safe_find_or_create_by' do
    let(:payload) { { test: 'test' } }
    let(:attributes) { build(:raw_usage_data).attributes }

    it 'creates the user avoiding race conditions' do
      expect(RawUsageData).to receive(:find_or_initialize_by).and_raise(ActiveRecord::RecordNotUnique)
      allow(RawUsageData).to receive(:find_or_initialize_by).and_call_original

      expect do
        RawUsageData
          .safe_find_or_create_by(attributes) { |record| record.payload = payload }
      end.to change(RawUsageData, :count).by(1)
    end

    it 'creates a record using safe_find_or_create_by' do
      new_raw_usage_data = RawUsageData.safe_find_or_create_by(attributes) { |record| record.payload = payload }

      expect(new_raw_usage_data).to be_valid
    end

    it "doesn't create a record if the record is not valid" do
      raw_usage_data = RawUsageData.safe_find_or_create_by(attributes) { |record| record.payload = nil }

      expect(raw_usage_data.id).to be_nil
      expect(raw_usage_data).not_to be_valid
    end
  end
end
