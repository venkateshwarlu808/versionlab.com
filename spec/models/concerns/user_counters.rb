# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'user_counters' do
  describe '#usage_users' do
    it 'returns nil' do
      expect(subject.usage_users).to be_nil
    end

    it 'returns the difference between historical_max_users_count - user_count when both fields are present' do
      subject.historical_max_users_count = 3
      subject.user_count = 2

      expect(subject.usage_users).to eq 1
    end
  end

  describe '#has_user_count?' do
    it 'returns nil if user_count is nil' do
      expect(subject.has_user_count?).to be_nil
    end

    it 'returns nil if user_count is a negative number' do
      subject.user_count = -1

      expect(subject.has_user_count?).to be false
    end

    it 'returns nil if user_count is 0' do
      subject.user_count = 0

      expect(subject.has_user_count?).to be false
    end
  end

  describe '#over?' do
    it 'returns nil' do
      expect(subject.over?).to be_nil
    end

    it 'returns the difference between historical_max_users_count - user_count when both fields are present' do
      subject.historical_max_users_count = 3
      subject.user_count = 2

      expect(subject.over?).to be true
    end
  end
end
