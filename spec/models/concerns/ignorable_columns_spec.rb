# frozen_string_literal: true

require 'spec_helper'

describe IgnorableColumns do
  let(:record_class) do
    Class.new(ApplicationRecord) do
      include IgnorableColumns
    end
  end

  subject { record_class }

  it 'adds columns to ignored_columns' do
    expect do
      subject.ignore_columns(:name, :created_at, remove_after: '2019-12-01')
    end.to change(subject, :ignored_columns).from([]).to(%w[name created_at])
  end

  it 'adds columns to ignored_columns (array version)' do
    expect do
      subject.ignore_columns(%i[name created_at], remove_after: '2019-12-01')
    end.to change(subject, :ignored_columns).from([]).to(%w[name created_at])
  end

  it 'requires remove_after attribute to be set' do
    expect { subject.ignore_columns(:name, remove_after: nil) }
      .to raise_error(ArgumentError, /Please indicate/)
  end

  it 'requires remove_after attribute to be set' do
    expect { subject.ignore_columns(:name, remove_after: "not a date") }
      .to raise_error(ArgumentError, /Please indicate/)
  end

  describe '.ignored_columns_details' do
    shared_examples_for 'storing removal information' do
      it 'storing removal information' do
        subject.ignore_column(columns, remove_after: '2019-12-01')

        [columns].flatten.each do |column|
          expect(subject.ignored_columns_details[column].remove_after).to eq(Date.parse('2019-12-01'))
        end
      end
    end

    context 'with single column' do
      let(:columns) { :name }

      it_behaves_like 'storing removal information'
    end

    context 'with array column' do
      let(:columns) { %i[name created_at] }

      it_behaves_like 'storing removal information'
    end

    it 'defaults to empty Hash' do
      expect(subject.ignored_columns_details).to eq({})
    end
  end

  describe '.utc_date_regex' do
    subject { record_class.utc_date_regex }

    it { is_expected.to match('2019-10-20') }
    it { is_expected.to match('1990-01-01') }
    it { is_expected.not_to match('11-1234-90') }
    it { is_expected.not_to match('aa-1234-cc') }
    it { is_expected.not_to match('9/9/2018') }
  end

  describe IgnorableColumns::ColumnIgnore do
    subject { described_class.new(remove_after) }

    describe '#safe_to_remove?' do
      context 'when after remove_after date has passed' do
        let(:remove_after) { Date.parse('2019-01-10') }

        it 'returns true (safe to remove)' do
          expect(subject).to be_safe_to_remove
        end
      end

      context 'when before remove_after date has passed' do
        let(:remove_after) { Date.today }

        it 'returns false (not safe to remove)' do
          expect(subject).not_to be_safe_to_remove
        end
      end
    end
  end
end
