# frozen_string_literal: true

require 'spec_helper'

describe User do
  subject(:user) { build(:user) }

  it { is_expected.to be_valid }

  describe '#private_token' do
    it 'ensures that use have a private token' do
      user.save

      expect(user.private_token).not_to be_blank
    end
  end
end
