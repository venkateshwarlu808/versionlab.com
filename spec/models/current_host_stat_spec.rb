# frozen_string_literal: true

require 'spec_helper'
require_relative 'concerns/sortable'
require_relative 'concerns/user_counters'

describe CurrentHostStat do
  it_behaves_like 'sortable'
  it_behaves_like 'user_counters'

  subject(:current_host_stat) { build(:current_host_stat, user_count: nil) }

  it { is_expected.to be_valid }

  describe 'Associations' do
    it { is_expected.to belong_to :host }
  end

  context 'when delegating methods' do
    it { is_expected.to delegate_method(:fortune_rank).to(:host) }
  end

  describe '.find_or_create_for_usage_data!' do
    let(:usage_data) { create(:usage_data, active_user_count: 5) }

    it 'creates a new record' do
      expect { described_class.find_or_create_for_usage_data!(usage_data.host, usage_data) }
        .to change(described_class, :count)
    end

    it 'updates an existing record' do
      create(:current_host_stat, url: usage_data.host.url, active_users_count: 0)

      expect { described_class.find_or_create_for_usage_data!(usage_data.host, usage_data) }
        .not_to change(described_class, :count)
      expect(described_class.last.active_users_count).to eq usage_data.active_user_count
    end

    it 'raises an error' do
      host = build(:host)

      expect { described_class.find_or_create_for_usage_data!(host, usage_data) }
        .to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  describe '.by_license' do
    let!(:current_host_stat) { create(:current_host_stat) }

    it 'returns empty list if no usage data exists for the host' do
      expect(described_class.by_license('md5')).to be_empty
    end

    it 'returns the record of a host found with the license' do
      expect(described_class.by_license(current_host_stat.license_md5)).to include(current_host_stat)
    end

    it 'returns all records of the hosts found with the license' do
      create(:current_host_stat)

      expect(described_class.by_license(current_host_stat.license_md5)).to match_array([current_host_stat])
    end
  end

  describe '.by_edition' do
    let!(:current_host_stat) { create(:current_host_stat) }

    it 'returns empty list if no edition found' do
      edition = 'bogus'

      expect(described_class.by_edition(edition)).to be_empty
    end

    it 'returns the record of a host found with the edition' do
      expect(described_class.by_edition(current_host_stat.edition)).to include(current_host_stat)
    end

    it 'returns all records of the hosts found with the edition' do
      create(:current_host_stat, edition: 'test_edition')

      expect(described_class.by_edition(current_host_stat.edition)).to match_array([current_host_stat])
    end
  end

  describe '.by_url' do
    let!(:current_host_stat) { create(:current_host_stat, url: 'http://example.com') }

    it 'has no matches' do
      expect(described_class.by_url('test')).to be_empty
    end

    it 'has one match' do
      expect(described_class.by_url('example')).to match([current_host_stat])
    end

    it 'has multiple matches' do
      current_host_stat_2 = create(:current_host_stat, url: 'http://another.example.com')

      expect(described_class.by_url('example')).to match([current_host_stat, current_host_stat_2])
    end
  end

  describe '#customers_account?' do
    it 'has a customer account' do
      expect(build(:current_host_stat, zuora_account_id: anything)).to be_customers_account
    end

    it 'does not have a customer account' do
      expect(build(:current_host_stat, zuora_account_id: nil)).not_to be_customers_account
    end
  end

  describe '.to_csv' do
    let(:controller_view_context) { CurrentHostStatsController.new.view_context }

    before do
      allow(controller_view_context).to receive(:host_url).and_return('this-is-a-test-host-url')
      create(:current_host_stat, url: 'https://my-test-site.com')
    end

    subject { described_class.with_host.limit(2).to_csv(controller_view_context) }

    it 'includes a header row' do
      header = 'host_id,host_link,active_users_count,historical_max_users_count,user_count,' \
               'version,usage_data_recorded_at,edition'

      expect(subject).to include(header)
    end

    it 'does not include the host url' do
      expect(subject).not_to include('https://my-test-site.com')
    end

    it 'includes the host link' do
      expect(subject).to include('this-is-a-test-host-url')
    end
  end
end
