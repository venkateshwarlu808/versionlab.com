# frozen_string_literal: true

require 'spec_helper'

describe ConversationalDevelopmentIndex do
  let(:conv_dev_index) do
    create(:conversational_development_index, leader_boards: 8, instance_boards: 2, leader_environments: 0)
  end

  describe '#create' do
    it 'calculates percentages' do
      expect(conv_dev_index.percentage_boards).to eq(25.0)
    end

    it 'returns json in the correct format' do
      result = JSON.parse(conv_dev_index.to_json)

      expect(result.keys).to match_array(
        [
          'Boards',
          'Pipelines',
          'Deployments',
          'Environments',
          'Issues',
          'Merge Requests',
          'Milestones',
          'Comments',
          'Monitoring',
          'Service Desk'
        ]
      )
      expect(result['Boards']).to eq(
        { 'instance_score' => 2.0, 'leader_score' => 8.0, 'percentage_score' => 25.0 }
      )
    end
  end
end
