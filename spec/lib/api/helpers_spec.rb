# frozen_string_literal: true

require 'spec_helper'

describe API::Helpers do
  subject { Class.new.include(described_class).new }

  let(:resource) { Version.most_recent.page(nil) }
  let(:base_query) { { page: nil } }

  describe '#current_user' do
    let(:tokens) { {} }

    before do
      allow(subject).to receive(:params).and_return(tokens)
      allow(subject).to receive(:env).and_return(tokens)
    end

    context 'without tokens' do
      it 'doesn\'t bother looking for users' do
        expect(User).not_to receive(:find_by)
        expect(subject.current_user).to eq(nil)
      end
    end

    context 'when token is in params' do
      let(:tokens) { { private_token: '_param_token_' } }

      it 'looks up the user by the private token' do
        expect(User).to receive(:find_by).with(private_token: '_param_token_')

        subject.current_user
      end

      it 'doesn\'t look for users more than once' do
        expect(User).to receive(:find_by).once

        subject.current_user
        subject.current_user
      end
    end

    context 'when token is in header' do
      let(:tokens) { { 'HTTP_PRIVATE_TOKEN' => '_header_token_' } }

      it 'looks up the user by the private token' do
        expect(User).to receive(:find_by).with(private_token: '_header_token_')

        subject.current_user
      end
    end
  end

  describe '#paginate' do
    context 'when no valid per page is passed' do
      let(:default_per_page) { Kaminari.config.default_per_page }
      let(:relation) { resource.per(default_per_page) }

      it 'has no per page parameter passed' do
        allow(subject).to receive(:params).and_return(base_query)

        expect(subject).to receive(:add_pagination_headers).with(relation, default_per_page)

        subject.paginate(resource)
      end

      it 'has a per page value of 0 passed' do
        local_params = base_query.merge(per_page: '0')
        allow(subject).to receive(:params).and_return(local_params)

        expect(subject).to receive(:add_pagination_headers).with(relation, default_per_page)

        subject.paginate(resource)
      end
    end

    context 'when valid per page value is passed' do
      it 'is non zero per page value' do
        per_page = 5
        relation = resource.per(per_page)
        local_params = base_query.merge(per_page: per_page.to_s)
        allow(subject).to receive(:params).and_return(local_params)

        expect(subject).to receive(:add_pagination_headers).with(relation, per_page)

        subject.paginate(resource)
      end
    end
  end
end
