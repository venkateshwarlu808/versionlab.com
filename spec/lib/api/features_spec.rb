# frozen_string_literal: true

require 'spec_helper'

describe API::Features, type: :request do
  let(:api_path) { '/api/v1' }
  let(:response_json) { JSON.parse(response.body, symbolize_names: true) }

  describe 'GET /features' do
    let(:path) { "#{api_path}/features" }
    let!(:version_11_8_0) { create(:version, :with_features, feature_count: 3, version: '11.8.0') }
    let!(:version_10_2_1) { create(:version, :with_features, feature_count: 2, version: '10.2.1') }
    let!(:version_10_2_0) { create(:version, :with_features, feature_count: 2, version: '10.2.0') }

    it 'returns features for a specific version' do
      get path,
          params: { version: '10.2.1' }

      feature = version_10_2_1.features.first
      expect(response).to have_http_status(200)
      expect(response_json.length).to eq(2)
      expect(response_json[0]).to include(
        applicable_roles: feature.applicable_roles,
        description: feature.description,
        read_more_url: feature.read_more_url,
        title: feature.title,
        version: feature.version.version
      )
    end

    it 'returns features for the latest version' do
      get path

      expect(response).to have_http_status(200)
      expect(response_json.length).to eq(3)
    end

    it 'returns features for a generalized version' do
      get path,
          params: { version: '10.2' }

      expect(response).to have_http_status(200)
      expect(response_json.length).to eq(4)
    end

    it 'returns features since a given time' do
      create(:feature, version: version_10_2_1, title: 'Newer feature', created_at: 2.hours.from_now)
      create(:feature, version: version_11_8_0, title: 'Newer newer feature', created_at: 3.hours.from_now)

      get path, params: { version: '10.2', since: 1.hour.from_now }

      expect(response).to have_http_status(200)
      expect(response_json.length).to eq(1)
      expect(response_json[0]).to include(
        title: 'Newer feature'
      )
    end

    it 'can filter down by a specific role' do
      create(:feature, version: version_10_2_1, title: 'Maintainer feature', applicable_roles: %w[maintainer admin])

      get path,
          params: { version: '10.2', roles: ['maintainer'] }

      expect(response).to have_http_status(200)
      expect(response_json.length).to eq(1)
      expect(response_json[0]).to include(
        title: 'Maintainer feature'
      )
    end

    it 'can filter down by specific roles' do
      create(:feature, version: version_10_2_1, title: 'Maintainer feature', applicable_roles: ['user'])
      create(:feature, version: version_10_2_1, title: 'Maintainer feature', applicable_roles: %w[maintainer admin])

      get path,
          params: { version: '10.2', roles: ['admin'] }

      expect(response).to have_http_status(200)
      expect(response_json.length).to eq(5)
    end

    it 'handles pagination' do
      create(:feature, version: version_11_8_0, title: 'Oldest feature', created_at: 2.hours.ago)

      get path,
          params: { per_page: 1, page: 4 }

      expect(response_json.count).to eq(1)
      expect(response_json[0]).to include(
        title: 'Oldest feature'
      )
    end

    it 'responds correctly when exceptions are raised on params' do
      get path,
          params: { since: 'foo' }

      expect(response).to have_http_status(422)
      expect(response_json).to eq(error: 'since is an invalid date')
    end
  end
end
