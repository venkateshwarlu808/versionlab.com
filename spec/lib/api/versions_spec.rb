# frozen_string_literal: true

require 'spec_helper'

describe API::Versions, type: :request do
  let(:api_path) { '/api/v1' }
  let(:user) { create(:user) }
  let(:auth_headers) { Hash[API::Helpers::PRIVATE_TOKEN_HEADER, user.private_token] }
  let(:response_json) { JSON.parse(response.body, symbolize_names: true) }

  let(:version) { create(:version, version: '10.1.1', details: 'testing', created_at: time) }
  let(:time) { Time.current }

  describe 'GET /versions' do
    let!(:version_11) { create(:version, version: '11.8.0', details: 'testing', created_at: time) }

    it 'returns versions data' do
      version

      get "#{api_path}/versions"

      expect(response_json).to eq(
        [
          { id: version.id, version: version.version, major: 10, minor: 1, details: 'testing',
            created_at: time.as_json },
          { id: version_11.id, version: version_11.version, major: 11, minor: 8, details: 'testing',
            created_at: time.as_json }
        ]
      )
    end

    it 'includes vulnerability data when authenticated' do
      get "#{api_path}/versions",
          headers: auth_headers

      expect(response_json[0]).to include(
        version: version_11.version,
        vulnerable: false
      )
    end

    it 'handles pagination' do
      version

      get "#{api_path}/versions",
          params: { per_page: 1, page: 2 }

      # this seems like the incorrect pagination for versions, but something
      # might be dependent on the order being this way.
      expect(response_json.count).to eq(1)
      expect(response_json[0]).to include(
        version: version_11.version
      )
    end
  end

  describe 'GET /versions/:version' do
    it 'returns a version by version' do
      get "#{api_path}/versions/v#{version.version}"

      expect(response_json).to include(
        version: version.version
      )
    end

    it 'returns a version by id' do
      get "#{api_path}/versions/#{version.id}"

      expect(response_json).to include(
        version: version.version
      )
    end
  end

  describe 'POST /versions' do
    let(:valid_params) { { version: '10.1.2', security_release_type: 'non_critical', details: 'testing' } }

    it 'responds with a 401 if unauthenticated' do
      post "#{api_path}/versions"

      expect(response).to have_http_status(:unauthorized)
      expect(response_json).to eq(message: '401 Unauthorized')
    end

    it 'creates a version' do
      expect do
        post "#{api_path}/versions",
             headers: auth_headers,
             params: valid_params
      end.to change(Version, :count).by(1)

      expect(Version.last.version).to eq valid_params[:version]
    end

    it 'marks previous versions as vulnerability_type = non-critical if a security_release_type is non-critical' do
      expect do
        post "#{api_path}/versions",
             headers: auth_headers,
             params: valid_params
      end.to change { version.reset.non_critical? }.to(true)
    end
  end

  describe 'PUT /versions/:id' do
    let(:valid_params) do
      {
        vulnerability_type: 'non_critical',
        security_release_type: 'non_critical',
        details: 'testing'
      }
    end

    it 'responds with a 401 if unauthenticated' do
      put "#{api_path}/versions/42"

      expect(response).to have_http_status(:unauthorized)
      expect(response_json).to eq(message: '401 Unauthorized')
    end

    it 'updates a version' do
      expect do
        put "#{api_path}/versions/v#{version.version}",
            headers: auth_headers,
            params: valid_params
      end.to change { version.reset.non_critical? }.to(true)
    end

    it 'does not allow updating the version of a version' do
      expect do
        put "#{api_path}/versions/#{version.id}",
            headers: auth_headers,
            params: valid_params.merge(version: '10.3.4')
      end.not_to change { version.reset.version }
    end

    it 'marks previous versions as vulnerable if a security patch' do
      older_version = create(:version, version: '10.1.0')

      expect do
        put "#{api_path}/versions/v#{version.version}",
            headers: auth_headers,
            params: valid_params
      end.to change { older_version.reset.non_critical? }.to(true)
    end
  end

  describe 'DELETE /version/:id' do
    it 'responds with a 401 if unauthenticated' do
      delete "#{api_path}/versions/42"

      expect(response).to have_http_status(:unauthorized)
      expect(response_json).to eq(message: '401 Unauthorized')
    end

    it 'deletes a version by version' do
      version

      expect do
        delete "#{api_path}/versions/v#{version.version}",
               headers: auth_headers
      end.to change(Version, :count).by(-1)
    end

    it 'deletes a version by id' do
      version

      expect do
        delete "#{api_path}/versions/#{version.id}",
               headers: auth_headers
      end.to change(Version, :count).by(-1)
    end
  end

  describe 'features' do
    let(:version) { create(:version, :with_features) }

    it 'returns features for a version' do
      get "#{api_path}/versions/v#{version.version}/features"

      feature = version.features.first
      expect(response_json[0]).to include(
        title: feature.title,
        description: feature.description,
        applicable_roles: feature.applicable_roles,
        read_more_url: feature.read_more_url
      )
    end
  end
end
