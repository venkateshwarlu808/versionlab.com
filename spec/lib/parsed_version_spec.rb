# frozen_string_literal: true

require 'spec_helper'

describe ParsedVersion do
  it 'raises an exception on invalid versions' do
    expect { described_class.new('x.x.x') }.to raise_error(
      ParsedVersion::InvalidVersionError,
      'version "x.x.x" is invalid for parsing.'
    )
  end

  it 'does not allow empty versions' do
    expect { described_class.new('') }.to raise_error(
      ParsedVersion::InvalidVersionError,
      'version "" is invalid for parsing.'
    )
  end

  it 'does not allow versions with too many parts' do
    expect { described_class.new('10.1.1.1.1') }.to raise_error(
      ParsedVersion::InvalidVersionError,
      'version "10.1.1.1.1" is invalid for parsing.'
    )
  end

  describe '#major' do
    it 'returns the major version of the parsed version' do
      subject = described_class.new('10.2.1')

      expect(subject.major).to eq '10'
    end
  end

  describe '#minor' do
    it 'returns the minor version of the parsed version' do
      subject = described_class.new('10.2.1')

      expect(subject.minor).to eq '2'
    end

    it 'returns nil if the parsed version does not include a minor version' do
      subject = described_class.new('10')

      expect(subject.minor).to eq nil
    end
  end

  describe '#patch' do
    it 'returns the patch version of the parsed version' do
      subject = described_class.new('10.2.1')

      expect(subject.patch).to eq '1'
    end

    it 'returns nil if the parsed version does not include a patch version' do
      subject = described_class.new('10.2')

      expect(subject.patch).to eq nil
    end
  end

  describe '#label' do
    it 'returns the label of the parsed version if one is present' do
      subject = described_class.new('10.2.1.pre')

      expect(subject.label).to eq 'pre'
    end

    it 'returns nil if the parsed version does not include a label' do
      subject = described_class.new('10.2.1')

      expect(subject.label).to eq nil
    end
  end

  describe '#precision' do
    it 'returns :patch when the precision is patch' do
      subject = described_class.new('10.2.1')

      expect(subject.precision).to eq :patch
    end

    it 'returns :minor when the precision is minor' do
      subject = described_class.new('10.2')

      expect(subject.precision).to eq :minor
    end

    it 'returns :minor when the precision is major' do
      subject = described_class.new('10')

      expect(subject.precision).to eq :major
    end

    it 'returns :label when a label is present' do
      subject = described_class.new('10.2.1.pre_release')

      expect(subject.precision).to eq :label
    end

    describe 'with odd scenarios' do
      it 'understands loosely how to handle things like 10.pre-release' do
        subject = described_class.new('10.pre_release')

        # it's major, not label here, because label is special
        expect(subject.precision).to eq :major

        expect(subject.label).to eq 'pre_release'
        expect(subject.to_a).to eq [10, 'pre_release']
      end
    end
  end

  describe '#to_a' do
    it 'understands versions that include all parts' do
      subject = described_class.new('10.2.1.pre')

      expect(subject.to_a).to eq [10, 2, 1, 'pre']
      expect(subject.to_a(:label)).to eq [10, 2, 1, 'pre']
      expect(subject.to_a(:patch)).to eq [10, 2, 1]
      expect(subject.to_a(:minor)).to eq [10, 2]
      expect(subject.to_a(:major)).to eq [10]
    end

    it 'understands versions that go to the patch level' do
      subject = described_class.new('10.2.1')

      expect(subject.to_a).to eq [10, 2, 1]
      expect(subject.to_a(:label)).to eq [10, 2, 1, nil]
      expect(subject.to_a(:patch)).to eq [10, 2, 1]
      expect(subject.to_a(:minor)).to eq [10, 2]
      expect(subject.to_a(:major)).to eq [10]
    end

    it 'understands versions that only go to minor level' do
      subject = described_class.new('10.2')

      expect(subject.to_a).to eq [10, 2]
      expect(subject.to_a(:label)).to eq [10, 2, 0, nil]
      expect(subject.to_a(:patch)).to eq [10, 2, 0]
      expect(subject.to_a(:minor)).to eq [10, 2]
      expect(subject.to_a(:major)).to eq [10]
    end

    it 'understands versions that only go to major level' do
      subject = described_class.new('10')

      expect(subject.to_a).to eq [10]
      expect(subject.to_a(:label)).to eq [10, 0, 0, nil]
      expect(subject.to_a(:patch)).to eq [10, 0, 0]
      expect(subject.to_a(:minor)).to eq [10, 0]
      expect(subject.to_a(:major)).to eq [10]
    end
  end

  describe '#to_query' do
    it 'understands that there could need to be a decimal in the query based on precision' do
      subject = described_class.new('10.2.1.pre')

      expect(subject.to_query).to eq '10.2.1.pre%'
      expect(subject.to_query(:patch)).to eq '10.2.1%'
      expect(subject.to_query(:minor)).to eq '10.2.%'
      expect(subject.to_query(:major)).to eq '10.%'
    end
  end

  describe '#to_s' do
    it 'calls through to to_a and joins that into a version with decimals' do
      subject = described_class.new('10')

      expect(subject).to receive(:to_a).and_return([10, 2, 1, 'pre'])
      expect(subject.to_s).to eq '10.2.1.pre'
    end
  end
end
