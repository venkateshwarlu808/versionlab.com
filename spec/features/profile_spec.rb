# frozen_string_literal: true

require 'spec_helper'

describe 'Profile', :skip do
  let(:user) { create(:user) }

  before do
    login_with(user)
  end

  it "resets user's private token" do
    old_token = user.private_token

    visit profile_path

    page.within '.reset-token' do
      expect(find("#token").value).to eq old_token
      click_button "Reset private token"
    end

    expect(page).to have_content("Token was successfully updated.")
    expect(find("#token").value).not_to eq old_token
    expect(find("#token").value).to eq user.reset.private_token
  end
end
