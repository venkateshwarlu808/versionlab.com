# frozen_string_literal: true

require 'spec_helper'

describe 'show page', :skip do
  let(:user) { create(:user) }

  before do
    login_with(user)
    visit usage_datum_path(usage_data)
  end

  context 'when there is usage data' do
    let(:usage_data) { create(:usage_data, :with_stats) }

    it 'includes service ping data table' do
      expect(page).to have_content('Each line in this service ping')
    end

    it 'shows a popup for a count metric', :js do
      click_link 'ci_builds'

      expect(page).to have_content('Close')
    end
  end

  context 'when there is no usage data' do
    let(:usage_data) { create(:usage_data) }

    it 'does not include service ping data table' do
      expect(page).not_to have_content('Each line in this service ping')
    end
  end
end
