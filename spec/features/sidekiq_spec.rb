# frozen_string_literal: true

require 'spec_helper'

describe 'Sidekiq', :skip do
  describe 'UI functionality', :redis do
    let(:user) { create(:user) }

    before do
      login_with(user)
    end

    it 'loads the sidekiq admin UI successfully' do
      visit sidekiq_path

      expect(page).to have_text('Dashboard')
    end
  end
end
