# frozen_string_literal: true

require 'spec_helper'

describe Salesforce::BackfillService do
  describe '#execute' do
    context 'when no params are passed' do
      subject { described_class.new(nil) }

      it 'returns zero' do
        expect(subject.execute).to eq(0)
      end

      it 'does not schedule any jobs' do
        expect(UsageDataWorker).not_to receive(:bulk_perform_async)

        subject.execute
      end
    end

    context 'when the params are not valid month_editions' do
      subject { described_class.new(['172018-02-CE', '2017-CE', '2017-01-']) }

      it 'returns zero' do
        expect(subject.execute).to eq(0)
      end

      it 'does not schedule any jobs' do
        expect(UsageDataWorker).not_to receive(:bulk_perform_async)

        subject.execute
      end
    end

    context 'when month_editions are passed' do
      let!(:ce_january_1) { create(:usage_data, :ce, created_at: Time.utc(2017, 1)) }
      let!(:ce_january_2) { create(:usage_data, :ce, created_at: Time.utc(2017, 1)) }
      let!(:ce_february_1) { create(:usage_data, :ce, created_at: Time.utc(2017, 2)) }
      let!(:ce_march_1) { create(:usage_data, :ce, created_at: Time.utc(2017, 3)) }
      let!(:ee_january_1) { create(:usage_data, edition: 'EE', created_at: Time.utc(2017, 1)) }
      let!(:ee_january_2) { create(:usage_data, edition: nil, created_at: Time.utc(2017, 1)) }

      subject { described_class.new(%w[2017-01-EE 2017-02-CE]) }

      before do
        allow(UsageDataWorker).to receive(:bulk_perform_async, &:itself)
      end

      it 'returns the number of jobs created' do
        expect(subject.execute).to eq(3)
      end

      it 'includes EE service pings where edition is nil' do
        expect(UsageDataWorker)
          .to receive(:bulk_perform_async)
          .with(a_collection_containing_exactly([ee_january_1.id], [ee_january_2.id]))
          .and_return(Array.new(2))

        expect(UsageDataWorker)
          .to receive(:bulk_perform_async)
          .with([[ce_february_1.id]])
          .and_return(Array.new(1))

        subject.execute
      end
    end
  end
end
