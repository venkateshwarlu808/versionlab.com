# frozen_string_literal: true

require 'spec_helper'

describe Salesforce::AccountMatcherService do
  describe '#execute' do
    context 'when a service ping without a license is passed' do
      subject { described_class.new(create(:usage_data, license_md5: '', license_sha256: nil)) }

      it 'returns nil' do
        expect(subject.execute).to be_nil
      end

      it 'does not make any license app or Salesforce requests' do
        expect(LicenseAppService).not_to receive(:new)
        expect(subject).not_to receive(:query)

        subject.execute
      end
    end

    context 'when a service ping with a license with only sha256 hash is passed' do
      subject { described_class.new(create(:usage_data, license_md5: '', license_sha256: 'abc23')) }

      before do
        license_app_service = instance_double(LicenseAppService, get_license: nil)
        allow(LicenseAppService).to receive(:new).and_return(license_app_service)
      end

      it 'returns nil' do
        expect(subject.execute).to be_nil
      end

      it 'makes a license app request' do
        expect(LicenseAppService).to receive(:new)

        subject.execute
      end
    end

    context 'when a service ping with a license with only an md5 hash is passed' do
      subject { described_class.new(create(:usage_data, license_md5: 'abc23', license_sha256: '')) }

      before do
        license_app_service = instance_double(LicenseAppService, get_license: nil)
        allow(LicenseAppService).to receive(:new).and_return(license_app_service)
      end

      it 'returns nil' do
        expect(subject.execute).to be_nil
      end

      it 'makes a license app request' do
        expect(LicenseAppService).to receive(:new)

        subject.execute
      end
    end

    context 'when a service ping with license_md5 and license_sha256 is passed' do
      let(:usage_data) { create(:usage_data, :eep) }

      subject { described_class.new(usage_data) }

      context 'when no matching license is found' do
        before do
          stub_license_show_request(usage_data.license_md5, nil)
          stub_license_show_request(usage_data.license_sha256, nil)
        end

        it 'returns nil' do
          expect(subject.execute).to be_nil
        end

        it 'does not make any Salesforce requests' do
          expect(subject).not_to receive(:query)

          subject.execute
        end
      end

      context 'when a matching license is found' do
        let(:zuora_id) { 'zuora_id123456' }
        let(:subscriptions) { [{ Id: '12', Zuora__Account__c: 'ac12' }, { Id: '34', Zuora__Account__c: 'ac34' }] }
        let(:accounts) { [{ Id: 'ac12' }, { Id: 'ac34' }] }

        let(:zuora_query) do
          "SELECT Zuora__Account__c FROM Zuora__Subscription__c WHERE Zuora__Zuora_Id__c = '#{zuora_id}'"
        end

        before do
          stub_license_show_request(usage_data.license_md5, { 'zuora_subscription_id' => zuora_id })

          allow(Rails.logger).to receive(:warn)
        end

        context 'when one account is found' do
          before do
            allow(subject)
              .to receive(:query).with(zuora_query).and_return(subscriptions.take(1))
          end

          it 'returns the correct account and Zuora subscription IDs' do
            expect(subject.execute).to eq(account_id: 'ac12', zuora_subscription_id: zuora_id)
          end

          it 'provides a valid account_id' do
            subject.execute

            expect(subject.account_id).to eq 'ac12'
          end
        end

        context 'when multiple accounts are found' do
          before do
            allow(subject)
              .to receive(:query).with(zuora_query).and_return(subscriptions)
          end

          it 'returns the first account ID and the correct Zuora subscription ID' do
            expect(subject.execute).to eq(account_id: 'ac12', zuora_subscription_id: zuora_id)
          end

          it 'logs that multiple accounts were found' do
            expect(Rails.logger)
              .to receive(:warn).with("#{described_class}: 2 accounts found for zuora_subscription_id zuora_id123456")

            subject.execute
          end
        end

        context 'when no accounts are found' do
          before do
            allow(subject)
              .to receive(:query).with(zuora_query).and_return([])
          end

          it 'returns nil' do
            expect(subject.execute).to be_nil
          end

          it 'logs that no accounts were found' do
            expect(Rails.logger)
              .to receive(:warn).with("#{described_class}: No account found for zuora_subscription_id zuora_id123456")

            subject.execute
          end
        end
      end
    end
  end
end
