# frozen_string_literal: true

require 'spec_helper'

describe Salesforce::PushUsageDataService do
  describe '#execute' do
    # TODO: this seems to be tautologic
    let(:expected_hash) do
      {
        active_user_count__c: usage_data.active_user_count,
        conv_dev_index__c: usage_data.conversational_development_index.try(:to_json) || "",
        counts__c: usage_data.stats.to_json,
        created_at__c: usage_data.created_at.iso8601,
        edition__c: usage_data.edition,
        historical_max_users__c: usage_data.historical_max_users,
        hostname__c: usage_data.hostname,
        installation_type__c: usage_data.installation_type || "",
        license_add_ons__c: usage_data.license_add_ons.try(:to_json) || "",
        license_expires_at__c: usage_data.license_expires_at.try(:iso8601) || "",
        license_md5__c: usage_data.license_md5,
        license_sha256__c: usage_data.license_sha256,
        license_restricted_count__c: usage_data.license_restricted_user_count,
        license_starts_at__c: '',
        license_user_count__c: usage_data.license_user_count,
        license_trial__c: usage_data.license_trial?,
        licensee__c: '',
        mattermost_enabled__c: !!usage_data.mattermost_enabled,
        recorded_at__c: usage_data.recorded_at.try(:iso8601) || "",
        source_ip__c: usage_data.source_ip,
        updated_at__c: usage_data.updated_at.iso8601,
        usage_data_id__c: usage_data.id,
        uuid__c: usage_data.uuid,
        version__c: usage_data.version
      }
    end

    shared_examples 'Salesforce request' do |type|
      context 'when the service ping already exists in Salesforce' do
        before do
          allow(subject).to receive(:create!).and_raise(Faraday::ClientError, "DUPLICATE_VALUE: duplicate value found")
        end

        it 'returns true' do
          expect(subject.execute).to be_truthy
        end

        it 'logs the exception' do
          expect(Rails.logger).to receive(:info).with(a_string_matching(usage_data.id.to_s))

          subject.execute
        end
      end

      context 'when the service ping sends extrameous fields' do
        before do
          allow(subject).to receive(:create!)
                              .and_raise(Faraday::ClientError, "INVALID_FIELD: No such column 'extra_field'")
        end

        it 'logs the exception' do
          expect(Rails.logger).to receive(:info).with(a_string_matching(usage_data.id.to_s))
          expect { subject.execute }.to raise_error(Faraday::ClientError)
        end
      end

      context 'when the request succeeds' do
        it "creates a service ping in Salesforce with the #{type} data" do
          expect(subject).to receive(:create!).with('Usage_Ping_Data__c', expected_hash)

          subject.execute
        end
      end
    end

    context 'when a zuora_subscription_id and account_id are passed' do
      let(:usage_data) { create(:usage_data, :eep, historical_max_users: 10) }
      let!(:conv_dev_index) { create(:conversational_development_index, usage_data: usage_data) }

      context 'when the account_id param is blank' do
        subject { described_class.new(usage_data, zuora_subscription_id: 'zuora123', account_id: '') }

        it 'does not create a service ping in Salesforce' do
          expect(subject).not_to receive(:create!)

          subject.execute
        end
      end

      context 'when the zuora_subscription_id param is blank' do
        let(:expected_hash) { super().merge zuora_subscription_id__c: '', Account__c: 'ac12' }

        subject { described_class.new(usage_data, zuora_subscription_id: '', account_id: 'ac12') }

        include_examples 'Salesforce request', 'account'
      end

      context 'when both params are present' do
        let(:expected_hash) { super().merge zuora_subscription_id__c: 'zuora123', Account__c: 'ac12' }

        subject { described_class.new(usage_data, zuora_subscription_id: 'zuora123', account_id: 'ac12') }

        context 'when the license is valid' do
          include_examples 'Salesforce request', 'account'
        end

        context 'when the license has a far-past start date' do
          let(:expected_hash) { super().merge license_starts_at__c: '2017-02-01T00:00:00Z' }

          before do
            # https://license.gitlab.com/licenses/10540
            usage_data.update(license_starts_at: Time.utc(17, 2, 1))
          end

          include_examples 'Salesforce request', 'account'
        end

        context 'when the license has a far-future expiration date' do
          let(:expected_hash) { super().merge license_expires_at__c: '' }

          before do
            # https://license.gitlab.com/licenses/13686
            usage_data.update(license_expires_at: Time.utc(172018))
          end

          include_examples 'Salesforce request', 'account'
        end

        context 'when the license has no trial information' do
          let(:expected_hash) { super().merge(license_trial__c: false) }

          before do
            usage_data.update(license_trial: nil)
          end

          include_examples 'Salesforce request', 'account'
        end

        context 'when the license has trial information' do
          let(:expected_hash) { super().merge(license_trial__c: true) }

          before do
            usage_data.update(license_trial: true)
          end

          include_examples 'Salesforce request', 'account'
        end
      end
    end

    context 'when a zuora_subscription_id is not passed' do
      subject { described_class.new(create(:usage_data), {}) }

      it 'does nothing' do
        expect(subject).not_to receive(:create!)

        subject.execute
      end
    end

    context 'when usage data contains stats' do
      let(:usage_data) { create(:usage_data, :with_stats) }

      let(:expected_hash) do
        count = described_class.map_fields(UsageDataHelpers::DEFAULT_COUNTS, :count)
        super().merge zuora_subscription_id__c: 'zuora456',
                      Account__c: 'ac34',
                      **count
      end

      subject { described_class.new(usage_data, zuora_subscription_id: 'zuora456', account_id: 'ac34') }

      include_examples 'Salesforce request', 'account'

      context 'when the stats contains extraneous fields' do
        before do
          usage_data.stats['a_hundred'] = 100
          allow(subject).to receive(:create!) # do not make the call
        end

        include_examples 'Salesforce request', 'account'

        it 'logs the extraneous fields' do
          expect(Rails.logger).to receive(:warn).with(a_string_matching('a_hundred'))
          subject.execute
        end
      end
    end
  end

  describe '.param_name' do
    subject { described_class.param_name(:count, 'a_sample_field') }

    it { is_expected.to be_equal :count_a_sample_field__c }

    context 'with metric mapping' do
      before do
        stub_const('Salesforce::METRICS_MAPPINGS', { a_sample_field: :another_field })
      end

      it { is_expected.to be_equal :count_another_field__c }
    end
  end
end
