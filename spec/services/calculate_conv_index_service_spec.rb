# frozen_string_literal: true

require 'spec_helper'

describe CalculateConvIndexService do
  let(:service) { described_class.new }
  let(:uuid) { 'a' }
  let(:end_period) { Time.zone.now }

  context 'when there are not enough records in the time window' do
    it 'does not perform calculations' do
      create(:usage_data, :recent, uuid: uuid)

      success, _ = service.execute(uuid, end_period)

      expect(success).to be false
    end
  end

  describe 'calculation with 3 records' do
    it 'compares the newest and the oldest record' do
      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 30.days.ago,
        stats: {
          issues: 10
        },
        active_user_count: 3
      )
      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 21.days.ago,
        stats: {
          issues: 17
        },
        active_user_count: 4
      )
      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 4.days.ago,
        stats: {
          issues: 18
        },
        active_user_count: 5
      )

      success, result = service.execute(uuid, end_period)

      expect(success).to be true
      expect(result['issues']).to be_within(0.1).of(2.66)
    end
  end

  describe 'missing data' do
    it 'assumes 0 if value is missing' do
      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 30.days.ago,
        stats: {
          issues: 10
        },
        active_user_count: 4
      )
      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 21.days.ago,
        stats: {
          boards: 20
        },
        active_user_count: 6
      )

      success, result = service.execute(uuid, end_period)

      expect(success).to be true
      expect(result['issues']).to eq 0
      expect(result['boards']).to eq 5
    end
  end

  describe 'no active users' do
    it 'assumes 1 active user' do
      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 30.days.ago,
        stats: {
          deployments: 2
        },
        active_user_count: 0
      )
      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 21.days.ago,
        stats: {
          deployments: 4
        },
        active_user_count: 0
      )

      success, result = service.execute(uuid, end_period)

      expect(success).to be true
      expect(result['deployments']).to eq 2
    end
  end

  describe 'projects_prometheus_active metric' do
    it 'calculates ratio using the newest record' do
      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 30.days.ago,
        stats: {
          projects_prometheus_active: 1,
          projects: 10
        },
        active_user_count: 2
      )
      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 21.days.ago,
        stats: {
          projects_prometheus_active: 3,
          projects: 8
        },
        active_user_count: 2
      )

      success, result = service.execute(uuid, end_period)

      expect(success).to be true
      expect(result['projects_prometheus_active']).to eq 0.375
    end
  end

  describe 'ci_pipelines metric' do
    it 'calculates conv index score' do
      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 30.days.ago,
        stats: {
          ci_pipelines: 10
        }
      )

      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 30.days.ago,
        stats: {
          ci_pipelines: 100
        }
      )

      success, result = service.execute(uuid, end_period)

      expect(success).to be true
      expect(result['ci_pipelines']).to eq(90.0)
    end

    it 'handles new ci_pipeline parameters' do
      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 30.days.ago,
        stats: {
          ci_internal_pipelines: 2,
          ci_external_pipelines: 8
        }
      )

      create(
        :usage_data,
        uuid: uuid,
        recorded_at: 15.days.ago,
        stats: {
          ci_internal_pipelines: 20,
          ci_external_pipelines: 80
        }
      )

      success, result = service.execute(uuid, end_period)

      expect(success).to be true
      expect(result['ci_pipelines']).to eq(90.0)
    end
  end
end
