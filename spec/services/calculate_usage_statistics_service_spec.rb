# frozen_string_literal: true

require 'spec_helper'

describe CalculateUsageStatisticsService do
  let(:service) { described_class.new }

  describe 'time window' do
    it 'ignores instances that were not active in the last 30 days' do
      create(:usage_data, :recent, stats: { ci_pipelines: 5 }, uuid: '1')
      create(:usage_data, stats: { ci_pipelines: 1 }, uuid: '2', recorded_at: 31.days.ago)

      expect(service.execute).to eq('ci_pipelines' => 5)
    end
  end

  describe 'multiple pings from the same instance' do
    it 'uses the latest ping' do
      create(:usage_data, stats: { ci_pipelines: 5 }, uuid: '1', recorded_at: 2.days.ago)
      create(:usage_data, stats: { ci_pipelines: 1 }, uuid: '1', recorded_at: 3.days.ago)

      expect(service.execute).to eq('ci_pipelines' => 5)
    end
  end

  describe 'missing data' do
    it 'ignores service pings with missing data' do
      create(:usage_data, :recent, stats: nil, uuid: '1')
      create(:usage_data, :recent, stats: { ci_pipelines: 1 }, uuid: '2')
      create(:usage_data, :recent, stats: { ci_builds: 2, ci_pipelines: 3 }, uuid: '3')

      expect(service.execute).to eq('ci_builds' => 2, 'ci_pipelines' => 2)
    end
  end

  describe 'very large numbers' do
    it 'does not raise an error' do
      failed_big_number = 70_368_744_183_808
      postgres_max_int_plus_one = 2147483647 + 1
      stats = { ci_pipelines: failed_big_number, ci_builds: postgres_max_int_plus_one }

      create(:usage_data, stats: stats, uuid: '1', recorded_at: 2.days.ago)

      expect(service.execute).to eq(stats.stringify_keys!)
    end
  end
end
