# frozen_string_literal: true

require 'spec_helper'
require 'rspec-parameterized'

describe LicenseAppService do
  describe '#get_license' do
    using RSpec::Parameterized::TableSyntax

    where(:license_sha256, :license_md5, :license_id, :sha256_reponse, :md5_reponse, :id_reponse, :result) do
      nil         | nil          | nil | false | false | false | nil
      nil         | '872bd4cb53' | nil | false | false | false | nil
      nil         | '872bd4cb53' | nil | false | true  | false | { 'key' => 'result' }
      nil         | '872bd4cb53' | 123 | false | false | true  | { 'key' => 'result' }
      nil         | '872bd4cb53' | 123 | false | true  | false | { 'key' => 'result' }

      '872bd4cb5' | '872bd4cb53' | 123 | false | false | false | nil
      '872bd4cb5' | '872bd4cb53' | 123 | true  | false | false | { 'key' => 'result' }
      '872bd4cb5' | '872bd4cb53' | 123 | false | true  | false | { 'key' => 'result' }
      '872bd4cb5' | '872bd4cb53' | 123 | false | false | true  | { 'key' => 'result' }

      '872bd4cb5' | nil          | 123 | false | false | false | nil
      '872bd4cb5' | nil          | 123 | false | false | true  | { 'key' => 'result' }
      '872bd4cb5' | nil          | 123 | true  | false | false | { 'key' => 'result' }

      '872bd4cb5' | nil          | nil | false | false | false | nil
      '872bd4cb5' | nil          | nil | true  | false | false | { 'key' => 'result' }
    end

    with_them do
      subject do
        described_class.new(
          license_sha256: license_sha256,
          license_md5: license_md5,
          license_id: license_id
        ).get_license
      end

      before do
        default_response = { 'key' => 'result' }

        stub_license_show_request(license_sha256, sha256_reponse ? default_response : nil)
        stub_license_show_request(license_md5, md5_reponse ? default_response : nil)
        stub_license_show_request(license_id, id_reponse ? default_response : nil)
      end

      it { is_expected.to eq(result) }
    end
  end
end
