# frozen_string_literal: true

require 'spec_helper'

describe EventTracker do
  let(:app_id) { 'app_id' }
  let(:host) { 'host' }
  let(:sdk_enabled) { 'true' }
  let(:event_name) { 'event_name' }
  let(:event_payload) { { pay: 'load' } }
  let(:user_data) { 'user_data' }
  let(:sdk_client) { instance_double(GitlabSDK::Client) }

  before do
    stub_env('BROWSER_SDK_APP_ID', app_id)
    stub_env('BROWSER_SDK_HOST', host)
    stub_env('BACKEND_SDK_ENABLED', sdk_enabled)
  end

  describe '.track' do
    subject { described_class.new.track(event_name, event_payload) }

    it "tracks the event" do
      expect(GitlabSDK::Client).to receive(:new).with(
        app_id: app_id,
        host: host
      ).and_return(sdk_client)

      expect(sdk_client).to receive(:track).with(event_name, event_payload)

      subject
    end

    context "with sdk disabled" do
      let(:sdk_enabled) { 'false' }

      it "doesn't track the event" do
        expect(GitlabSDK::Client).not_to receive(:new)

        subject
      end
    end
  end

  describe '.identify' do
    subject { described_class.new.identify(user_data) }

    it "identifies" do
      expect(GitlabSDK::Client).to receive(:new).with(
        app_id: app_id,
        host: host
      ).and_return(sdk_client)

      expect(sdk_client).to receive(:identify).with(user_data)

      subject
    end

    context "with sdk disabled" do
      let(:sdk_enabled) { 'false' }

      it "doesn't identify" do
        expect(GitlabSDK::Client).not_to receive(:new)

        subject
      end
    end
  end
end
