# frozen_string_literal: true

require 'spec_helper'

describe UsagePing::ServiceHandler, :usage_data do
  let(:source_ip) { '8.8.8.8' }
  let(:params) { test_usage_data }
  let(:raw_usage_data) { create(:raw_usage_data, uuid: "1", recorded_at: Time.now, payload: params) }

  shared_examples 'host association' do
    before do
      allow(UsageDataWorker).to receive(:perform_async)
    end

    context 'when a host found by the usage data url' do
      let!(:host) { create(:host, url: 'example.com') }

      it 'does not create a new host' do
        expect { subject.execute(params, source_ip, raw_usage_data.id) }.not_to change(Host, :count)
      end

      it 'associates the created usage data to a host' do
        subject.execute(params, source_ip, raw_usage_data.id)

        expect(UsageData.last.host).to eq(host)
      end
    end

    context 'when a host not found by the usage data url' do
      it 'creates a new host' do
        expect { subject.execute(params, source_ip, raw_usage_data.id) }.to change(Host, :count).from(0).to(1)
      end

      it 'associates the created usage data to a new host' do
        subject.execute(params, source_ip, raw_usage_data.id)

        expect(UsageData.last.host).to eq(Host.last)
      end
    end
  end

  context 'when license found' do
    before do
      allow(UsageDataWorker).to receive(:perform_async)
    end

    it 'sets source IP' do
      subject.execute(params, source_ip, raw_usage_data.id)

      expect(UsageData.last.source_ip).to eq source_ip
    end

    it 'returns conv_index data' do
      expect(subject.execute(params, source_ip, raw_usage_data.id)).to have_key(:conv_index)
    end
  end

  context 'when 11.0' do
    context 'when license not found' do
      it_behaves_like 'host association' do
        let(:params) { test_usage_data }
      end
    end

    it 'handles a top-level container_registry key' do
      subject.execute({ container_registry: false }, source_ip, raw_usage_data.id)

      expect(UsageData.last.container_registry_enabled).to eq(false)
    end
  end

  context 'when there are no stats' do
    it 'skips stats processing' do
      subject.execute(test_usage_data_without_stats, source_ip, raw_usage_data.id)

      expect(UsageData.last.stats).to match({})
    end
  end

  context 'when recent payload' do
    let(:params) { test_usage_data_recent }

    context 'when license not found' do
      it_behaves_like 'host association' do
        let(:params) { test_usage_data_recent }
      end
    end

    context 'with stats formatting' do
      it 'stats are correct without massaging' do
        subject.execute(test_usage_data_fixed_epics_stats, source_ip, raw_usage_data.id)

        expect(UsageData.last.stats.symbolize_keys).to match(test_usage_fixed_epics_stats)
      end

      it 'stats massaged to be numeric' do
        subject.execute(params, source_ip, raw_usage_data.id)

        expect(UsageData.last.stats.symbolize_keys).to match(test_usage_fixed_epics_stats)
      end
    end
  end

  context 'when avg_cycle_analytics param is present' do
    let(:params) { test_usage_data.merge(avg_cycle_analytics: test_cycle_analytics_data) }

    it 'creates a new average cycle analytics' do
      expect { subject.execute(params, source_ip, raw_usage_data.id) }
        .to change(AvgCycleAnalytics, :count).from(0).to(1)
    end

    it 'stores the average cycle analytics data' do
      subject.execute(params, source_ip, raw_usage_data.id)

      expect(AvgCycleAnalytics.last).to have_attributes(cycle_analytics_attributes)
    end
  end

  context 'when avg_cycle_analytics param is not present' do
    it 'does not raise an error' do
      expect { subject.execute(params, source_ip, raw_usage_data.id) }.not_to raise_error
    end
  end

  context 'when not able to correctly parse stats' do
    it 'has non integer results' do
      bad_stats = { stats: { bad_stat: 'something' } }

      expect { subject.execute(test_usage_data_fixed_epics_stats.deep_merge(bad_stats), source_ip, raw_usage_data.id) }
        .to raise_error(UsagePing::StatsParser::NonIntegerError)
              .with_message("Non integer stats exist: #{bad_stats[:stats]}")
    end
  end

  context 'with raw_usage_data_id' do
    it 'saves the raw_usage_data_id' do
      subject.execute(params, source_ip, raw_usage_data.id)

      expect(UsageData.last.raw_usage_data_id).to eq(raw_usage_data.id)
    end

    it 'saves a nil raw_usage_data_id for nil id sent' do
      subject.execute(params, source_ip, nil)

      expect(UsageData.last.raw_usage_data_id).to eq(nil)
    end
  end

  describe 'current_host_stat' do
    context 'when a current_host_stat does exist' do
      it 'updates a host stat' do
        host_stat = create(:current_host_stat, url: params[:hostname], active_users_count: 0)

        subject.execute(params, source_ip, raw_usage_data.id)

        expect(host_stat.reset.active_users_count).to eq params[:active_user_count]
      end
    end

    context 'when a current_host_stat does not exist yet' do
      it 'creates a new current_host_stat with usage data' do
        expect { subject.execute(params, source_ip, raw_usage_data.id) }.to change(CurrentHostStat, :count)
        expect(CurrentHostStat.last).to have_attributes(url: params[:hostname],
                                                        host_created_at: Host.last.created_at,
                                                        usage_data_recorded_at: UsageData.last.recorded_at,
                                                        active_users_count: params[:active_user_count],
                                                        historical_max_users_count: params[:historical_max_users],
                                                        user_count: params[:license_user_count],
                                                        license_md5: params[:license_md5],
                                                        version: params[:version],
                                                        edition: params[:edition])
      end

      it 'has no hostname as part of the params' do
        local_params = params.merge({ hostname: nil })

        expect { subject.execute(local_params, source_ip, raw_usage_data.id) }.not_to change(CurrentHostStat, :count)
      end
    end
  end

  describe 'parameters cleaning' do
    context 'when license data is unreasonable' do
      let(:params) { test_usage_data.merge(license_user_count: 1_000_000_000) }

      it 'set license_user_count to 0' do
        subject.execute(params, source_ip, raw_usage_data.id)

        expect(UsageData.last.license_user_count).to be_zero
      end
    end
  end
end
