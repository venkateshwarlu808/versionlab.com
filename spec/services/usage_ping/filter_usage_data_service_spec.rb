# frozen_string_literal: true

require 'spec_helper'

describe UsagePing::FilterUsageDataService, :usage_data do
  describe '#execute' do
    let(:payload) { ActionController::Parameters.new(latest_service_ping_data) }

    subject { described_class.new(payload).execute }

    before do
      stub_metric_definitions_request
    end

    context 'with extra key in Service Ping' do
      let(:payload) { ActionController::Parameters.new(latest_service_ping_data).merge('random' => true) }

      it 'deletes any extra key' do
        expect(subject).not_to have_key('random')
      end
    end

    context 'without extra key in Service Ping' do
      it 'does not delete any key' do
        expect(subject.keys).to match_array(payload.keys)
      end
    end

    context 'with an empty payload' do
      let(:payload) { ActionController::Parameters.new }

      it 'returns an empty payload' do
        expect(subject).to eq({})
      end
    end

    context 'with an example payload' do
      let(:payload) do
        ActionController::Parameters.new(
          {
            counts: {
              foo: 5,
              quz: 0,
              bar: {
                baz: 6,
                xyzzy: nil
              },
              baz: {}
            },
            foobar: true
          }
        )
      end

      let(:metric_definitions) do
        allowed_key_paths.map do |key_path|
          { 'key_path' => key_path }
        end
      end

      before do
        allow_any_instance_of(UsagePing::MetricDefinitions).to receive(:execute).and_return(metric_definitions)
      end

      shared_examples 'a correctly filtered payload' do
        it 'correctly builds the filtered payload' do
          expect(subject).to eq(expected_payload)
        end
      end

      context 'with an existing key path' do
        let(:allowed_key_paths) { ['counts.foo', 'counts.bar.xyzzy', 'foobar'] }
        let(:expected_payload) do
          ActionController::Parameters.new(
            {
              counts: {
                foo: 5,
                bar: {
                  xyzzy: nil
                }
              },
              foobar: true
            }
          )
        end

        it_behaves_like 'a correctly filtered payload'
      end

      context 'with metric having an empty Hash as a value' do
        let(:allowed_key_paths) { ['counts.baz'] }
        let(:expected_payload) do
          ActionController::Parameters.new(
            {
              counts: {
                baz: {}
              }
            }
          )
        end

        it_behaves_like 'a correctly filtered payload'
      end

      context 'with key path not pointing to an end metric' do
        let(:allowed_key_paths) { ['counts.bar', 'counts'] }
        let(:expected_payload) do
          ActionController::Parameters.new
        end

        it_behaves_like 'a correctly filtered payload'
      end

      context 'with key path having a non-existing end metric' do
        let(:allowed_key_paths) { ['counts.foo', 'counts.bar.foobar'] }
        let(:expected_payload) do
          ActionController::Parameters.new(
            {
              counts: {
                foo: 5
              }
            }
          )
        end

        it_behaves_like 'a correctly filtered payload'
      end

      context 'with key path having non-existing parts' do
        let(:allowed_key_paths) { ['counts.foobar.baz', 'foobar'] }
        let(:expected_payload) do
          ActionController::Parameters.new(
            {
              foobar: true
            }
          )
        end

        it_behaves_like 'a correctly filtered payload'
      end

      context 'with key path having containing a legacy root key' do
        let(:payload) do
          ActionController::Parameters.new(
            {
              analytics_unique_visits: {
                foo: 5,
                quz: 0,
                bar: {
                  baz: 6,
                  xyzzy: nil
                },
                baz: {}
              },
              foobar: true
            }
          )
        end

        let(:allowed_key_paths) { ['analytics_unique_visits'] }
        let(:expected_payload) do
          ActionController::Parameters.new(
            {
              analytics_unique_visits: {
                foo: 5,
                quz: 0,
                bar: {
                  baz: 6,
                  xyzzy: nil
                },
                baz: {}
              }
            }
          )
        end

        it_behaves_like 'a correctly filtered payload'
      end
    end
  end
end
