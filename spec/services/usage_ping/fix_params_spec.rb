# frozen_string_literal: true

require 'spec_helper'
require 'rspec-parameterized'

describe UsagePing::FixParams do
  context 'with for version and edition bug' do
    using RSpec::Parameterized::TableSyntax

    where(:payload, :expected_edition, :expected_version) do
      [
        [
          {
            uuid: '0000',
            settings: { collected_data_categories: %w[Standard Subscription Operational] }
          },
          'EE',
          '14.1'
        ],
        [
          {
            uuid: '0000',
            settings: { collected_data_categories: %w[Standard Operational Subscription] }
          },
          'EE',
          '14.1'
        ],
        [
          {
            uuid: '0000',
            edition: 'EEP',
            version: '14.2',
            settings: { collected_data_categories: %w[Standard Operational Subscription] }
          },
          'EEP',
          '14.2'
        ],
        [
          {
            uuid: '0000',
            edition: 'EEP',
            version: '14.2',
            settings: { collected_data_categories: %w[Standard Subscription Operational Optional] }
          },
          'EEP',
          '14.2'
        ],
        [
          {
            uuid: '0000',
            edition: 'EEP',
            version: '14.0'
          },
          'EEP',
          '14.0'
        ],
        [
          {
            uuid: '0000',
            settings: {}
          },
          nil,
          nil
        ],
        [
          {
            uuid: '0000',
            settings: { collected_data_categories: %w[bogus] }
          },
          nil,
          nil
        ],

        # These cases should not occur. If they do, we won't be able to tell the version or edition.
        [
          {
            uuid: '0000',
            version: '14.2',
            settings: { collected_data_categories: %w[Standard Operational Subscription] }
          },
          nil,
          '14.2'
        ],
        [
          {
            uuid: '0000',
            edition: 'EEP',
            settings: { collected_data_categories: %w[Standard Operational Subscription] }
          },
          'EEP',
          nil
        ]
      ]
    end

    with_them do
      it 'returns the fixed params' do
        fixed_params = described_class.new.execute(payload)

        expect(fixed_params[:edition]).to eq(expected_edition)
        expect(fixed_params[:version]).to eq(expected_version)
        expect(fixed_params[:uuid]).to eq('0000')
      end
    end
  end

  context 'with gitaly.filesystems fallback bug' do
    let(:payload) do
      {
        uuid: '0000',
        version: '14.2',
        gitaly: {
          filesystems: -1,
          servers: 1,
          version: '14.2.14'
        }
      }
    end

    it 'returns a fallback Array' do
      fixed_params = described_class.new.execute(payload)

      expect(fixed_params[:gitaly][:filesystems]).to eq(["-1"])
    end
  end

  describe 'git version' do
    let(:expected_git) do
      {
        major: 2,
        minor: 30,
        patch: 2
      }
    end

    context 'with legacy formatter' do
      let(:payload) do
        {
          git: { version: '2.30.2' }
        }
      end

      it 'returns a fixed git version' do
        fixed_params = described_class.new.execute(payload)

        expect(fixed_params[:git][:version]).to eq(expected_git)
      end
    end

    context 'with new formatter' do
      let(:payload) do
        {
          git: { version: { major: 2, minor: 30, patch: 2 } }
        }
      end

      it 'returns original value' do
        fixed_params = described_class.new.execute(payload)

        expect(fixed_params[:git][:version]).to eq(expected_git)
      end
    end
  end
end
