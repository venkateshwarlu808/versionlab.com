# frozen_string_literal: true

require 'spec_helper'

describe UsagePing::ErrorHandler do
  describe '#execute!' do
    let(:version) { create :version }
    let(:host) { create :host }
    let(:error) do
      {
        uuid: '1',
        hostname: host.url,
        version: version.version,
        message: 'Error',
        elapsed: '10.5',
        time: Time.now.getutc.to_s
      }
    end

    let(:subject) { described_class.new.execute!(error) }

    context 'with correct parameters' do
      it 'not raising errors' do
        expect { subject }.not_to raise_error
      end

      it 'creates UsagePingError with correct values' do
        subject

        record = UsagePingError.last

        expect(record.uuid).to eq('1')
        expect(record.host_id).to eq(host.id)
        expect(record.version_id).to eq(version.id)
        expect(record.message).to eq('Error')
        expect(record.elapsed).to eq(10.5)
      end
    end

    context 'with corrupted parameters' do
      let(:error) do
        {
          message: "Filtered",
          hostname: nil
        }
      end

      it 'raise exception' do
        expect { subject }.to raise_error(StandardError)
      end
    end

    context 'when version doesnt exist in the database' do
      let(:error) do
        {
          uuid: '1',
          hostname: host.url,
          version: '14.10.666-pre',
          message: 'Error',
          elapsed: '10.5',
          time: Time.now.getutc.to_s
        }
      end

      it 'raises Non found error' do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context 'when version has suffix' do
      let(:error) do
        {
          uuid: '1',
          hostname: host.url,
          version: "#{version.version}-ee",
          message: 'Error',
          elapsed: '10.5',
          time: Time.now.getutc.to_s
        }
      end

      it 'parses version properly' do
        subject

        record = UsagePingError.last
        latest_version = Version.latest

        expect(record.version).to eq(latest_version)
      end
    end

    context 'when version does not contain expected format' do
      let(:error) do
        {
          uuid: '1',
          hostname: host.url,
          version: '_bogus_',
          message: 'Error',
          elapsed: '10.5',
          time: Time.now.getutc.to_s
        }
      end

      it 'raises ArgumentError' do
        expect do
          subject
        end.to raise_error(ArgumentError,
                           'Version format mismatch found for UsagePing error association with version _bogus_')
      end
    end
  end
end
