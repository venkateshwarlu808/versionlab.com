# frozen_string_literal: true

require_relative '../support/usage_data_helpers'

FactoryBot.define do
  factory :usage_data do
    edition { UsageData::CE_EDITION }
    uuid { '1' }
    sequence(:hostname) { |n| "https://gitlab#{n}.example.com" }
    host
    recorded_at { Time.now }

    trait :recent do
      recorded_at { 2.days.ago }
      recording_ce_finished_at { 2.days.ago + 30.minutes }
      recording_ee_finished_at { 2.days.ago + 120.minutes }
    end

    trait :ce do
    end

    trait :ee do
      edition { 'EE' }
      license_add_ons { '' }
      license_expires_at { Time.utc(2017, 6) }
      license_md5 { 'md5' }
      license_sha256 { 'sha256' }
      license_user_count { 20 }
      license_restricted_user_count { 20 }
    end

    trait :with_license_data do
      ee
      historical_max_users { 30 }
      licensee do
        {
          'Name': 'John',
          'Company': 'GitLab, Inc.',
          'Email': 'test@gitlab.com'
        }
      end
    end

    trait :ees do
      ee
      edition { 'EES' }
    end

    trait :eep do
      ee
      edition { 'EEP' }
    end

    trait :with_stats do
      stats { UsageDataHelpers::DEFAULT_COUNTS }
    end

    trait :with_bad_stats do
      stats { UsageDataHelpers::USAGE_BROKEN_STATS }
    end

    trait :with_good_stats do
      stats { UsageDataHelpers::USAGE_GOOD_STATS }
    end
  end
end
