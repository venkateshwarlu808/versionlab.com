# frozen_string_literal: true

FactoryBot.define do
  factory :avg_cycle_analytics do
    usage_data

    issue_average { 561600 }
    issue_sd { 0 }
    issue_missing { 3 }
    plan_average { 633600 }
    plan_sd { 35272 }
    plan_missing { 3 }
    code_average { 648002 }
    code_sd { 0 }
    code_missing { 3 }
    test_average { nil }
    test_sd { 0 }
    test_missing { 9 }
    review_average { 1368002 }
    review_sd { 7887 }
    review_missing { 3 }
    staging_average { nil }
    staging_sd { 0 }
    staging_missing { 9 }
    production_average { nil }
    production_sd { 0 }
    production_missing { 9 }
    total { 3254402 }
  end
end
