# frozen_string_literal: true

FactoryBot.define do
  factory :version do
    sequence(:version) { |sequence| "9.4.#{sequence}" }
    vulnerability_type { 0 }

    trait :with_features do
      transient do
        feature_count { 5 }
      end

      after(:create) do |version, evaluator|
        create_list(:feature, evaluator.feature_count, version: version)
      end
    end
  end
end
