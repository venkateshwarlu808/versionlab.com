# frozen_string_literal: true

FactoryBot.define do
  factory :version_check do
    gitlab_version { '9.0.0' }
    referer_url { 'http://localhost' }
  end
end
