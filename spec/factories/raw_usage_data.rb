# frozen_string_literal: true

FactoryBot.define do
  factory :raw_usage_data do
    recorded_at { 2.days.ago }
    uuid { '1' }
    payload { { name: 'John' } }
  end
end
