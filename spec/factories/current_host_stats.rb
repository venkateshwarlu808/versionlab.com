# frozen_string_literal: true

FactoryBot.define do
  factory :current_host_stat do
    host
    url { 'https://gitlab.example.com' }
    version { '9.0.0' }
    host_created_at { Time.now.utc }
    usage_data_recorded_at { Time.now.utc }
    active_users_count { 20 }
    historical_max_users_count { 30 }
    user_count { 10 }
    license_md5 { SecureRandom.hex(16) }
    edition { UsageData::CE_EDITION }
  end
end
