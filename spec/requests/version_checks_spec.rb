# frozen_string_literal: true

require 'spec_helper'

describe "VersionChecks" do
  describe "GET /check.png" do
    it 'responds 404 for invalid encrypted content' do
      check(info: 'AAAAAA')

      expect(response).to be_not_found
    end

    it 'responds 200 for valid encrypted content' do
      check(headers: { 'HTTP_REFERER' => 'https://gitlab.com/admin' })

      expect(response).to be_ok
      expect(response.content_type).to eq 'image/png'
    end

    it 'does not create a Host entry for an invalid referer' do
      check(headers: { 'HTTP_REFERER' => 'this is not a real referer' })

      expect(response).to be_ok
      expect(Host.count).to eq 0
    end
  end

  describe "GET /check.svg" do
    it 'responds 404 for invalid encrypted content' do
      check(:svg, info: 'AAAAAA')

      expect(response).to be_not_found
    end

    it 'responds 200 for valid encrypted content' do
      check(:svg, headers: { 'HTTP_REFERER' => 'https://gitlab.com/admin' })

      expect(response).to be_ok
      expect(response.content_type).to eq 'image/svg+xml'
    end

    it 'does not create a Host entry for an invalid referer' do
      check(headers: { 'HTTP_REFERER' => 'this is not a real referer' })

      expect(response).to be_ok
      expect(Host.count).to eq 0
    end
  end

  def check(format = :png, info: encrypted_string, headers: {})
    get "/check.#{format}?gitlab_info=#{info}", params: {}, headers: headers
  end

  def encrypted_string
    string = { "version" => "7.8.0.pre" }.to_json
    Base64.urlsafe_encode64(string)
  end
end
