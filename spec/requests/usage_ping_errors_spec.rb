# frozen_string_literal: true

require 'spec_helper'

describe "UsagePingErrors" do
  describe "POST /usage_ping_errors" do
    let(:payload) do
      {
        error: {
          uuid: '1',
          hostname: 'localhost',
          version: '14.5',
          message: 'Error',
          elapsed: '10.5',
          time: Time.now.getutc.to_s
        }
      }
    end

    before do
      allow(UsagePing::ErrorHandler).to receive_message_chain(:new, :execute!)
    end

    it "response 201 for valid request" do
      post "/usage_ping_errors", params: payload

      expect(response).to be_created
    end

    it "calls ErrorHandler with params" do
      expect(UsagePing::ErrorHandler).to receive_message_chain(:new, :execute!).with(payload[:error].stringify_keys)

      post "/usage_ping_errors", params: payload
    end
  end
end
