# frozen_string_literal: true

require 'spec_helper'

describe "UsagePingMetadata" do
  describe "POST /usage_ping_metadata" do
    context 'with valid params' do
      let(:metrics_metadata) do
        [
          { "name" => 'count.issues', "time_elapsed" => "123" },
          { "name" => 'count.boards', "time_elapsed" => "223" },
          { "name" => 'count.epics', "time_elapsed" => "323" },
          { "name" => 'count.merge_requests', "time_elapsed" => "423" },
          { "name" => 'count.pipelines', "time_elapsed" => "523" },
          { "name" => 'count.notes', "time_elapsed" => "633" },
          { "name" => 'count.emojis', "time_elapsed" => "723" },
          { "name" => 'count.services', "time_elapsed" => "823" },
          { "name" => 'count.clusters', "time_elapsed" => "923" },
          { "name" => 'count.namespaces', "time_elapsed" => "10323", "error" => "" },
          { "name" => 'count.projects', "time_elapsed" => "11323", "error" => "Undefined is not a function" }
        ]
      end

      let(:payload) do
        {
          metadata: {
            uuid: "uuid",
            metrics: metrics_metadata
          }
        }
      end

      it "response 201 for valid request" do
        post "/usage_ping_metadata", params: payload

        expect(response).to be_created
      end

      it "create UsagePingMetadatum object", :aggregated_errors do
        expect do
          post "/usage_ping_metadata", params: payload
        end.to change(UsagePingMetadatum, :count).by(1)

        metadata = UsagePingMetadatum.last

        expect(metadata.uuid).to eq("uuid")
        expect(metadata.metrics).to eq(metrics_metadata)
      end

      context 'without uuid - backwards compatibility' do
        let(:payload) do
          {
            metadata: {
              metrics: metrics_metadata
            }
          }
        end

        it "response 201 for valid request" do
          post "/usage_ping_metadata", params: payload

          expect(response).to be_created
        end
      end
    end

    context 'with corrupted data' do
      let(:payload) do
        {
          metadata: {
            uuid: "abcd",
            metrics: nil
          }
        }
      end

      it "response 403" do
        post "/usage_ping_metadata", params: payload

        expect(response).to be_unprocessable
      end

      it "logs error" do
        expect(Rails.logger).to receive(:warn)
                                  .with("Failed to save UsagePingMetadatum. Uuid: abcd. Errors: Metrics can't be blank")
        post "/usage_ping_metadata", params: payload
      end

      it "does not create UsagePingMetadatum object" do
        expect do
          post "/usage_ping_metadata", params: payload
        end.not_to change(UsagePingMetadatum, :count)
      end
    end
  end
end
