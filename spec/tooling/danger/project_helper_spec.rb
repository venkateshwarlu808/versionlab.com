# frozen_string_literal: true

require 'rspec-parameterized'
require 'gitlab-dangerfiles'
require 'danger'
require 'danger/plugins/internal/helper'
require 'gitlab/dangerfiles/spec_helper'

require_relative '../../../danger/plugins/project_helper'

describe Tooling::Danger::ProjectHelper do
  include_context "with dangerfile"

  let(:fake_danger) { DangerSpecHelper.fake_danger.include(described_class) }
  let(:fake_helper) { Danger::Helper.new(project_helper) }

  subject(:project_helper) { fake_danger.new(git: fake_git) }

  before do
    allow(project_helper).to receive(:helper).and_return(fake_helper)
    allow(fake_helper).to receive(:config).and_return(double(files_to_category: described_class::CATEGORIES))
  end

  describe '#categories_for_file' do
    using RSpec::Parameterized::TableSyntax

    where(:path, :expected_categories) do
      'doc/foo'         | :none
      'CONTRIBUTING.md' | :backend
      'LICENSE'         | :backend
      'MAINTENANCE.md'  | :backend
      'PHILOSOPHY.md'   | :backend
      'PROCESS.md'      | :backend
      'README.md'       | :backend
      '.gitlab/dashboards/common_metrics.yml' | :backend

      'app/assets/foo'       | :frontend
      'app/views/foo'        | :frontend
      'public/foo'           | :frontend
      'scripts/frontend/foo' | :frontend
      'spec/javascripts/foo' | :frontend
      'spec/frontend/bar'    | :frontend
      'vendor/assets/foo'    | :frontend
      'jest.config.js'       | :frontend
      'package.json'         | :frontend
      'yarn.lock'            | :frontend

      'app/models/foo' | :backend
      'bin/foo'        | :backend
      'config/foo'     | :backend
      'lib/foo'        | :backend
      'rubocop/foo'    | :backend
      'spec/foo'       | :backend
      'spec/foo/bar'   | :backend

      'generator_templates/foo' | :backend
      'vendor/languages.yml'    | :backend
      'vendor/licenses.csv'     | :backend

      'Gemfile'        | :backend
      'Gemfile.lock'   | :backend
      'Procfile'       | :backend
      'Rakefile'       | :backend
      'FOO_VERSION'    | :backend

      'Dangerfile'                                            | :backend
      'danger/commit_messages/Dangerfile'                     | :backend
      'danger/commit_messages/'                               | :backend
      '.gitlab-ci.yml'                                        | :backend
      '.gitlab/ci/cng.gitlab-ci.yml'                          | :backend
      '.gitlab/ci/ee-specific-checks.gitlab-ci.yml'           | :backend
      'scripts/foo'                                           | :backend
      'lib/gitlab/danger/foo'                                 | :backend
      '.overcommit.yml.example'                               | :backend
      '.codeclimate.yml'                                      | :backend

      'lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml'   | :backend

      'db/schema.rb'                                              | :database
      'db/migrate/foo'                                            | :database
      'db/post_migrate/foo'                                       | :database
      'lib/gitlab/background_migration.rb'                        | :database
      'lib/gitlab/background_migration/foo'                       | :database
      'lib/gitlab/database.rb'                                    | :database
      'lib/gitlab/database/foo'                                   | :database
      'lib/gitlab/sql/foo'                                        | :database
      'rubocop/cop/migration/foo'                                 | :database

      'db/fixtures/foo.rb'                                        | :backend

      'FOO'          | :unknown
      'foo'          | :unknown

      'foo/bar.rb'  | :backend
      'foo/bar.js'  | :frontend
      'foo/bar.txt' | :backend
      'foo/bar.md'  | :backend
    end

    with_them do
      subject { project_helper.helper.categories_for_file(path) }

      it { is_expected.to eq([expected_categories]) }
    end
  end

  describe '.local_warning_message' do
    it 'returns an informational message with rules that can run' do
      expect(described_class.local_warning_message).to eq('==> Only the following Danger rules can be run locally: gemfile, database')
    end
  end

  describe '.success_message' do
    it 'returns an informational success message' do
      expect(described_class.success_message).to eq('==> No Danger rule violations!')
    end
  end

  describe '#rule_names' do
    context 'when running locally' do
      before do
        expect(fake_helper).to receive(:ci?).and_return(false)
      end

      it 'returns local only rules' do
        expect(project_helper.rule_names).to match_array(described_class::LOCAL_RULES)
      end
    end

    context 'when running under CI' do
      before do
        expect(fake_helper).to receive(:ci?).and_return(true)
      end

      it 'returns all rules' do
        expect(project_helper.rule_names).to eq(described_class::LOCAL_RULES | described_class::CI_ONLY_RULES)
      end
    end
  end

  describe '#file_lines' do
    let(:filename) { 'spec/foo_spec.rb' }
    let(:file_spy) { spy }

    it 'returns the chomped file lines' do
      expect(project_helper).to receive(:read_file).with(filename).and_return(file_spy)

      project_helper.file_lines(filename)

      expect(file_spy).to have_received(:lines).with(chomp: true)
    end
  end
end
