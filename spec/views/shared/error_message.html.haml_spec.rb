# frozen_string_literal: true

require 'spec_helper'

describe 'shared/error_message.html.haml' do
  before do
    assign(:message, 'This is the error message')
  end

  it 'shows the error message' do
    render

    expect(rendered).to have_content(/This is the error message/)
  end
end
