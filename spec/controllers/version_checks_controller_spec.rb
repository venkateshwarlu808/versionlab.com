# frozen_string_literal: true

require 'spec_helper'

describe VersionChecksController do
  describe 'GET #check' do
    let(:gitlab_url) { 'http://gitlab.example.com' }

    let(:gitlab_info) do
      Base64.urlsafe_encode64({ version: '0.9.0', type: 'CE' }.to_json)
    end

    before do
      request.env['HTTP_REFERER'] = gitlab_url
    end

    context 'when HTTP_REFERER is missing' do
      it 'returns status 404' do
        request.env['HTTP_REFERER'] = nil

        get :check, params: { gitlab_info: gitlab_info }, as: :svg

        expect(response).to have_http_status(404)
      end
    end

    context 'when everything is ok' do
      context 'when requesting json' do
        before do
          create(:version, version: '1.1.1')
          create(:version, version: '1.2.1')
          create(:version, version: '1.2.2')
          create(:version, version: '1.3.1', details: '<h1>Testing</h1>')
        end

        shared_examples 'expected response' do |vulnerability_type, severity, critical_vulnerability|
          before do
            create(:version, version: '0.9.0', vulnerability_type: vulnerability_type)
            get :check, params: { gitlab_info: gitlab_info }, as: :json
          end

          it 'returns latest 3 stable versions, latest version number, lastest version details, and upgrade severity' do
            version_json = JSON.parse(response.body)
            expect(version_json).to eq(
              {
                'latest_stable_versions' => ['1.3.1', '1.2.2', '1.1.1'],
                'latest_version' => '1.3.1',
                'severity' => severity,
                'critical_vulnerability' => critical_vulnerability,
                'details' => '<h1>Testing</h1>'
              }
            )
          end

          it 'returns json content' do
            expect(response).to have_http_status(200)
            expect(response.content_type).to eq('application/json; charset=utf-8')
          end
        end

        context 'without any vulnerabilities' do
          include_examples 'expected response', :not_vulnerable, 'warning', false
        end

        context 'with non_critical vulnerabilities' do
          include_examples 'expected response', :non_critical, 'danger', false
        end

        context 'with critical vulnerabilities' do
          include_examples 'expected response', :critical, 'danger', true
        end
      end

      context 'when requesting png' do
        it 'returns image' do
          get :check, params: { gitlab_info: gitlab_info }, as: :png

          expect(response).to have_http_status(200)
          expect(response.content_type).to eq('image/png')
        end
      end

      context 'when requesting svg' do
        it 'returns image' do
          get :check, params: { gitlab_info: gitlab_info }, as: :svg

          expect(response).to have_http_status(200)
          expect(response.content_type).to eq('image/svg+xml')
        end
      end
    end

    context 'when hostname is not valid url' do
      before do
        request.env['HTTP_REFERER'] = 'something'
      end

      it 'returns status 200' do
        get :check, params: { gitlab_info: gitlab_info }, as: :png

        expect(response).to have_http_status(200)
      end

      it 'returns update_failed image' do
        get :check, params: { gitlab_info: gitlab_info }, as: :png

        expect(response.header['Content-Disposition']).to include('update_failed.png')
      end
    end

    context 'when version seems invalid' do
      let(:gitlab_info) do
        Base64.urlsafe_encode64({ version: 'v11.9.9.v20190904.2', type: 'CE' }.to_json)
      end

      it 'returns status 200' do
        get :check, params: { gitlab_info: gitlab_info }, as: :png

        expect(response).to have_http_status(200)
      end
    end

    context 'when version is incorrectly encoded' do
      let(:gitlab_info) { 'eyj2zxbzaw9uijoimtuunc4xlwvrin0=' }

      before do
        get :check, params: { gitlab_info: gitlab_info }, as: :json
      end

      it 'returns status 400' do
        expect(response).to have_http_status(400)
      end
    end
  end
end
