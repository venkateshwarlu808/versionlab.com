# frozen_string_literal: true

require 'spec_helper'

describe StatisticsController, type: :controller do
  let(:user) { create(:user) }

  before do
    sign_in(user)
  end

  describe '#show' do
    it 'returns 200' do
      get :show

      expect(response).to have_http_status(200)
    end
  end
end
