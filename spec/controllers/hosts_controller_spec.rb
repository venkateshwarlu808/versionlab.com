# frozen_string_literal: true

require 'spec_helper'

describe HostsController, type: :controller do
  let(:user) { create(:user) }

  before do
    sign_in(user)
  end

  describe '#show' do
    before do
      # rubocop:disable RSpec/EnvAssignment
      ENV['BROWSER_SDK_APP_ID'] = 'test_app_id'
      ENV['BROWSER_SDK_HOST'] = 'test_host'
      ENV['BROWSER_SDK_ENABLED'] = 'test_enabled'
      # rubocop:enable RSpec/EnvAssignment
    end

    it 'returns 200' do
      host = create(:host)

      get :show, params: { id: host.id }

      expect(response).to have_http_status(200)
    end

    it 'assigns gon variables' do
      host = create(:host)

      get :show, params: { id: host.id }

      expect(controller.gon.browser_sdk_app_id).to eq('test_app_id')
      expect(controller.gon.browser_sdk_host).to eq('test_host')
      expect(controller.gon.browser_sdk_enabled).to eq('test_enabled')
    end
  end

  describe '#update' do
    let(:host) { create(:host) }

    it 'returns 200 for a valid host update' do
      put :update, params: { id: host.id, host: { star: false } }

      expect(response).to have_http_status(200)
    end
  end
end
