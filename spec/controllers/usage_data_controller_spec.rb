# frozen_string_literal: true

require 'spec_helper'
require 'rspec-parameterized'

describe UsageDataController, :usage_data do
  let(:headers) { { 'Content-Type' => 'application/json' } }

  before do
    stub_metric_definitions_request
  end

  context 'with authentication' do
    let(:user) { create(:user) }

    before do
      sign_in(user)
    end

    describe 'GET #months' do
      it 'calls UsageDataByMonthService' do
        expect(UsageDataByMonthService).to receive_message_chain(:new, :execute)

        get :months
      end
    end

    describe 'POST #schedule_backfill' do
      let(:backfill_service) { double(:backfill_service, execute: 3) }

      before do
        allow(Salesforce::BackfillService).to receive(:new).and_return(backfill_service)
      end

      it 'calls Salesforce::BackfillService with the month_editions param' do
        expect(Salesforce::BackfillService)
          .to receive(:new)
                .with(%w[2017-01-CE 2016-12-EEP])
                .and_return(backfill_service)

        post :schedule_backfill, params: { month_editions: %w[2017-01-CE 2016-12-EEP] }
      end

      it 'sets the flash to the number of jobs that were scheduled' do
        post :schedule_backfill, params: { month_editions: [] }

        expect(flash[:notice]).to eq('Scheduled 3 jobs')
      end
    end

    describe 'GET #show' do
      render_views

      let(:uuid_1) { '00000000-0000-0000-0000-000000000000' }
      let(:uuid_2) { '63200ba1-f65c-479a-aed0-307bccbf07d5' }

      let!(:usage_data_1) { create(:usage_data, :with_license_data, uuid: uuid_1) }
      let!(:usage_data_2) { create(:usage_data, :with_license_data, uuid: uuid_2) }

      it 'shows service pings for given ID' do
        get :show, params: { id: usage_data_1.id }

        expect(response).to have_http_status(200)
        expect(response.body).to match(/John/)
        expect(response.body).to match(/00000000-0000-0000-0000-000000000000/)
        expect(response.body).to match(/example.com/)
      end

      it 'shows service pings for given UUID' do
        get :show, params: { id: uuid_2 }

        expect(response).to have_http_status(200)
        expect(response.body).to match(/John/)
        expect(response.body).to match(/63200ba1-f65c-479a-aed0-307bccbf07d5/)
        expect(response.body).to match(/example.com/)
      end
    end
  end

  describe 'payloads', type: :request do
    shared_examples 'creates the usage_data and raw_usage_data' do
      it 'creates the usage_data and raw_usage_data' do
        file = File.read(path)
        params = JSON.parse(file)

        expect do
          post usage_data_path, params: params.to_json, headers: headers
        end.to change(RawUsageData, :count).by(1).and change(UsageData, :count).by(1)

        expect(response).to have_http_status(201)

        raw_usage_data = RawUsageData.last
        usage_data = UsageData.last

        expect(usage_data.raw_usage_data_id).to eq(raw_usage_data.id)

        expect(usage_data.uuid).to eq('00000000-0000-0000-0000-000000000000')
        expect(usage_data.hostname).to eq('gdk.test')
        expect(usage_data.settings['collected_data_categories']).to eq(collected_data_categories)

        expect(raw_usage_data.payload['uuid']).to eq('00000000-0000-0000-0000-000000000000')
        expect(raw_usage_data.payload['hostname']).to eq('gdk.test')
        expect(raw_usage_data.payload['settings']['collected_data_categories']).to eq(collected_data_categories)

        raw_usage_data_payload = raw_usage_data.raw_usage_data_payload

        expect(raw_usage_data_payload).not_to be_nil
        expect(raw_usage_data_payload['uuid']).to eq('00000000-0000-0000-0000-000000000000')
        expect(raw_usage_data_payload['hostname']).to eq('gdk.test')
        expect(raw_usage_data_payload['settings']['collected_data_categories']).to eq(collected_data_categories)
      end
    end

    context 'when having Standard, Subscription and Operational metrics for ee' do
      let(:collected_data_categories) { %w[Standard Subscription Operational] }
      let(:path) { Rails.root.join('spec/support/service_ping/ee_standard_subscription_operational.json') }

      it_behaves_like 'creates the usage_data and raw_usage_data'
    end

    context 'when having Standard, Subscription, Operational, Optional metrics for ee' do
      let(:collected_data_categories) { %w[Standard Subscription Operational Optional] }
      let(:path) { Rails.root.join('spec/support/service_ping/ee_standard_subscription_operational_optional.json') }

      it_behaves_like 'creates the usage_data and raw_usage_data'
    end

    context 'when having Standard, Subscription, Operational, Optional metrics for ce' do
      let(:collected_data_categories) { %w[Standard Subscription Operational Optional] }
      let(:path) { Rails.root.join('spec/support/service_ping/ce_standard_subscription_operational_optional.json') }

      it_behaves_like 'creates the usage_data and raw_usage_data'
    end
  end

  context 'with no authentication' do
    describe 'POST #create', type: :request do
      it 'creates a new entry' do
        license_data = [{ 'zuora_subscription_id' => 'zuora12345' }]
        stub_license_show_request(test_usage_data[:license_md5], license_data)

        allow(UsageDataWorker).to receive(:perform_async)

        params = test_usage_data.merge(avg_cycle_analytics: test_cycle_analytics_data)
        stats = test_usage_data[:stats].with_indifferent_access.merge(epics_deepest_relationship_level: 0)
        post usage_data_path, params: params.to_json, headers: headers

        usage_data = UsageData.last
        raw_usage_data = RawUsageData.last

        expect(RawUsageData.count).to eq(1)
        expect(UsageData.count).to eq(1)
        expect(usage_data.raw_usage_data_id).to eq(raw_usage_data.id)
        expect(AvgCycleAnalytics.count).to eq(1)
        expect(usage_data).to have_attributes(usage_data_attributes(stats))
        expect(usage_data.attributes.select { |_, v| v.nil? }.keys).to be_empty
        expect(raw_usage_data.raw_usage_data_payload).not_to be_nil
      end

      it 'creates usage_data without valid raw_usage_data' do
        allow(UsageDataWorker).to receive(:perform_async)

        params = {}

        post usage_data_path, params: params.to_json, headers: headers

        expect(RawUsageData.count).to eq(0)
        expect(UsageData.count).to eq(1)
        expect(UsageData.last.raw_usage_data_id).to be_nil
      end

      it 'saves the exact params in raw_usage_data payload' do
        post usage_data_path, params: test_usage_data.to_json, headers: headers

        raw_usage_data_payload = RawUsageData.last.raw_usage_data_payload

        expect(RawUsageData.last.payload.with_indifferent_access).to eq(test_usage_data.with_indifferent_access)
        expect(raw_usage_data_payload.with_indifferent_access).to eq(test_usage_data.with_indifferent_access)
      end

      it 'includes monthly counts data' do
        allow(UsageDataWorker).to receive(:perform_async)

        expect do
          post usage_data_path, params: test_usage_data_fixed_stats.to_json, headers: headers
        end.to change(UsageData, :count).by(1)

        usage_data = UsageData.last
        expect(usage_data.counts_monthly).not_to be_empty
      end

      it 'includes usage activity by stage data' do
        license_data = [{ 'zuora_subscription_id' => 'zuora12345' }]
        stub_license_show_request(test_usage_data[:license_md5], license_data)
        allow(UsageDataWorker).to receive(:perform_async)

        expect do
          post usage_data_path, params: test_usage_data_fixed_stats.to_json, headers: headers
        end.to change(UsageData, :count).by(1)

        usage_data = UsageData.last
        expect(usage_data.usage_activity_by_stage_monthly).not_to be_empty
        expect(usage_data.usage_activity_by_stage).not_to be_empty
      end

      it 'does not raise an error when we get a feature key with an enabled suffix' do
        # See https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/20043
        params = test_usage_data.each_with_object({}) do |(key, value), hash|
          key =
            if UsageDataHelpers::FEATURES_KEYS_REPLACED.include?(key)
              "#{key}_enabled"
            else
              key
            end

          hash[key.to_sym] = value
        end

        expect { post usage_data_path, params: params.to_json, headers: headers }
          .not_to raise_error
      end

      it 'does not raise an error when we get a empty whitelisted hash key' do
        params = test_usage_data.merge(license_add_ons: {})

        expect { post usage_data_path, params: params.to_json, headers: headers }
          .not_to raise_error
      end

      it 'does not raise an error when we get a empty whitelisted hash key is nil' do
        params = test_usage_data.merge(license_add_ons: nil)

        expect { post usage_data_path, params: params.to_json, headers: headers }
          .not_to raise_error
      end

      it 'does not raise an error for non integer stats' do
        headers = { 'Content-Type' => 'application/json' }
        bad_stats = { counts: { bad_stat: 'something', epics: 'something_else' } }
        params = test_usage_data.merge(avg_cycle_analytics: test_cycle_analytics_data).deep_merge(bad_stats)

        post usage_data_path, params: params.to_json, headers: headers

        expect(response).to have_http_status(422)
        expect(JSON.parse(response.body)).to match({})
      end

      context 'when uploading file' do
        let(:headers) { { 'Content-Type' => 'application/json', 'Accept' => 'text/html' } }
        let(:event_tracker) { instance_double(EventTracker) }
        let(:sdk_enabled) { false }

        subject(:perform_request) { post usage_data_path, params: { service_usage_data_file: file }, headers: headers }

        before do
          stub_env('BACKEND_SDK_ENABLED', sdk_enabled)
        end

        context 'when data is correct' do
          let(:file) { fixture_file_upload('usage_data_payload.json', 'application/json') }

          it 'creates records' do
            perform_request

            expect(RawUsageData.count).to eq(1)
            expect(UsageData.count).to eq(1)
          end

          it 'shows successful message' do
            perform_request

            expect(flash[:notice]).to eq('Service usage data was successfully uploaded.')
          end

          context "with sdk enabled" do
            let(:sdk_enabled) { true }

            it "tracks the event" do
              expect(EventTracker).to receive(:new).and_return(event_tracker)

              expect(event_tracker).to receive(:track).with("service_ping_manual_upload", { upload_successful: true })

              perform_request
            end
          end
        end

        context 'when data has incorrect values' do
          let(:file) { fixture_file_upload('incorrect_values_usage_data_payload.json', 'application/json') }

          it 'creates UsageData' do
            subject

            expect(UsageData.count).to eq(1)
          end

          it 'does not create RawUsageData' do
            subject

            expect(RawUsageData.count).to eq(0)
          end

          context "with sdk enabled" do
            let(:sdk_enabled) { true }

            it "tracks the event" do
              expect(EventTracker).to receive(:new).and_return(event_tracker)

              expect(event_tracker).to receive(:track).with("service_ping_manual_upload", { upload_successful: true })

              perform_request
            end
          end
        end

        context 'when data is not in json format' do
          let(:file) { fixture_file_upload('non_json_usage_data_payload.json', 'application/json') }

          it 'does not create records' do
            subject

            expect(RawUsageData.count).to eq(0)
            expect(UsageData.count).to eq(0)
          end

          it 'shows error message' do
            subject

            expect(flash[:alert]).to eq('There was an error processing uploaded file.')
            expect(flash[:upload_failed]).to eq(true)
          end

          context "with sdk enabled" do
            let(:sdk_enabled) { true }

            it "tracks the event" do
              expect(EventTracker).to receive(:new).and_return(event_tracker)

              expect(event_tracker).to receive(:track).with("service_ping_manual_upload", { upload_successful: false })

              perform_request
            end
          end
        end
      end

      context 'when fixing params' do
        using RSpec::Parameterized::TableSyntax

        where(:payload, :expected_edition, :expected_version) do
          [
            [
              {
                uuid: '0000',
                recorded_at: Time.now.utc,
                settings: { collected_data_categories: %w[Standard Subscription Operational] }
              },
              'EE',
              '14.1'
            ],
            [
              {
                uuid: '0000',
                recorded_at: Time.now.utc,
                edition: 'EEP',
                version: '14.2',
                settings: { collected_data_categories: %w[Standard Operational Subscription] }
              },
              'EEP',
              '14.2'
            ]
          ]
        end

        with_them do
          it 'creates the raw_usage_data and usage_data records with correct version, edition and uuid' do
            allow(UsageDataWorker).to receive(:perform_async)

            expect do
              post usage_data_path, params: payload.to_json, headers: headers
            end.to change(RawUsageData, :count).by(1).and change(UsageData, :count).by(1)

            expect(response).to have_http_status(201)

            raw_usage_data_payload = RawUsageData.last.payload
            usage_data = UsageData.last

            expect(raw_usage_data_payload['edition']).to eq(expected_edition)
            expect(raw_usage_data_payload['version']).to eq(expected_version)
            expect(raw_usage_data_payload['uuid']).to eq('0000')

            expect(usage_data.read_attribute(:edition)).to eq(expected_edition)
            expect(usage_data.version).to eq(expected_version)
            expect(usage_data.uuid).to eq('0000')
          end
        end
      end
    end
  end
end
